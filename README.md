# Homogenized Yarn-Level Cloth

![Deformed Periodic Yarn Pattern](hylc_thumb.png "")

This repository contains the micro-scale part of 'Homogenized Yarn-Level Cloth' (Siggraph 2020).

[Project Page: https://visualcomputing.ist.ac.at/publications/2020/HYLC/](https://visualcomputing.ist.ac.at/publications/2020/HYLC/)

The repository includes
- the yarn pattern elastostatics subject to periodic boundary conditions via macro-scale deformation
- scripts for running these simulations and gathering data
- python scripts for fitting the data

For the macro-scale part, simulating cloth using the fitted materials, see the separate repository ["ARCSim-HYLC"](https://git.ist.ac.at/gsperl/ARCSim-HYLC/).

## Download / Setup

This repository uses submodules that need to be cloned recursively:
```sh
git clone --recurse-submodules <REPOSITORY>
```
If cloned non-recursively, you can download the submodules afterwards using
```sh
git submodule update --init --recursive
```

Next, use the submodule [vcpkg](https://github.com/microsoft/vcpkg/) to install additional dependencies from within the `vcpkg/` directory:
```sh
./bootstrap-vcpkg.sh
./vcpkg install eigen3 pybind11 tbb magnum magnum-integration[imgui]
```
<!-- probably default magnum includes a lot of unneeded modules -->
<!-- maybe also jsoncpp for old YLC dynamics -->
In case you do not need the interactive visualization, but just want to use the python-exposed version you could omit the 'magnum' dependencies.

Optionally, free up some disk space with `rm -rf buildtress && rm -rf downloads`.


## Compilation & Usage

We tested our code on Ubuntu 18.04 with g++ version > 8.

To compile and run the interactive optimization visualization, we use a helper python script: `python exec.py` called from within the `solver/` directory.

To build and install the python package (exposing the c++ pattern optimization to python via pybind11) run `pip install -e src/python/` from within the `solver/` directory (using `-e` makes it a development install that overwrites pre-existing installations of the same package). For examples on importing and using the python module see the `solver/scripts/` directory.
See `batch_hylc.py` script for generating data as in the paper. <!-- ... is the most similar to how ... -->

<!-- make sure pip and python version are the same, ie pip3 for python3 if they are different on your system. -->


## Folders

- **data:** strain->energy data used in the paper
- **db2pyp:** blender utility for reading patterns (in the format of ['Interactive Design of Periodic Yarn-Level Cloth Patterns'](https://graphics.stanford.edu/projects/yarnsim/)), modifying them, and writing to '.pyp'-files
- **fitting:** fitting cloth materials from generated data
- **pyp:** periodic yarn patterns for use in the solver/optimization (see also [db2pyp/readme.txt](db2pyp/readme.txt) and [PeriodicYarnPattern.h](solver/src/simulation/pyp/PeriodicYarnPattern.h) for more details on the format)
- **solver:** periodic pattern solver (inc. python bindings and deformation sampling scripts)
- **vcpkg:** package manager submodule for installing c++ dependencies

## Short Implementation Information:
We extend a [PeriodicYarnPattern.h](solver/src/simulation/pyp/PeriodicYarnPattern.h) via tiling to a [GhostYarnPattern.h](solver/src/simulation/pyp/GhostYarnPattern.h) as an implementation of a periodic force model (using periodic copies to compute forces and reducing them back to forces only on the actual non-copied original degrees of freedom). This is combined with a [GridExpansion.h](solver/src/simulation/pyp/expansion/GridExpansion.h) to describe a deformed shell volume (computed from input strains sx, sa, sy, II as described in the paper) and in turn define the micro-scale fluctuation $`\tilde{u}`$. The [Solvable.h](solver/src/simulation/statics/newton/Solvable.h) then exposes the degrees of freedom (excluding periodicity!), forces (via a [PeriodicForceModel.h](solver/src/simulation/pyp/PeriodicForceModel.h)) and constraints to the [NewtonSolver.h](solver/src/simulation/statics/newton/NewtonSolver.h). Note that we reparametrize the DOFs from $`\tilde{u}`$ to $`g = R^T \tilde{u}`$ for easier constraints and trivial periodicity ($`g^+ = g^-`$).

## GUI Application

Controls:
- drag left mouse: move
- drag right mouse + control: zoom
- drag right mouse + shift: rotate
- Space: pause/unpause
- R: reset (e.g. after changing parameters)

You can play with input strains in the 'Expansion' part of the GUI and the yarn material in the 'Material' part. You can change the pattern by choosing another filepath for the 'PatternData File'. The homogenized energy can be found as 'E/A' in the 'Output' window.

## Disclamer

This code is a re-implementation of the code used for the actual publication. Specifically, this code includes a re-implementation of the micro-optimization, and does not contain code for running the 30x30cm^2 large-scale yarn-level cloth simulations. This choice was made to generate a more readable/usable version of the optimization code for publication and for use in future projects, at the expense of 1:1 reproducibility of the previous results in 'Homogenized Yarn-Level Cloth'. 

For example, while the code is indeed more readable, the yarn-level optimizations converge at different speeds, and it may take longer to generate the same amount of data, most likely due to the modified Newton solver's new regularization being in an imperfect state. It might be worthwhile considering an external library to replace just the Newton solver.

In addition, the optimization now includes the parametric-sliding constraint discussed in the paper ["Mechanics-Aware Deformation of Yarn Pattern Geometry"](https://git.ist.ac.at/gsperl/MADYPG), but can be disabled in the settings of [Solvable.h](solver/src/simulation/statics/newton/Solvable.h).

## License & Citation

This code is released under the MIT license (see [LICENSE.txt](LICENSE.txt)).
It depends on a modified part of `libWetCloth` (`solver/src/simulation/pyp/forces/libWetCloth`) for discrete elastic rod forces, which is released under the (compatible) Clear-BSD license. Files under a different license have a respective statement at the top.

If you use our code or data, please consider citing us:

```bibtex
@article{sperl2020hylc,
  author    = {Sperl, Georg and Narain, Rahul and Wojtan, Chris},
  title     = {Homogenized Yarn-Level Cloth},
  journal   = {ACM Transactions on Graphics (TOG)},
  number    = {4},
  volume    = {39},
  year      = {2020},
  publisher = {ACM}
}
```
