import bpy
import numpy as np
import math

bpyscale = 100


def importDB(filename, targetradius):
    r = float(filename.split("_")[-1][:-4])
    # rescale s.t. radius = targetradius
    if targetradius <= 0:
        importscale = 1
    else:
        importscale = targetradius / r
        r = targetradius

    yl = []
    with open(filename, 'r') as thefile:
        y = []
        for line in thefile:
            line = line.split()
            if len(line) == 3 and not line[0].startswith("Num"):  # xyz
                y.append([float(v)*importscale for v in line])
            elif len(y) > 0:
                yl.append(y)
                y = []
        if len(y) > 0:
            yl.append(y)

    assert(all([len(y) > 1 for y in yl]))

    return yl, r


def objFromYL(yl, radius, scale=1, name="splineobj", **kwargs):
    # create the curve data
    curveData = bpy.data.curves.new('name', type='CURVE')
    curveData.dimensions = '3D'

    for yarn in yl:
        # map coords to spline
        spline = curveData.splines.new('NURBS')
        spline.points.add(len(yarn) - 1)  # already starts with 1 point
        for i, vtx in enumerate(yarn):
            x, y, z = vtx
            spline.points[i].co = (x, y, z, 1)
        if "order_u" in kwargs:
            spline.order_u = kwargs["order_u"]
        spline.use_endpoint_u = True

    # create object from data
    cobj = bpy.data.objects.new(name, curveData)
    if "resolution_u" in kwargs:
        # this should set u for all subsplines
        cobj.data.resolution_u = kwargs["resolution_u"]

    # fill curve
    cobj.data.fill_mode = 'FULL'
    cobj.data.bevel_depth = radius
    if "resolution_v" in kwargs:
        cobj.data.bevel_resolution = kwargs["resolution_v"]
    cobj.scale = (scale, scale, scale)

    return cobj


def ylFromObj(obj, scale=1):
    r = obj.data.bevel_depth * scale
    yl = []
    for spline in obj.data.splines:
        yl.append([[c * scale for c in p.co][:3] for p in spline.points])
    return yl, r


def resampleYL(yl, max_segment_length, keep_if_smaller=False):
    if (max_segment_length <= 0):
        return yl

    ylnew = []
    for y in yl:
        y = np.array(y, dtype=np.float)

        if keep_if_smaller: # dont resample if max length < target max length
            smaller = True
            for i in range(len(y)-1):
                l = np.linalg.norm(y[i+1]-y[i])
                if l > max_segment_length:
                    smaller = False
                    break
            if smaller:
                print("max length < target max length, skipping resampling.")
                ylnew.append(y)
                continue

        # prepare param array [0... lacc/l ... 1]
        l = 0
        T = np.empty(len(y), dtype=np.float)
        T[0] = 0
        for i in range(len(y)-1):
            l += np.linalg.norm(y[i+1]-y[i])
            T[i+1] = l
        T /= l

        # resample to n vertices with distance < max_segment_length
        n = int(math.ceil(l / max_segment_length) + 0.5) + 1
        n = max(n, 2)

        # for each new t: find next smaller param in T and use for lerping vertex
        ynew = np.empty((n, 3), dtype=np.float)
        iT = 0
        assert(len(y) > 1)
        for i in range(n):
            t = i * 1.0/(n-1)  # [0 ... 1]
            # find next iT such that T[iT] < t <= T[iT+1] but limited to iT < nT-2
            while iT < len(T) - 2 and T[iT+1] <= t:
                iT += 1
            if i == n-1:
                ynew[i] = y[-1]
            else:
                a = (t-T[iT])/(T[iT+1]-T[iT])
                ynew[i] = (1-a) * y[iT] + a * y[iT+1]
        ylnew.append(ynew)
    return ylnew


def find_periodic_match(yl, i, j, px, py, ptol):
    # find tip vertex yl[ii][jj] that is exactly px or py away from yl[i][j]
    # assume only one match and only one periodic direction (ie not in corner!)

    ptol2 = ptol*ptol
    for ii in range(len(yl)):
        for jj in [0, len(yl[ii])-1]:
            if ii == i and jj == j:
                continue
            d = yl[ii][jj] - yl[i][j]
            for k in [0, 1]:
                pdir = np.eye(3)[k]
                p = d @ pdir
                o = (d - p * pdir)

                if (o @ o > ptol2):
                    continue

                if abs(abs(p) - [px, py][k]) < ptol:
                    # if vertex found on left, jump is to right (toroidal)
                    dx = (-1 if d[0] > 0 else 1) if k == 0 else 0
                    dy = (-1 if d[1] > 0 else 1) if k == 1 else 0
                    # adapt approx periodic to perfect from average
                    c = 0.5 * (yl[ii][jj] + yl[i][j])
                    ab = np.array([px*dx, py*dy, 0])
                    yl[ii][jj] = c - 0.5 * ab
                    yl[i][j] = c + 0.5 * ab

                    return ii, jj, dx, dy
    assert(False)
    return -1, -1, 0, 0


def toPYP(yl, px, py, ptol=1e-10):
    # [[xyz]] -> V [xyz], E [v0 v1 dx dy]

    n = sum([len(y)-1 for y in yl])  # not counting periodic vertex
    yl = [np.array(y, dtype=np.float) for y in yl]

    V = np.empty((n, 3), dtype=np.float)  # xyz
    E = np.empty((n, 4), dtype=np.int)  # v0 v1 dx dy

    vix = 0
    # visited := vix != -1, stores starting vix of yl-yarns
    startvix = [-1] * len(yl)
    for i in range(len(yl)):
        if startvix[i] >= 0:
            continue

        # march through periodic yarn accumulating data
        i_cur = i
        forward = True
        while 1:
            # check for bad periodic loop
            assert(i_cur == i or startvix[i_cur] < 0)
            startvix[i_cur] = vix

            # accumulate vertex-edges except final
            if forward:
                start = 0
                stop = len(yl[i_cur]) - 1
                step = 1
            else:
                start = len(yl[i_cur]) - 1
                stop = 0
                step = -1

            for j in range(start, stop, step):  # 0 .. N-2 or N-1 .. 1
                V[vix] = yl[i_cur][j]
                E[vix] = (vix, vix+1, 0, 0)  # will be overwritten for final
                vix += 1

            # find jump
            next_i, next_j, dx, dy = find_periodic_match(
                yl, i_cur, stop, px, py, ptol)

            assert(abs(dx)+abs(dy) == 1)
            i_cur = next_i
            forward = next_j == 0
            if startvix[next_i] >= 0:
                ovix = startvix[next_i]
            else:
                ovix = vix  # next periodic yarn will start with vix
            E[vix - 1] = (vix-1, ovix, dx, dy)
            V[ovix] = yl[next_i][next_j]  # for averaged perfect period length

            if i_cur == i:  # have come full circle
                break

    assert(vix == n)
    return V, E


def rectangulize(V, E, px, py):
    ve = computeVertexEdgeTable(len(V), E)

    # for each vert find di dj such that its position + di*py dj px is within [minx,minx+px) etc
    # apply its new position and apply di dj depending on order to its incident edges
    minxy = V[:,:2].min(axis=0)
    for i in range(V.shape[0]):
        xy = V[i,:2]

        dx = int((xy[0] - minxy[0]) // px)
        dy = int((xy[1] - minxy[1]) // py)
        if not (dx == 0 and dy == 0):   
            V[i,:2] -= np.array([px * dx, py * dy])
            E[ve[i,0],2:4] += np.array([dx,dy],dtype=np.int) # modify prev edge
            E[ve[i,1],2:4] -= np.array([dx,dy],dtype=np.int) # modify next edge

    # afterwards check if any edge has both di dj nonzero
    for e in E:
        assert (sum([e[i] != 0 for i in [2,3]]) < 2)


def tile(V, E, px, py, n, m):
    n = max(1, n)
    m = max(1, m)
    if n == 1 and m == 1:
        return V, E, px, py
    # copy V to n*m size with translated vertices
    # copy E to n*m size with nvertsfactor shifted vert indices
    # for all periodic edges in cell i,j (ij from cix from vix % nverts) if connecting then change didij to 0 replace vixout

    npre = len(V)
    Vnew = np.resize(V,(npre * n * m, 3)) # copies V to fill!
    Enew = np.resize(E,(npre * n * m, 4)) # copies E to fill!

    for vix in range(Vnew.shape[0]):
        cix = vix // npre # each cell has npre verts/edges
        i = cix // m
        j = cix % m
        # translate to new cell
        Vnew[vix] += np.array([j * px, i * py,0.0])
        # update edge indices        
        Enew[vix,0] += cix * npre # just shift index
        dx, dy = Enew[vix,2:]
        if (abs(dx) + abs(dy)) == 0: # normal edge
            Enew[vix,1] += cix * npre
            # print("Connecting\n", vix, Enew[vix,0], Enew[vix,1])
        else: # periodic edge
            # get neighboring cell to which the periodic edge jumps
            # if it is outside -> wrap around
            # note python does modulo also for negative
            nj = j + dx
            ni = i + dy
            if nj >= 0 and nj <= m-1: # jump between existing cells
                Enew[vix,2] = 0 # set dx = 0
            if ni >= 0 and ni <= n-1: # jump between existing cells
                Enew[vix,3] = 0 # set dy = 0
            cixother = (ni % n) * m + (nj % m)
            Enew[vix,1] += cixother * npre

    return Vnew, Enew, px * m, py * n


def computeVertexEdgeTable(n, E):
    ve = np.full((n, 2), -1, dtype=np.int)
    for i in range(len(E)):
        ve[E[i, 0], 1] = i  # outgoing edge
        ve[E[i, 1], 0] = i  # incoming edge
    return ve


def toYL(V, E, px, py):
    yl = []
    ve = computeVertexEdgeTable(len(V), E)

    # starts: vix where previous edge ve[vix,0] is periodic E[ve[vix,0],2:].abssum > 0
    starts = np.argwhere(np.abs(E[ve[:, 0], 2:]).sum(axis=1) > 0).reshape(-1)

    for vix in starts:
        vix = vix
        y = []
        while 1:
            y.append(V[vix])
            outedge = E[ve[vix, 1]]
            if np.abs(outedge[2:]).sum() == 0:  # non periodic edge
                vix = outedge[1]
            else:
                ovix, dx, dy = outedge[1:]
                y.append(V[ovix] + np.array([px*dx, py*dy, 0.0]))
                break
        yl.append(y)
    return yl

def savePYP(filepath, V, E, px, py, r, floatfmt="{:.8g}"):
    assert(len(V) == len(E))
    assert(px > 0 and py > 0 and r > 0)
    intfmt = "{:d}"
    headerfmt = " ".join([floatfmt, floatfmt, floatfmt, intfmt]) + "\n"
    vertfmt = " ".join([floatfmt] * 4) + "\n"
    edgefmt = " ".join([intfmt] * 4) + "\n"
    # *pattern # px py radius nvertedges
    # px py radius n
    #
    # *V # dofs: x y z t
    # x y z t
    # ...
    #
    # *E # edges [v0, v1, dx, dy]
    # v0 v1 dx dy
    # ...
    with open(filepath, 'w') as thefile:
        thefile.write("*pattern # px py radius num_vertedges\n")
        thefile.write(headerfmt.format(px, py, r, len(V)))
        thefile.write("\n")

        thefile.write("*Q # dofs: x y z t\n")
        for i in range(len(V)):
            thefile.write(vertfmt.format(V[i,0],V[i,1],V[i,2],0))
        thefile.write("\n")

        thefile.write("*E # edges [v0, v1, dx, dy]\n")
        for i in range(len(E)):
            thefile.write(edgefmt.format(*E[i]))
        thefile.write("\n")


