Short explanation:

Using python scripts from within the 'db2pyp.blend' file, we load or construct periodic patterns and save them in our custom '.pyp' format.

In the subdirectory DB/IDPYP we have deleted everything except a manually fixed knit pattern, as Leaf et al. did not include a license file with their data. Instead, you can just download and paste their data in there.

In the 'db2pyp.blend' file, you will find scripts on the right, where "load" loads a Leaf-et-al file as curves, "save" saves it as a pyp file. After loading or handcrafting a pattern you'll have to manually set the curve objects 'Custom Properties' to contain a vector 'period' to define the periodic patch size. In between you can also look at the 'apply' script, that allows resampling the curves uniformly, making sure that all vertices are contained in a rectangular patch, and even tiling/repeating the pattern.

Note that we also load patterns scaled by 100 to cm units, to be better visible in blender, though we save in units of meters again. Note that Leaf et al.'s patterns might be in different scales (knit patterns usually in cm, and woven patterns smaller).

About the '.pyp' format:

'.pyp' files and similarly the PeriodicYarnPattern (often abbreviated as PYP) class in the solver/src directory are in a self-made format for periodic yarn patterns, with all the information we found necessary to save a (rest) state. It should be somewhat documented in terms of comments in the header file of said class, and also all of the pyp files contain comments for what the blocks of data are.

E.g. the following block gives the periodic length (in m) along x and y (or also called xi0, xi1) coordinates, with a yarn radius of 0.001m. And just in case it also mentiones how many yarn segments are in this pattern. Note that due to periodicity, there are as many vertices as there are edges. 
*pattern # px py radius num_vertedges
0.021428 0.01428571 0.001 251
And similarly to libWetCloths yarn model which we use for forces, almost all per-edge data is also stored as if indexed by its first vertex. As an example the 4-vectors that make up the vertex values are xyz of a vertex and the twist t(or theta) of its outgoing edge. These are stored in the *V block.

The edges *E are of the form [v0, v1, dx, dy], where v0 v1 are the incident vertex indices, and dx dy describes how many boundaries the edge crosses (which at most should be one). Edges with a nonzero dx or dy thus connect some vertex to a periodic version of it across the boundary. Having the info stored this way made it much easier for me to traverse individual periodic yarns in knits, or to tile patterns for creating ghost segments.

This is the minimum info needed to construct a periodic patch, and then after simulation it can also include information such as rest lengths, rest curvatures, discrete elastic rods reference directors, etc.