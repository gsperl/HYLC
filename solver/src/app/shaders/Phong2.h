#ifndef _PHONG2_H_
#define _PHONG2_H_

#include <Corrade/Utility/Resource.h>
#include <Magnum/GL/Shader.h>
#include <Magnum/GL/Version.h>

#include <Magnum/GL/AbstractShaderProgram.h>
#include <Magnum/Magnum.h>
#include <Magnum/Math/Color.h>
#include <Magnum/Math/Matrix3.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Math/Vector3.h>

namespace Magnum {

struct Phong2Lights {
  Magnum::Color3 ambientLight;
  Magnum::Color3 lightColor1;
  Magnum::Color3 lightColor2;
};

struct Phong2Material {
  float shininess;
  float shininess2;
};

class Phong2 : public GL::AbstractShaderProgram {
public:
  // inputs
  typedef GL::Attribute<0, Vector3> Position;
  typedef GL::Attribute<2, Vector3> Normal;
  typedef GL::Attribute<3, Color3> Color;

  explicit Phong2();
  // uniforms
  // mat4 0 transformationMatrix
  // mat3 1 normalMatrix
  // mat4 2 projectionMatrix
  // vec4 3 lightColor1
  // vec4 4 lightColor2
  // vec3 5 lightDir1
  // vec3 6 lightDir2
  // float 7 shininess
  // float 8 shininess2
  // vec4 9 ambientLight

  Phong2 &setLights(Phong2Lights &data) {
    setUniform(9, data.ambientLight);
    setUniform(3, data.lightColor1);
    setUniform(4, data.lightColor2);
    return *this;
  }

  Phong2 &setMaterial(Phong2Material &data) {
    setUniform(7, data.shininess);
    setUniform(8, data.shininess2);
    return *this;
  }

  Phong2 &setTransformationMatrix(const Matrix4 &mat) {
    setUniform(0, mat);
    return *this;
  }
  Phong2 &setProjectionMatrix(const Matrix4 &mat) {
    setUniform(2, mat);
    return *this;
  }
  Phong2 &setNormalMatrix(const Matrix3 &mat) {
    setUniform(1, mat);
    return *this;
  }
  Phong2 &setLightDir1(const Vector3 &vec) {
    setUniform(5, vec);
    return *this;
  }
  Phong2 &setLightDir2(const Vector3 &vec) {
    setUniform(6, vec);
    return *this;
  }
};
}

#endif /* end of include guard: _PHONG2_H_ */
