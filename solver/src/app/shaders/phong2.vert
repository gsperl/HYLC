#extension GL_ARB_explicit_uniform_location : require
#extension GL_ARB_explicit_attrib_location : require
layout(location = 0) uniform highp mat4 transformationMatrix;
layout(location = 1) uniform mediump mat3 normalMatrix;
layout(location = 2) uniform highp mat4 projectionMatrix;

layout(location = 0) in highp vec4 position;
layout(location = 2) in mediump vec3 normal;
layout(location = 3) in mediump vec3 color;

out mediump vec3 transformedNormal;
out highp vec3 cameraDirection;
out mediump vec3 vertexColor;

void main() {
  /* Transformed vertex position */
  highp vec4 transformedPosition4 = transformationMatrix * position;
  highp vec3 transformedPosition =
      transformedPosition4.xyz / transformedPosition4.w;

  /* Transformed normal vector */
  transformedNormal = normalMatrix * normal;

  /* Direction to the camera */
  cameraDirection = -transformedPosition;

  vertexColor = color;

  /* Transform the position */
  gl_Position = projectionMatrix * transformedPosition4;
  // gl_Position = projectionMatrix*transformationMatrix *position;
  // gl_Position.rgb *=10;
}
