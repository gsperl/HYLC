#include "Phong2.h"

#include <Corrade/Containers/Reference.h>

// NEEDED FOR STATIC LIBRARY
// static void initialize() { CORRADE_RESOURCE_INITIALIZE(Shader_RSRCS); }

Magnum::Phong2::Phong2() {
  // initialize();

  Utility::Resource rs("Shaders");

  GL::Shader vert(GL::Version::GL330, GL::Shader::Type::Vertex);
  GL::Shader frag(GL::Version::GL330, GL::Shader::Type::Fragment);

  vert.addSource(rs.get("phong2.vert"));
  frag.addSource(rs.get("phong2.frag"));

  CORRADE_INTERNAL_ASSERT_OUTPUT(GL::Shader::compile({vert, frag}));

  attachShaders({vert, frag});

  CORRADE_INTERNAL_ASSERT_OUTPUT(link());
}
