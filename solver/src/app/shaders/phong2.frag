#extension GL_ARB_explicit_uniform_location : require
layout(location = 3) uniform lowp vec3 lightColor1;
layout(location = 4) uniform lowp vec3 lightColor2;
layout(location = 5) uniform highp vec3 lightDirection1;
layout(location = 6) uniform highp vec3 lightDirection2;
layout(location = 7) uniform mediump float shininess1;
layout(location = 8) uniform mediump float shininess2;
layout(location = 9) uniform lowp vec3 ambientLight;

in mediump vec3 transformedNormal;
in highp vec3 cameraDirection;
in mediump vec3 vertexColor;

out lowp vec4 colorOut;

void main() {
  colorOut = vec4(0,0,0,1);

  // /* Ambient color */
  colorOut.rgb += ambientLight*vertexColor;

  mediump vec3 normalizedTransformedNormal = normalize(transformedNormal);
  highp vec3 normalizedLightDirection1 =lightDirection1;// normalize(lightDirection1);
  highp vec3 normalizedLightDirection2 = lightDirection2;//normalize(lightDirection2);
  /* Add diffuse color */
  lowp float intensity1 =
      max(0.0, dot(normalizedTransformedNormal, normalizedLightDirection1));
  lowp float intensity2 =
      max(0.0, dot(normalizedTransformedNormal, normalizedLightDirection2));
  colorOut.rgb += vertexColor * lightColor1 * intensity1;
  colorOut.rgb += vertexColor * lightColor2 * intensity2;

  /* Add specular color, if needed */
  if (intensity1 > 0.001) {
    highp vec3 reflection =
        reflect(-normalizedLightDirection1, normalizedTransformedNormal);
    mediump float specularity =
        pow(max(0.0, dot(normalize(cameraDirection), reflection)), shininess1);
    colorOut.rgb += lightColor1 * vertexColor * specularity;
  }
  if (intensity2 > 0.001) {
    highp vec3 reflection =
        reflect(-normalizedLightDirection2, normalizedTransformedNormal);
    mediump float specularity =
        pow(max(0.0, dot(normalize(cameraDirection), reflection)), shininess2);
    colorOut.rgb += lightColor2 * vertexColor * specularity;
  }
  // color.a = 1.0;
}
