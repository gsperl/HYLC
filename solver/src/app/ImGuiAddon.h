#ifndef _IMGUIADDON_H_
#define _IMGUIADDON_H_

#include <imgui.h>
#include <string>
// #include <unordered_map>

std::string linux_fileselector(const std::string &filehint) {
  std::string data;
  FILE *stream;
  const int max_buffer = 1024;
  char buffer[max_buffer];

  char cmdbuffer[1024];
  sprintf(cmdbuffer, "zenity --file-selection --filename=%s", filehint.c_str());

  stream = popen(cmdbuffer, "r");
  if (stream) {
    while (!feof(stream))
      if (fgets(buffer, max_buffer, stream) != NULL)
        data.append(buffer);
    pclose(stream);
  }

  // remove trailing white space
  while (!data.empty() && std::isspace(data.back())) data.pop_back();

  return data;
}

// ----- ImGui Addon ------------------------------------------------------- //
namespace ImGui {

bool InputText(const char *label, std::string &str, size_t size = 1024) {
  PushID(&str);
  char str0[size];
  strncpy(str0, str.c_str(), sizeof(str0));

  if (ImGui::InputText(label, str0, size)) {
    str = str0;
    return true;
  } else {
    return false;
  }
  PopID();
}

bool DragDouble(const char *label, double *v, float v_speed = 1.0f,
                float v_min = 0.0f, float v_max = 0.0f,
                const char *display_format = "%.3f", float power = 1.0f) {
  float tmp = *v;
  if (ImGui::DragFloat(label, &tmp, v_speed, v_min, v_max, display_format,
                       power)) {
    *v = tmp;
    return true;
  }
  return false;
}

class ItemWidthGuard {
 public:
  ItemWidthGuard(float width) { ImGui::PushItemWidth(width); }
  ~ItemWidthGuard() { ImGui::PopItemWidth(); }
};

// namespace internal {
// template <typename T> void UnorderedMapLine(const std::string &key, T &value)
// {
//   ImGui::Text("map value type gui not implemented");
// }
// template <> void UnorderedMapLine(const std::string &key, bool &value) {
//   ImGui::Checkbox(key.c_str(), &value);
// }
// template <> void UnorderedMapLine(const std::string &key, double &value) {
//   ImGui::DragDouble(key.c_str(), &value,0.001f);
// }
// } // namespace internal
//
// template <typename T>
// void UnorderedMap(const std::string &label,
//                   std::unordered_map<std::string, T> &map,
//                   ImGuiTreeNodeFlags flags = 0) {
//   PushID(&map);
//   if (ImGui::TreeNodeEx(label.c_str(), flags)) {
//     for (auto &entry : map) {
//       const auto &key = entry.first;
//       auto &value = entry.second;
//       // ImGui::AlignTextToFramePadding();
//       // ImGui::Text("%s", key.c_str());
//       // ImGui::SameLine();
//       // ImGui::Checkbox(key.c_str(), &value);
//       internal::UnorderedMapLine(key, value);
//     }
//     ImGui::TreePop();
//   }
//   PopID();
// }
//
//
// void FormattedOutput(const std::string &label,
//                   std::map<int, std::string> &map,
//                   ImGuiTreeNodeFlags flags = 0) {
//   PushID(&map);
//   if (ImGui::TreeNodeEx(label.c_str(), flags)) {
//     for (auto &entry : map) {
//       // const auto &key = entry.first;
//       auto &value = entry.second;
//       ImGui::Text("%s",value.c_str());
//     }
//     ImGui::TreePop();
//   }
//   PopID();
// }

// [ 3.12 ] e [1]
template <typename Scalar>
bool DragScientific(const char *label, Scalar *v, float v_speed = 0.1f,
                    float v_min = -9.999999f, float v_max = 9.999999f,
                    int e_min = 0, int e_max = 0,
                    const char *display_format = "%.2f", float power = 10.0f) {
  PushID(v);
  // deconstruct to mantissa and integer power
  // *v = m * power ^ e
  int e   = 0;
  float m = (float)*v;
  if (m != 0) {
    while (std::abs(m) * power < 1) {  // m should be not in -1/power to 1/power
      m *= power;
      --e;
    }
    float invpower = 1.0f / power;
    while (std::abs(m) >= power) {  // m should be within -power to power
      m *= invpower;
      ++e;
    }
  }

  bool modified = false;
  ImGui::AlignTextToFramePadding();
  ImGui::Text("%s", label);
  ImGui::SameLine();
  ImGui::PushItemWidth(100);
  modified |= ImGui::DragFloat("##empty1", &m, v_speed, v_min, v_max,
                               display_format, power);
  ImGui::SameLine();
  ImGui::AlignTextToFramePadding();
  ImGui::Text("e");
  ImGui::SameLine();
  ImGui::PushItemWidth(100);
  modified |= ImGui::DragInt("##empty2", &e, 1, e_min, e_max, "%.0f");
  if (modified) {
    // reconstruct and store in var
    *v = m * std::pow(power, e);
  }
  ImGui::SameLine();
  ImGui::AlignTextToFramePadding();
  ImGui::Text("%e", *v);

  PopID();
  return modified;
}

bool SwitchEnum(const std::string &label, const std::vector<std::string> labels,
                int *v) {
  ImGui::AlignTextToFramePadding();
  ImGui::Text("%s", label.c_str());
  bool modified = false;
  for (int i = 0; i < (int)labels.size(); ++i) {
    ImGui::SameLine();
    bool tmp = i == *v;
    if (ImGui::Checkbox(labels[i].c_str(), &tmp)) {
      modified = true;
      *v       = i;
    }
  }
  return modified;
}

bool HueButton(float hue, const char *c /*, int id = -1*/) {
  ImGui::PushID(c);
  ImGui::PushID(&hue);
  ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(hue, 0.6f, 0.6f));
  ImGui::PushStyleColor(ImGuiCol_ButtonHovered,
                        (ImVec4)ImColor::HSV(hue, 0.7f, 0.7f));
  ImGui::PushStyleColor(ImGuiCol_ButtonActive,
                        (ImVec4)ImColor::HSV(hue, 0.8f, 0.8f));
  bool val = ImGui::Button(c);
  ImGui::PopStyleColor(3);
  ImGui::PopID();
  ImGui::PopID();
  return val;
}

bool FileButton(float hue, const char *label, std::string &str) {
  ImGui::PushID(&str);

  if (ImGui::HueButton(hue, label)) {
    auto str2 = linux_fileselector(str);
    if (!str2.empty())
      str = str2;
    ImGui::PopID();
    return true;
  }
  ImGui::PopID();
  return false;
}

}  // namespace ImGui

#endif /* end of include guard: _IMGUIADDON_H_ */
