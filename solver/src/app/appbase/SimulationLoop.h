#ifndef _SIMULATIONLOOP_H_
#define _SIMULATIONLOOP_H_

using namespace std;
#include <atomic>
#include <chrono>
#include <iostream>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>

template <class T> class ConcurrentQueue : public queue<T> {
public:
  void push(const T &t) {
    lock_guard<mutex> guard(queue_mutex);
    m_queue.push(t);
  }
  void push(T &&t) {
    lock_guard<mutex> guard(queue_mutex);
    m_queue.push(std::forward<T>(t));
  }
  template <class... Args> void emplace(Args &&... args) {
    lock_guard<mutex> guard(queue_mutex);
    m_queue.emplace(std::forward<Args>(args)...);
  }
  queue<T> copy() {
    lock_guard<mutex> guard(queue_mutex);
    return m_queue; // copies once
  }
  void moveTo(queue<T> &q) // copy and clear
  {
    lock_guard<mutex> guard(queue_mutex);
    q = m_queue;
    while (!m_queue.empty())
      m_queue.pop();
  }

  void clear() {
    lock_guard<mutex> guard(queue_mutex);
    while (!m_queue.empty())
      m_queue.pop();
  };

private:
  mutex queue_mutex;
  queue<T> m_queue;
};

template <class SimulationType> // assume update(), getDrawingState()
class SimulationLoop {
  // magical function pointer that also allows lambda functions
  using Task = std::function<void(SimulationType &)>;

public:
  SimulationLoop() {
    m_paused = false;
    m_single_step = true;
    m_default_sleep = 10;
    m_simulation = std::make_shared<SimulationType>();
  }

  ~SimulationLoop() { // called from application thread
    printf("Stopping thread...\n");
    m_running = false;
    printf("Joining thread...\n");
    m_thread
        .join(); // application thread should wait for this simulation thread
    printf("Thread joined.\n");
  }

  void startThread() { // called from application thread

    printf("Starting thread.\n");
    m_running = true;
    m_thread = thread([this]() { // simulation thread
      while (m_running) {
        if (!m_paused || m_single_step) {
          this->loop();
          m_single_step = false;
        }

        // add a delay of some microseconds to avoid mutex-lock-lag if
        // simulation runs at 0 microseconds (e.g. some pausing mechanisms)
        if (m_default_sleep > 0)
          std::this_thread::sleep_for(
              std::chrono::microseconds(m_default_sleep));
        }
      printf("Thread stopped.\n");
    });
  }

  // run on calling thread, but mutexed simulation
  void runTaskImmediate(Task task) {
    lock_guard<mutex> guard(m_sim_mutex);
    task(*m_simulation);
  }
  void addTaskPeriodic(Task task) { m_periodicTasks.push(task); }
  void addTaskOnce(Task task) { m_singleTasks.push(task); }

  std::atomic<bool> m_paused;
  std::atomic<bool> m_single_step;
  std::atomic<int> m_default_sleep; // ms to sleep instead of paused loop

  // warning not threadsafe, use only when sure
  std::shared_ptr<SimulationType> getSimulation() { return m_simulation; }

private:
  std::shared_ptr<SimulationType> m_simulation;
  ConcurrentQueue<Task>
      m_singleTasks; // tasks executed once only, concurrent access
  ConcurrentQueue<Task>
      m_periodicTasks; // tasks executed every frame, concurrent access
  thread m_thread;
  std::atomic<bool> m_running;
  mutex m_sim_mutex;

  void loop() {
    // periodic tasks
    queue<Task> q1 = m_periodicTasks.copy();

    while (!q1.empty()) {
      lock_guard<mutex> guard(m_sim_mutex);
      q1.front()(*m_simulation);
      q1.pop();
    }

    // single tasks
    queue<Task> q2;
    m_singleTasks.moveTo(q2);

    while (!q2.empty()) {
      lock_guard<mutex> guard(m_sim_mutex);
      q2.front()(*m_simulation);
      q2.pop();
    }
  }
};

#endif /* end of include guard: _SIMULATIONLOOP_H_ */
