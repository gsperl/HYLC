#ifndef _GLAPPLICATIONBASE_H_
#define _GLAPPLICATIONBASE_H_

#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/Renderer.h>
#include <Magnum/Platform/Sdl2Application.h>

#include <Magnum/ImGuiIntegration/Context.hpp>
#include <imgui.h>

#include "../debug/debug.h"
#include "SimulationLoop.h"

#include <mutex>
using namespace std;

using namespace Magnum;
using namespace Magnum::Math::Literals;

/*
Application for fixed Simulation, Scene and synchronizing DrawingState
Requirements:
SimulationType.update(); // in SimulationLoop.h
DrawingStateType.synchronize(SimulationType); // in GLApplicationBase.h
SceneType.loadDrawingState(DrawingStateType); // in GLApplicationBase.h
SceneType.draw(); // in GLApplicationBase.h
*/

template <typename SimulationType, typename SceneType,
          typename DrawingStateType>
class GLApplicationBase : public Magnum::Platform::Application {
public:
  explicit GLApplicationBase(const Arguments &arguments,
                             const Configuration &configuration,
                             const GLConfiguration &glconfiguration,
                             bool start_immediate = true)
      : Platform::Application{arguments, configuration, glconfiguration} {

    m_imgui = ImGuiIntegration::Context(Vector2{windowSize()} / dpiScaling(),
                                        windowSize(), framebufferSize());
    /* Set up proper blending to be used by ImGui. There's a great chance
           you'll need this exact behavior for the rest of your scene. If not,
       set this only for the drawFrame() call. */
    GL::Renderer::setBlendEquation(GL::Renderer::BlendEquation::Add,
                                   GL::Renderer::BlendEquation::Add);
    GL::Renderer::setBlendFunction(
        GL::Renderer::BlendFunction::SourceAlpha,
        GL::Renderer::BlendFunction::OneMinusSourceAlpha);

    // limit maximum looping speed
    setMinimalLoopPeriod(16); // ms -> 62ish fps, if vsync turned off

    GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
    GL::Renderer::enable(GL::Renderer::Feature::FaceCulling);

    m_drawingstate_dirty = false;
    m_sync = true;
    m_draw = true;

    // initialize simulation loop
    m_simulationloop = std::make_unique<SimulationLoop<SimulationType>>();
    // threadsafe synchronization of drawing state (from sim to struct)
    m_simulationloop->addTaskPeriodic([this](SimulationType &sim) {
      lock_guard<mutex> guard(m_statemutex);
      if (m_sync) { // dont event send the information if not required
        m_drawingstate.synchronize(sim);
        m_drawingstate_dirty = true;
      }
    });

    // start loop in a separate thread
    if (start_immediate)
      m_simulationloop->startThread();
  }

  ~GLApplicationBase() {
    lock_guard<mutex> guard(m_statemutex); // wait for sync to finish, then stop
                                           // sync which should allow for safe
                                           // destructions
    m_sync = false; // force stop sync such that drawingstate syncing cannot be
                    // caleld for possible already destructed state but loop
                    // still alive
  }

  void drawEvent() override {

    GL::defaultFramebuffer.clear(GL::FramebufferClear::Color |
                                 GL::FramebufferClear::Depth);

    if (m_sync)
      // blocking synchronization of simulation state into scene, over
      // intermediate drawingstate struct
      this->loadDrawingState();

    if (m_draw)
      m_scene.draw();

    {
      m_imgui
          .newFrame(); // windowSize(), GL::defaultFramebuffer.viewport().size()

      /* Enable text input, if needed */
      if (ImGui::GetIO().WantTextInput && !isTextInputActive())
        startTextInput();
      else if (!ImGui::GetIO().WantTextInput && isTextInputActive())
        stopTextInput();

      this->drawGUI();

      GL::Renderer::enable(GL::Renderer::Feature::Blending);
      GL::Renderer::disable(GL::Renderer::Feature::FaceCulling);
      GL::Renderer::disable(GL::Renderer::Feature::DepthTest);
      GL::Renderer::enable(GL::Renderer::Feature::ScissorTest);
      m_imgui.drawFrame();
      GL::Renderer::disable(GL::Renderer::Feature::ScissorTest);
      GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
      GL::Renderer::enable(GL::Renderer::Feature::FaceCulling);
      GL::Renderer::disable(GL::Renderer::Feature::Blending);
    }

    swapBuffers();

    redraw(); // calls itself in a loop
  }

protected:
  // simulation sync
  std::unique_ptr<SimulationLoop<SimulationType>> m_simulationloop;
  SceneType m_scene;
  DrawingStateType m_drawingstate;
  bool m_drawingstate_dirty;
  bool m_sync;
  bool m_draw;
  mutex m_statemutex;

  // gui
  ImGuiIntegration::Context m_imgui{NoCreate};
  virtual void drawGUI() {}

  void loadDrawingState() {
    {
      lock_guard<mutex> guard(m_statemutex);
      if (m_drawingstate_dirty) {

        m_scene.loadDrawingState(m_drawingstate);

        m_drawingstate_dirty = false;
      }
    }
  }

private:
  void viewportEvent(ViewportEvent &event) override {
    GL::defaultFramebuffer.setViewport({{}, event.framebufferSize()});
    m_imgui.relayout(Vector2{event.windowSize()} / event.dpiScaling(),
                     event.windowSize(), event.framebufferSize());
  }
  void keyPressEvent(KeyEvent &event) override {
    if (m_imgui.handleKeyPressEvent(event))
      return;
  }
  void keyReleaseEvent(KeyEvent &event) override {
    if (m_imgui.handleKeyReleaseEvent(event))
      return;
  }
  void mousePressEvent(MouseEvent &event) override {
    if (m_imgui.handleMousePressEvent(event))
      return;
  }
  void mouseReleaseEvent(MouseEvent &event) override {
    if (m_imgui.handleMouseReleaseEvent(event))
      return;
  }
  void mouseMoveEvent(MouseMoveEvent &event) override {
    if (m_imgui.handleMouseMoveEvent(event))
      return;
  }
  void mouseScrollEvent(MouseScrollEvent &event) override {
    if (m_imgui.handleMouseScrollEvent(event))
      return;
  }
  void textInputEvent(TextInputEvent &event) override {
    if (m_imgui.handleTextInputEvent(event))
      return;
  }
};

#endif /* end of include guard: _GLAPPLICATIONBASE_H_ */
