#ifndef _COLORMAP_H_
#define _COLORMAP_H_

#include <iostream>
#include <tuple>
#include <vector>
template <typename T> class ColorMap {
public:
  ColorMap() {}
  ColorMap(const std::vector<std::tuple<float, T>> &map) : m_map(map) {}

  ColorMap &addColor(float a, T col) {
    m_map.emplace_back(a, col);
    return *this;
  }

  // TODO: allow / handle float values outside of 0 .. 1
  // TODO: sort colormap

  T getColor(float a) {
    // empty map
    if (m_map.size() == 0)
      return T{0, 0, 0};

    // default to only entry
    if (m_map.size() == 1)
      return std::get<1>(m_map[0]);

    // clamp
    if (a <= std::get<0>(m_map[0]))
      return std::get<1>(m_map[0]);
    else if (a >= std::get<0>(m_map[m_map.size() - 1]))
      return std::get<1>(m_map[m_map.size() - 1]);

    // look up
    for (int i = 0; i < (int)m_map.size() - 1; ++i) {
      float a1 = std::get<0>(m_map[i]);
      float a2 = std::get<0>(m_map[i + 1]);
      if (a < a1 || a > a2) // not in current segment range
        continue;

      float a_01 = (a - a1) / (a2 - a1);
      return (1 - a_01) * std::get<1>(m_map[i]) +
             a_01 * std::get<1>(m_map[i + 1]);
    }

    // didnt find value / error
    return T{0, 0, 0};
  }

private:
  std::vector<std::tuple<float, T>> m_map;
};

#endif /* end of include guard: _COLORMAP_H_ */
