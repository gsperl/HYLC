#ifndef _DRAWABLEMESH_H_
#define _DRAWABLEMESH_H_

#include "../shaders/Phong2.h"
#include <Magnum/GL/Buffer.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/GL/Renderer.h> // outline
#include <Magnum/Magnum.h>
#include <Magnum/MeshTools/Interleave.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Object.h>
#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/Shaders/Flat.h>
#include <Magnum/Shaders/Generic.h>
#include <vector>
using namespace std;

namespace Magnum {
namespace SceneGraph { // subnamespace?
typedef Object<SceneGraph::MatrixTransformation3D> Object3D;
typedef Camera3D Camera3D;

// template<typename Shader>
class DrawableMesh : public Object3D, public Drawable3D { // rename to
  // DrawableMesh<ShaderType,PrimitiveType?>
public:
  explicit DrawableMesh(Object3D *parent, DrawableGroup3D *group,
                        Phong2 *shader, Phong2Material *material)
      : Object3D{parent}, SceneGraph::Drawable3D{*this, group} {
    this->m_shader = shader;
    this->m_material = material;
    m_mesh
        // .addVertexBuffer(m_vertexBuffer, 0, Phong2::Position{},
        .addVertexBuffer(m_vertexBuffer, 0, Phong2::Position{},
                         Phong2::Normal{}, Phong2::Color{})
        .setIndexBuffer(m_indexBuffer, 0, GL::MeshIndexType::UnsignedInt)
        .setPrimitive(GL::MeshPrimitive::Triangles);
  }

  void draw(const Matrix4 &transformationMatrix, Camera3D &camera) override {
    (*m_shader)
        .setTransformationMatrix(transformationMatrix)
        .setNormalMatrix(transformationMatrix.rotation()) // assuming no scaling
        .setProjectionMatrix(camera.projectionMatrix())
        .setMaterial(*m_material);

    if (wireframe)
      GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Line);
    m_mesh.draw(*m_shader);
    if (wireframe)
      GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);
  }

  void setIndices(const vector<UnsignedInt> &indices) {
    this->m_indices = indices;
    m_mesh.setCount(this->m_indices.size());
    m_indexBuffer.setData(this->m_indices, GL::BufferUsage::DynamicDraw);
  }

  void setVertices(vector<Vector3> &vertices, vector<Color3> &colors,
                   bool recalculateNormals = false) {
    this->m_vertices = vertices;
    if (recalculateNormals)
      this->recalculateNormals();
    this->m_colors = colors; // TODO need to store at all?

    m_vertexBuffer.setData(
        // MeshTools::interleave(this->m_vertices, this->m_normals),
        MeshTools::interleave(this->m_vertices, this->m_normals,
                              this->m_colors),
        GL::BufferUsage::DynamicDraw);
  }

  GL::Mesh &getMesh() { return m_mesh; }

  bool wireframe = false;

private:
  Phong2 *m_shader;
  Phong2Material *m_material;
  GL::Buffer m_indexBuffer, m_vertexBuffer;
  GL::Mesh m_mesh;

  vector<Vector3> m_vertices;
  vector<Color3> m_colors;
  vector<Vector3> m_normals;
  vector<UnsignedInt> m_indices;

  void recalculateNormals() {

    if (m_normals.size() != m_vertices.size())
      m_normals.resize(m_vertices.size());

    // reset normals to 0
    for (auto &n : m_normals)
      n = Vector3(0, 0, 0);

    assert(m_indices.size() % 3 == 0);
    // for each triangle calculate normal * area
    // add to each vertex of the triangle
    for (size_t i = 0; i < m_indices.size(); i += 3) {
      auto ix0 = m_indices[i + 0];
      auto ix1 = m_indices[i + 1];
      auto ix2 = m_indices[i + 2];
      auto v0 = m_vertices[ix0];
      auto v1 = m_vertices[ix1];
      auto v2 = m_vertices[ix2];
      auto t1 = v0 - v1;
      auto t2 = v2 - v1;
      auto n = cross(t2, t1); // order!
      m_normals[ix0] += n;
      m_normals[ix1] += n;
      m_normals[ix2] += n;
    }

    // normalize all
    for (auto &n : m_normals)
      n = n.normalized();
  }
};

} // namespace SceneGraph
} // namespace Magnum
#endif /* end of include guard: _DRAWABLEMESH_H_ */
