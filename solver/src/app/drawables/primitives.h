#ifndef _DRAWABLEPRIMITIVES_H_
#define _DRAWABLEPRIMITIVES_H_

#include <Magnum/GL/Buffer.h>
#include <Magnum/Magnum.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/GL/Renderer.h>
#include <Magnum/MeshTools/Interleave.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Object.h>
#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/Shaders/Flat.h>
#include <Magnum/Shaders/VertexColor.h>
#include <numeric> // iota
#include <vector>
using namespace std;

namespace Magnum {
namespace SceneGraph {
typedef Object<SceneGraph::MatrixTransformation3D> Object3D;
typedef Camera3D Camera3D;

template <GL::MeshPrimitive primitive, typename Shader>
class DrawablePrimitivesBase3D : public Object3D, public Drawable3D {
public:
  explicit DrawablePrimitivesBase3D(
      Object3D *parent, DrawableGroup3D *group,
      Shader *shader, /*MeshPrimitive primitive,*/
      GL::BufferUsage vertexBufferUsage = GL::BufferUsage::StaticDraw,
      GL::BufferUsage indexBufferUsage  = GL::BufferUsage::StaticDraw)
      : Object3D{parent}, Drawable3D{*this, group},
        m_indexBufferUsage(indexBufferUsage),
        m_vertexBufferUsage(vertexBufferUsage) {
    this->m_shader = shader;
    m_mesh.setIndexBuffer(m_indexBuffer, 0, GL::MeshIndexType::UnsignedInt)
        .setPrimitive(primitive);
  }

  void setIndices(const vector<UnsignedInt> &indices) {
    this->m_indices = indices;
    m_mesh.setCount(this->m_indices.size());
    m_indexBuffer.setData(this->m_indices, m_indexBufferUsage);
  }

  void draw(const Matrix4 &transformationMatrix, Camera3D &camera) {
    (*m_shader).setTransformationProjectionMatrix(camera.projectionMatrix() *
                                                  transformationMatrix);

    switch (primitive) {
    case GL::MeshPrimitive::Points:
      GL::Renderer::setPointSize(m_size);
      break;
    case GL::MeshPrimitive::LineStrip:
    case GL::MeshPrimitive::LineLoop:
    case GL::MeshPrimitive::Lines:
    case GL::MeshPrimitive::LineStripAdjacency:
    case GL::MeshPrimitive::LinesAdjacency:
      GL::Renderer::setLineWidth(m_size);
      break;
    default:
      break;
    }

    m_mesh.draw(*m_shader);
  }

  float m_size = 1.0f;

protected:
  GL::BufferUsage m_indexBufferUsage, m_vertexBufferUsage;
  GL::Buffer m_indexBuffer, m_vertexBuffer;
  Shader *m_shader;
  GL::Mesh m_mesh;

  void recalculateIndices(int n) {
    m_indices.resize(n);
    std::iota(std::begin(m_indices), std::end(m_indices),
              0); // increasing integer values
    m_mesh.setCount(this->m_indices.size());
    m_indexBuffer.setData(this->m_indices, m_indexBufferUsage);
  }

private:
  vector<UnsignedInt> m_indices;
};

template <GL::MeshPrimitive primitive>
class DrawablePrimitivesUniformColor3D
    : public DrawablePrimitivesBase3D<primitive, Shaders::Flat3D> {
public:
  explicit DrawablePrimitivesUniformColor3D(
      Object3D *parent, DrawableGroup3D *group,
      Shaders::Flat3D *shader, /*MeshPrimitive primitive,*/
      GL::BufferUsage vertexBufferUsage = GL::BufferUsage::StaticDraw,
      GL::BufferUsage indexBufferUsage  = GL::BufferUsage::StaticDraw)
      : DrawablePrimitivesBase3D<primitive, Shaders::Flat3D>(
            parent, group, shader, vertexBufferUsage, indexBufferUsage) {
    this->m_color = Color3{1};
    this->m_mesh.addVertexBuffer(this->m_vertexBuffer, 0,
                                 Shaders::Flat3D::Position{});
  }

  void draw(const Matrix4 &transformationMatrix, Camera3D &camera) {
    // set up uniform color
    (*this->m_shader).setColor(m_color);
    // call base class drawing
    DrawablePrimitivesBase3D<primitive, Shaders::Flat3D>::draw(
        transformationMatrix, camera);
  }

  void setVertices(const vector<Vector3> &vertices,
                   bool recalculateIndices = false) {
    this->m_vertices = vertices;
    this->m_vertexBuffer.setData(this->m_vertices, this->m_vertexBufferUsage);

    if (recalculateIndices)
      this->recalculateIndices(this->m_vertices.size());
  }

  Color3 m_color;

private:
  vector<Vector3> m_vertices;
};

template <GL::MeshPrimitive primitive>
class DrawablePrimitivesVertexColor3D
    : public DrawablePrimitivesBase3D<primitive, Shaders::VertexColor3D> {
public:
  struct Vertex {
    Vector3 position;
    Color3 color;
  };

  explicit DrawablePrimitivesVertexColor3D(
      Object3D *parent, DrawableGroup3D *group, Shaders::VertexColor3D *shader,
      GL::BufferUsage vertexBufferUsage = GL::BufferUsage::StaticDraw,
      GL::BufferUsage indexBufferUsage  = GL::BufferUsage::StaticDraw)
      : DrawablePrimitivesBase3D<primitive, Shaders::VertexColor3D>(
            parent, group, shader, vertexBufferUsage, indexBufferUsage) {
    this->m_mesh.addVertexBuffer(
        this->m_vertexBuffer, 0, Shaders::VertexColor3D::Position{},
        Shaders::VertexColor3D::Color3{
            Shaders::VertexColor3D::Color3::Components::Three});
  }

  void setVertices(const vector<Vector3> &vertices,
                   const vector<Color3> &colors,
                   bool recalculateIndices = false) {

    assert(vertices.size() == colors.size());

    this->m_vertices.resize(vertices.size());
    for (size_t i = 0; i < vertices.size(); ++i) {
      this->m_vertices[i].position = vertices[i];
      this->m_vertices[i].color    = colors[i];
    }
    this->m_vertexBuffer.setData(this->m_vertices, this->m_vertexBufferUsage);

    if (recalculateIndices)
      this->recalculateIndices(this->m_vertices.size());
  }

private:
  vector<Vertex> m_vertices;
};

typedef DrawablePrimitivesUniformColor3D<GL::MeshPrimitive::Points>
    DrawablePoints3D;
typedef DrawablePrimitivesVertexColor3D<GL::MeshPrimitive::Points>
    DrawablePointsVC3D;
typedef DrawablePrimitivesUniformColor3D<GL::MeshPrimitive::Lines> DrawableLines3D;
typedef DrawablePrimitivesVertexColor3D<GL::MeshPrimitive::Lines> DrawableLinesVC3D;
typedef DrawablePrimitivesUniformColor3D<GL::MeshPrimitive::LineLoop>
    DrawableLineLoop3D;
typedef DrawablePrimitivesVertexColor3D<GL::MeshPrimitive::LineLoop>
    DrawableLineLoopVC3D;
typedef DrawablePrimitivesUniformColor3D<GL::MeshPrimitive::LineStrip>
    DrawableLineStrip3D;
typedef DrawablePrimitivesVertexColor3D<GL::MeshPrimitive::LineStrip>
    DrawableLineStripVC3D;
typedef DrawablePrimitivesUniformColor3D<GL::MeshPrimitive::Triangles>
    DrawableTriangles3D;
typedef DrawablePrimitivesVertexColor3D<GL::MeshPrimitive::Triangles>
    DrawableTrianglesVC3D;
}
}
#endif /* end of include guard: _DRAWABLEPRIMITIVES_H_ */
