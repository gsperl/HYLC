#ifndef _GUI_H_
#define _GUI_H_

#include "../../simulation/statics/Simulation.h"
#include "../ImGuiAddon.h"
#include "../imgui_stdlib/imgui_stdlib.h"

// ----- Individual Settings------------------------------------------------ //

// void drawLBFGSSettings(LBFGS<scalar>::Params &settings) {
//   ImGui::DragInt("m", &settings.m, 1, 3, 100);
//   ImGui::DragInt("max_skips", &settings.max_skips, 1, 0, 100);
//   ImGui::DragDouble("update threshold", &settings.bad_update_epsilon, 1e5,
//   0.0f,
//                     1.0f);
//   ImGui::DragDouble("damping", &settings.theta, 0.01f, 0.0f, 1.0f);
// }

void drawSolverSettings(NewtonSolverSettings &settings) {
  ImGui::ItemWidthGuard iwg(100.0f);
  ImGui::DragDouble("reg start", &settings.linear_reg_start, 0.1f, 0.0f, 1.0f,
                    "%.2e");
  ImGui::DragDouble("reg end", &settings.linear_reg_stop, 0.1f, 0.0f, 1.0f,
                    "%.2e");
  ImGui::DragInt("reg steps", &settings.linear_reg_steps, 1, 1, 1000);

  ImGui::DragDouble("stop: |z| < eps * |z0|", &settings.projgrad_epsilon, 0.1f,
                    0.0f, 1.0f, "%.2e");
  ImGui::DragDouble("stop: |Cq-d|/N < eps", &settings.lagrange_epsilon, 0.1f,
                    0.0f, 1.0f, "%.2e");
  ImGui::DragInt("linesearch max steps", &settings.linesearch_steps, 1, 1, 20);
  ImGui::DragDouble("linesearch factor", &settings.linesearch_mult, 0.01f, 0.0f,
                    1.0f, "%.2e");
}

void drawSolvableSettings(SolvableSettings &settings) {
  ImGui::ItemWidthGuard iwg(100.0f);
  ImGui::DragDouble("step factor", &settings.step_factor, 1.0f, 0.0f, 1.0f,
                    "%.2f", 10.0f);
  ImGui::DragDouble("unit Nm", &settings.unit_Nm, 1.0f, 0.0f, 1e15, "%e",
                    10.0f);
  ImGui::DragDouble("unit pos(*r)", &settings.unit_pos, 1.0f, 0.0f, 1.0f,
                    "%.2f", 10.0f);
  ImGui::DragDouble("unit twist", &settings.unit_rad, 1.0f, 0.0f, 1.0f, "%.2f",
                    10.0f);
  ImGui::DragDouble("constr scale", &settings.constraint_scale, 1.0f, 0.0f,
                    10.0f, "%.2f", 10.0f);
  ImGui::Checkbox("ignore sliding constraint",
                  &settings.disable_slide_constraint);
}

void drawQuickSettings(SimulationSettings &settings) {
  ImGui::Text("Other");
  ImGui::SameLine();
  if (ImGui::HueButton(125, "...")) {
    settings.extpx = 0.6;
    settings.extpy = 0.6;
    settings.gridResolution = 0.25;
    settings.newtonSettings.linear_reg_start = 3e3;
    settings.newtonSettings.linear_reg_stop = 2e0;
    settings.newtonSettings.linear_reg_steps = 400;
    settings.newtonSettings.linesearch_mult = 0.5;
    settings.newtonSettings.linesearch_steps = 13;//10;
    settings.newtonSettings.lagrange_epsilon = 1e-04;
    settings.newtonSettings.projgrad_epsilon = 1e-06;//5e-05;
    settings.solvableSettings.constraint_scale = 1.0;
    settings.solvableSettings.step_factor = 0.3;
    settings.solvableSettings.unit_Nm = 1e-05;
    settings.solvableSettings.unit_pos = 1.0;
    settings.solvableSettings.unit_rad = 1.0;//0.01;
  }

  ImGui::Text("Mat");
  ImGui::SameLine();
  if (ImGui::HueButton(125, "B")) {
    settings.material.yE      = 1e5;
    settings.material.G       = 4e4;
    settings.material.gamma   = 0.1;
    settings.material.kc      = 1.2e1;
    settings.material.density = 1.2e2;
  }
  ImGui::SameLine();
  if (ImGui::HueButton(125, "H")) {
    settings.material.yE      = 5e5;
    settings.material.G       = 2e5;
    settings.material.gamma   = 0.1;
    settings.material.kc      = 6e1;
    settings.material.density = 1.2e2;
  }
  ImGui::SameLine();
  if (ImGui::HueButton(125, "R")) {
    settings.material.yE      = 5e5;
    settings.material.G       = 2e5;
    settings.material.gamma   = 0.001;
    settings.material.kc      = 6e1;
    settings.material.density = 6e1;
  }
  ImGui::SameLine();
  if (ImGui::HueButton(125, "Sa")) {
    settings.material.yE      = 1e5;
    settings.material.G       = 4e4;
    settings.material.gamma   = 0.1;
    settings.material.kc      = 1.2e1;
    settings.material.density = 1.2e2;
  }
  ImGui::SameLine();
  if (ImGui::HueButton(125, "St")) {
    settings.material.yE      = 5e5;
    settings.material.G       = 2e5;
    settings.material.gamma   = 0.001;
    settings.material.kc      = 6e1;
    settings.material.density = 1.2e2;
    settings.extpx            = 0.4;
    settings.extpy            = 0.6;
  }
}

// ----- Combined Settings-------------------------------------------------- //

void drawSimulationSettings(SimulationSettings &settings) {
  {
    std::string &str = settings.pypfile;
    size_t size      = 1024;
    char str0[size];
    strncpy(str0, str.c_str(), sizeof(str0));

    if (ImGui::InputText("PatternData File", str0, size)) {
      str = str0;
    }
  }

  if (ImGui::TreeNode("Quick Settings")) {
    drawQuickSettings(settings);
    ImGui::TreePop();
  }

  if (ImGui::TreeNode("Material")) {
    ImGui::ItemWidthGuard iwg(100.0f);
    ImGui::DragDouble("kc", &settings.material.kc, 1.0f, 0.0f, 1e15, "%e",
                      10.0f);
    ImGui::DragDouble("yE", &settings.material.yE, 1.0f, 0.0f, 1e15, "%.1e",
                      10.0f);
    ImGui::DragDouble("G", &settings.material.G, 1.0f, 0.0f, 1e15, "%.1e",
                      10.0f);
    ImGui::DragDouble("gamma", &settings.material.gamma, 0.01f, 0.0f, 1.0f,
                      "%.2f");
    ImGui::DragDouble("density", &settings.material.density, 1.0f, 0.0f, 1e15,
                      "%.1e", 10.0f);
    ImGui::TreePop();
  }

  if (ImGui::TreeNode("Solver")) {
    drawSolverSettings(settings.newtonSettings);
    ImGui::TreePop();
  }

  if (ImGui::TreeNode("Solvable")) {
    drawSolvableSettings(settings.solvableSettings);
    ImGui::TreePop();
  }

  if (ImGui::TreeNode("Expansion")) {
    ImGui::ItemWidthGuard iwg(100.0f);
    ImGui::DragDouble("extend px", &settings.extpx, 0.01f, -0.1f, 2.0f);
    ImGui::DragDouble("extend py", &settings.extpy, 0.01f, -0.1f, 2.0f);
    ImGui::DragDouble("sx", &settings.strains(0), 0.01f, -0.9f, 10.0f);
    ImGui::DragDouble("sa", &settings.strains(1), 0.01f, 0.0f, 10.0f);
    ImGui::DragDouble("sy", &settings.strains(2), 0.01f, -0.9f, 10.0f);
    ImGui::DragDouble("II00", &settings.strains(3), 0.01f, 0.0f, 10.0f);
    ImGui::DragDouble("II01", &settings.strains(4), 0.01f, 0.0f, 10.0f);
    ImGui::DragDouble("II11", &settings.strains(5), 0.01f, 0.0f, 10.0f);
    ImGui::DragDouble("grid res", &settings.gridResolution, 0.1f, 0.1f, 10.0f);
    ImGui::TreePop();
  }
}

#endif /* end of include guard: _GUI_H_ */
