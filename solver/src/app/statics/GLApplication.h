#ifndef _GLAPPLICATION_H_
#define _GLAPPLICATION_H_
#include <unistd.h>

#include "../../simulation/statics/Simulation.h"
#include "../appbase/GLApplicationBase.h"
#include "../imgui_stdlib/imgui_stdlib.h"
#include "GUI.h"
#include "Scene.h"
#include "SimulationDrawingState.h"
#include <Magnum/GL/Version.h>

class GLApplication
    : public GLApplicationBase<Simulation, Scene, SimulationDrawingState> {
public:
  explicit GLApplication(const Arguments &arguments);

private:
  typedef GLApplicationBase<Simulation, Scene, SimulationDrawingState> Base;

  void keyPressEvent(KeyEvent &event) override;
  void keyReleaseEvent(KeyEvent &event) override;
  void mousePressEvent(MouseEvent &event) override;
  void mouseReleaseEvent(MouseEvent &event) override;
  void mouseMoveEvent(MouseMoveEvent &event) override;
  void mouseScrollEvent(MouseScrollEvent &event) override;

  void drawGUI() override;
  void drawGUI_MenuBar();
  void drawGUI_Rendering();
  void drawGUI_Output();

  // lambda wrappers?
  void singleStep();
  void pauseSimulation();
  void resetSimulation();
  void serialize_simulation_state(const std::string &file);

  // gui
  int gui_capFPS = 0;
  std::string gui_output_file = "../pyp/out.pyp";
  int gui_sim_ms = 1;

  // vars for modifying reststate
  float RLscale=0.9, RKscale=0.8, RTscale=1.0;

  std::vector<float> gui_Eplot;
  float last_E=0,last_A=1.0;
  int gui_Eplotix;

  SimulationSettings m_simsettings;

  // input
  Magnum::Vector2i m_previousMousePosition;
};

#endif /* end of include guard: _GLAPPLICATION_H_ */
