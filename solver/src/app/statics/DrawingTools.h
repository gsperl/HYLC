#ifndef _DRAWINGTOOLS_H_
#define _DRAWINGTOOLS_H_

#include "../ColorMap.h"
#include "SimulationDrawingState.h"

#include <vector>

#include <Magnum/Magnum.h>
#include <Magnum/Math/Color.h>
#include <Magnum/Math/Vector3.h>
using namespace Magnum;
using namespace Magnum::Math::Literals;

void drawGrid(const MatrixXXs &grid, int rows, int cols,
              std::vector<Vector3> &pts, std::vector<Color3> &clrs,
              std::vector<UnsignedInt> &ixs) {

  int offix_pts = pts.size();
  // int offix_ix = ixs.size();

  pts.resize(pts.size() + rows * cols);
  clrs.resize(clrs.size() + rows * cols);
  // ixs.resize(ixs.size() + (rows-1)*cols + (cols-1)*rows);
  for (int i = 0; i < rows; ++i) {
    for (int j = 0; j < cols; ++j) {
      int ix = i * cols + j;

      pts[offix_pts + ix] = {(float)grid(ix, 0), (float)grid(ix, 1),
                             (float)grid(ix, 2)};
      // pts[offix_pts + ix] = {j*0.01,i*0.01,0};
      clrs[offix_pts + ix] = Color3{0.5, 0.5, 0.5};

      if (i < rows - 1) {
        int ixT = (i + 1) * cols + j;
        ixs.push_back(offix_pts + ix);
        ixs.push_back(offix_pts + ixT);
      }

      if (j < cols - 1) {
        int ixR = i * cols + j + 1;
        ixs.push_back(offix_pts + ix);
        ixs.push_back(offix_pts + ixR);
      }
    }
  }
}

// per yarn
// note startvertex
// -> for each segment: build 2 circlegons and push them back
// -> iterate pairs of circlegons and connect
// triangulate ends
void gen_circles(const SimulationDrawingState::RodDrawingState &rds,
         std::vector<Vector3> &pts, std::vector<Color3> &clrs, int n,
         ColorMap<Color3> &cm) {
  double PI2_n = 2 * M_PI / n;
  pts.resize(n * (rds.vertices.size() * 2 - 2));
  clrs.resize(n * (rds.vertices.size() * 2 - 2));
  scalar rfactor = Debug::get<float>("render:radius",1.0);

  // prepare circle vtxs at each yarn edges start and end vtx
  //   by making circle in plane accoring to n and bn
  // offset the circle by epsilon in tangent directions at start and end of
  // edge for connectivity
  for (int i = 0; i < (int)rds.vertices.size() - 1; i++) {
    // auto eps = 0.01 * (rds.vertices[i + 1] - rds.vertices[i]);
    auto eps = 0.025 * (rds.vertices[i + 1] - rds.vertices[i]);
    for (int ci = 0; ci < n; ci++) {
      double theta = ci * PI2_n;
      auto vtx = rfactor *
                 (cos(theta) * rds.frame_n[i] + sin(theta) * rds.frame_bn[i]);
      pts[i * 2 * n + 0 * n + ci] = (vtx + rds.vertices[i] + eps);
      pts[i * 2 * n + 1 * n + ci] = (vtx + rds.vertices[i + 1] - eps);

      Vector3 clr;
      {
        float a = ci * 1.0 / (n - 1);
        clr = cm.getColor(a);

        // FLAT SHADING via color. TODO use actual phong2+clr shader?
        Vector3 n = vtx.normalized();
        float d = Math::dot(n, Vector3{1, -0.5, 1}.normalized());
        // float diffuse = std::max(0.0f, d);
        float diffuse = std::max(0.5f*(d+1), 0.1f);
        clr = clr * diffuse + (1 - diffuse) * (clr * Vector3{0.3f, 0.25f, 0.4f});
      }

      if (rds.periodic[i]) {
        // darken and desaturate periodic
        clr *= 0.66;
        clr = 0.75 * clr + 0.25 * Vector3{1, 1, 1} * clr.length();
      }
      clrs[i * 2 * n + 0 * n + ci] = Vector3{clr[0], clr[1], clr[2]};
      clrs[i * 2 * n + 1 * n + ci] = Vector3{clr[0], clr[1], clr[2]};
    }
  }
}

void connect_circles(const SimulationDrawingState::RodDrawingState &rds,
         std::vector<Vector3> &pts, std::vector<Color3> &clrs,
         std::vector<UnsignedInt> &ixs, int n, UnsignedInt ixoffset, bool disconnected) {
  // CONNECT & TRIANGULATE QUADS
  for (int ei = 0; ei < (int)rds.vertices.size() - 1; ei++) {
    std::vector<int> offsets =
        (ei == 0 || disconnected) ? std::vector<int>{2 * n * ei}
                              : std::vector<int>{2 * n * ei, 2 * n * ei - n};
    for (int offe : offsets)
      for (int ci = 0; ci < n; ++ci) {
        int ix1 = ci;
        int ix2 = ci + n;
        int ix3 = (ci + 1) % n;
        int ix4 = (ci + 1) % n + n;

        ixs.push_back(ixoffset + offe + ix1);
        ixs.push_back(ixoffset + offe + ix2);
        ixs.push_back(ixoffset + offe + ix3);

        ixs.push_back(ixoffset + offe + ix2);
        ixs.push_back(ixoffset + offe + ix4);
        ixs.push_back(ixoffset + offe + ix3);
      }
  }

  // Connect tip faces
  if (!disconnected) {
    Vector3 avg = {0, 0, 0};
    Vector3 avgc = {0, 0, 0};
    for (int ci = 0; ci < n; ++ci) {
      avg += pts[ci];
      avgc += clrs[ci];
    }
    avg /= n;
    avgc /= n;
    pts.push_back(avg);
    clrs.push_back(avgc);

    int ix3 = (int)pts.size() - 1;
    for (int ci = 0; ci < n; ++ci) {
      int ix1 = ci;
      int ix2 = (ci + 1) % n;
      ixs.push_back(ixoffset + ix1);
      ixs.push_back(ixoffset + ix3);
      ixs.push_back(ixoffset + ix2);
    }
  }
  if (!disconnected) {
    int offe = 2 * n * ((int)rds.vertices.size() - 2) + n;
    Vector3 avg = {0, 0, 0};
    Vector3 avgc = {0, 0, 0};
    for (int ci = 0; ci < n; ++ci) {
      avg += pts[offe + ci];
      avgc += clrs[offe + ci];
    }
    avg /= n;
    avgc /= n;
    pts.push_back(avg);
    clrs.push_back(avgc);

    int ix3 = (int)pts.size() - 1;
    for (int ci = 0; ci < n; ++ci) {
      int ix1 = offe + ci;
      int ix2 = offe + (ci + 1) % n;
      ixs.push_back(ixoffset + ix1);
      ixs.push_back(ixoffset + ix2);
      ixs.push_back(ixoffset + ix3);
    }
  }
}

void drawRod3D(const SimulationDrawingState::RodDrawingState &rds,
               std::vector<Vector3> &pts, std::vector<Color3> &clrs,
               std::vector<UnsignedInt> &ixs, ColorMap<Color3> &cm, bool disconnected) {

  // for a yarn
  int n = 32; // 6; // points per circle
  // if (Debug::get<bool>("beautymode")) {
  //   n = 32;
  // }
  // allocate vtx array for n * numvtx  -2 points
  // 2 per vtx except start and end
  int c = pts.size();
  // pts.reserve(c + n * (rds.vertices.size() * 2 - 2));

  std::vector<Vector3> new_pts;
  std::vector<Color3> new_clrs;
  gen_circles(rds, new_pts, new_clrs, n, cm);

  std::vector<UnsignedInt> new_ixs;
  connect_circles(rds, new_pts, new_clrs, new_ixs, n, c, disconnected);

  pts.insert(pts.end(), new_pts.begin(), new_pts.end());
  clrs.insert(clrs.end(), new_clrs.begin(), new_clrs.end());
  ixs.insert(ixs.end(), new_ixs.begin(), new_ixs.end());
}

#endif /* end of include guard: _DRAWINGTOOLS_H_ */

/*

  void drawRodLine(const SimulationDrawingState::RodDrawingState &rds,
                   std::vector<Vector3> &pts, std::vector<Color3> &clrs,
                   std::vector<UnsignedInt> &ixs) {

    int c = pts.size();

    // center line
    for (size_t i = 0; i < rds.vertices.size() - 1; i++) {

      pts.emplace_back(rds.vertices[i][0], rds.vertices[i][1],
                       rds.vertices[i][2]);
      clrs.emplace_back(0.8, 0.7, 0.2);

      ixs.emplace_back(c + 0);
      pts.emplace_back(rds.vertices[i + 1][0], rds.vertices[i + 1][1],
                       rds.vertices[i + 1][2]);
      clrs.emplace_back(0.8, 0.7, 0.2);
      ixs.emplace_back(c + 1);
      c += 2;
    }

    // ADD material frames
    float framescale = 1.0; // 0.1;
    for (size_t i = 0; i < rds.vertices.size() - 1; i++) {
      auto x = 0.5 * (rds.vertices[i] + rds.vertices[i + 1]);
      pts.push_back(x);
      pts.push_back(x + framescale * rds.frame_n[i]);
      clrs.emplace_back(0, 0.8, 0.3);
      clrs.emplace_back(0, 0.8, 0.3);
      ixs.emplace_back(c + 0);
      ixs.emplace_back(c + 1);
      c += 2;
      pts.push_back(x);
      pts.push_back(x + framescale * rds.frame_bn[i]);
      clrs.emplace_back(0, 0.3, 0.8);
      clrs.emplace_back(0, 0.3, 0.8);
      ixs.emplace_back(c + 0);
      ixs.emplace_back(c + 1);
      c += 2;
    }
  }

  void drawRod3DLine(const SimulationDrawingState::RodDrawingState &rds,
                     std::vector<Vector3> &pts, std::vector<Color3> &clrs,
                     std::vector<UnsignedInt> &ixs) {
    // for a yarn
    int n = 6; // points per circle
    double PI2_n = 2 * M_PI / n;
    // allocate vtx array for n * numvtx  -2 points
    // 2 per vtx except start and end
    int c = pts.size();
    pts.reserve(c + n * (rds.vertices.size() * 2 - 2));

    // prepare circle vtxs at each yarn edges start and end vtx
    //   by making circle in plane accoring to n and bn
    // offset the circle by epsilon in tangent directions at start and end of
    // edge for connectivity
    for (size_t i = 0; i < rds.vertices.size() - 1; i++) {

      auto eps = 0.01 * (rds.vertices[i + 1] - rds.vertices[i]);
      for (int ci = 0; ci < n; ci++) {
        double theta = ci * PI2_n;
        auto vtx = cos(theta) * rds.frame_n[i] + sin(theta) * rds.frame_bn[i];
        pts.push_back(vtx + rds.vertices[i] + eps);
        pts.push_back(vtx + rds.vertices[i + 1] - eps);
        float a = ci * 1.0 / (n - 1);
        Vector3 clr =
            (1 - a) * Vector3{0.8, 0.7, 0.2} + a * Vector3{0.2, 0.2, 0.8};
        clr = clr.normalized() * 0.8;
        clrs.emplace_back(clr[0], clr[1], clr[2]);
        clrs.emplace_back(clr[0], clr[1], clr[2]);
      }
    }

    // make list of triangles, by quad-connecting subsequent circles based on
    // ordering, give color based on ordering

    for (int i = 0; i < n * ((int)rds.vertices.size() * 2 - 2) - 1; i++) {
      ixs.push_back(c + i);
      ixs.push_back(c + i + 1);
    }
  }

*/
