#ifndef _SCENE_H_
#define _SCENE_H_

#include <Magnum/Magnum.h>

#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/Renderer.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/Shaders/Flat.h>
#include <Magnum/Shaders/VertexColor.h>
#include "../../simulation/statics/Simulation.h"
#include "../ColorMap.h"
#include "../camera/camera.h"
#include "../drawables/mesh.h"
#include "../drawables/primitives.h"
#include "../shaders/Phong2.h"
#include "DrawingTools.h"
#include "SimulationDrawingState.h"

#include <math.h>
#include <random>

using namespace Magnum;
using namespace Magnum::Math::Literals;

#ifndef PI
#define PI 3.14159265358979
#endif

typedef SceneGraph::Scene<SceneGraph::MatrixTransformation3D> Scene3D;
typedef SceneGraph::Object<SceneGraph::MatrixTransformation3D> Object3D;

class Scene {
 public:
  Scene() {
    Debug::get<bool>("drawaxis") = true;

    sg_object_camera       = new Object3D(&sg_root);
    sg_visual_scale_object = new Object3D(&sg_root);
    sg_visual_scale_object->scale({100, 100, 100});  // scale visually cm to m
    sg_camera            = new SceneGraph::Camera3D(*sg_object_camera);
    m_coordinateRotation = Matrix4::rotationX(-90.0_degf);
    resetCamera();

    Matrix3 iso_move({1, 0, -1}, {0, 1, 0}, {1, 0, 1});
    (*(sg_object_light_main = new Object3D(&sg_root)))
        .translate(iso_move * Vector3{20, 40, 20});
    (*(sg_object_light_rim = new Object3D(&sg_root)))
        .translate(iso_move * Vector3{-20, -20, -10});

    m_material.shininess  = 80.0f;  // 80
    m_material.shininess2 = 50.0f;  // 30
    m_lights.ambientLight = Color3{0.3, 0.3, 0.3};
    m_lights.lightColor1  = Color3{1, 1, 1};
    m_lights.lightColor2  = Color3{0, 0.2, 1};
    m_shader.setLights(m_lights);

    m_points = new SceneGraph::DrawablePointsVC3D(
        sg_visual_scale_object, &sg_drawables, &m_shader_vertexcolor,
        GL::BufferUsage::DynamicDraw, GL::BufferUsage::DynamicDraw);
    m_lines = new SceneGraph::DrawableLinesVC3D(
        sg_visual_scale_object, &sg_drawables, &m_shader_vertexcolor,
        GL::BufferUsage::DynamicDraw, GL::BufferUsage::DynamicDraw);
    m_triangles = new SceneGraph::DrawableTrianglesVC3D(
        sg_visual_scale_object, &sg_drawables, &m_shader_vertexcolor,
        GL::BufferUsage::DynamicDraw, GL::BufferUsage::DynamicDraw);

    m_mesh = new SceneGraph::DrawableMesh(
        sg_visual_scale_object, &sg_drawables, &m_shader,
        &m_material);  // all pointers, with automatic cleanup from scenegraph

    cm_falsecolors = ColorMap<Color3>({{0.0, {0, 0, 1}},
                                       {0.25, {0, 1, 1}},
                                       {0.5, {0, 1, 0}},
                                       {0.75, {1, 1, 0}},
                                       {1.0, {1, 0, 0}}});

    Color3 c1{0.6, 0.4, 0.4};
    Color3 c2{0.4, 0.55, 0.45};
    Color3 c3{0.4, 0.4, 0.6};
    Color3 tint{0.3, 0.6, 0.5};
    float t = 0.3;
    c1      = (1 - t) * c1 + t * tint;
    c2      = (1 - t) * c2 + t * tint;
    c3      = (1 - t) * c3 + t * tint;

    cm_twist = ColorMap<Color3>({{0, c1},
                                 {0.333, c1},
                                 {0.334, c2},
                                 {0.666, c2},
                                 {0.667, c3},
                                 {0.999, c3},
                                 {1.0, c1}});
    cm_twist_white = ColorMap<Color3>(
        {{0, Color3{0.9, 0.9, 0.9}}, {1.0, Color3{0.9, 0.9, 0.9}}});
  }

  void resetCamera() {
    // SphericalCamera(double r0, double phi0, double theta0, Magnum::Vector3
    // target,
    //                 double speed = 1.0, bool perspective = true);
    m_camera =
        SphericalCamera(1.0, -3.14159265897935 * 0.25 - 0.3,
                        3.14159265897935 * 0.125, Vector3(0, 0, 0), 4.0, true);
    m_camera.m_fov  = 75.0_degf;
    m_camera.m_near = 0.01;  // default is 0.01
  }

  int draw3Drods = 2;  // 0 is line, 1 is lines around thickness, 2 is tris
  void loadDrawingState(const SimulationDrawingState &state) {
    if (render_yarns) {  // set GL points
      vector<Vector3> pts;
      vector<Color3> clrs;
      for (auto &rds : state.rod_drawingstates) {
        // auto alpha = Debug::get<double>("alpha (dq)");
        // auto beta = Debug::get<double>("beta (-grad)");
        for (int i = 0; i < (int)rds.vertices.size(); i++) {
          // auto dpos = alpha * rds.dq[i] + beta * rds.grad[i];
          pts.push_back(rds.vertices[i]);  // + dpos);
          if (i == 0 || i == (int)rds.vertices.size() - 1)
            clrs.emplace_back(0.2, 0.2, 0.8);
          else
            clrs.emplace_back(0.8, 0.7, 0.2);
        }
      }

      m_points->setVertices(pts, clrs, true);
      m_points->m_size = 4.0f;
    }

    {  // set GL lines
      vector<Vector3> pts;
      vector<Color3> clrs;
      vector<UnsignedInt> ixs;
      int c = pts.size();

      if (Debug::get<bool>("drawaxis")) {
        float axis_length = 0.1;  // m
        // ADD COORD FRAME
        pts.emplace_back(0, 0, 0);
        pts.emplace_back(1 * axis_length, 0, 0);
        clrs.emplace_back(1, 0, 0);
        clrs.emplace_back(1, 0, 0);
        ixs.emplace_back(c + 0);
        ixs.emplace_back(c + 1);
        c += 2;
        pts.emplace_back(0, 0, 0);
        pts.emplace_back(0, 1 * axis_length, 0);
        clrs.emplace_back(0, 1, 0);
        clrs.emplace_back(0, 1, 0);
        ixs.emplace_back(c + 0);
        ixs.emplace_back(c + 1);
        c += 2;
        pts.emplace_back(0, 0, 0);
        pts.emplace_back(0, 0, 1 * axis_length);
        clrs.emplace_back(0, 0, 1);
        clrs.emplace_back(0, 0, 1);
        ixs.emplace_back(c + 0);
        ixs.emplace_back(c + 1);
        c += 2;
      }

      if (render_grid) {
        int n     = state.grid_n;
        int m     = state.grid_m;
        auto &phi = state.grid_phi;
        if (phi.rows() > 0 && n > 0 && m > 0)
          drawGrid(phi, n, m, pts, clrs, ixs);
      }

      m_lines->setVertices(pts, clrs, true);
      m_lines->setIndices(ixs);
      m_lines->m_size = 1.0f;
    }

    {  // set GL MESH
      vector<Vector3> pts;
      vector<Color3> clrs;
      vector<UnsignedInt> ixs;
      bool disconnected = Debug::get<bool>("render:disconnected");

      if (render_yarns)
        if (draw3Drods == 2) {
          for (auto &rds : state.rod_drawingstates) {
            drawRod3D(rds, pts, clrs, ixs, render_white ? cm_twist_white : cm_twist,
                      disconnected);
          }
        }

      if (!Debug::get<bool>("beautymode")) {
        m_triangles->setVertices(pts, clrs, true);
        m_triangles->setIndices(ixs);
        m_mesh->setIndices({});
      } else {
        m_mesh->setIndices(ixs);
        m_mesh->setVertices(pts, clrs, true);
        m_triangles->setIndices({});
      }
    }
  }

  void draw() {
    GL::Renderer::disable(GL::Renderer::Feature::FaceCulling);

    // set viewmatrix in the scene graph
    // TODO merge class, or just make wrapper around camera3d that can do
    // spherical movement
    sg_object_camera->setTransformation(
        (m_camera.getViewMatrix() * m_coordinateRotation).invertedRigid());
    sg_camera->setProjectionMatrix(m_camera.getProjectionMatrix(
        Vector2{GL::defaultFramebuffer.viewport().size()}));

    // common shader uniforms
    auto V = sg_object_camera->absoluteTransformationMatrix().invertedRigid();
    // auto L1 = sg_object_light_main->absoluteTransformationMatrix();
    // auto L2 = sg_object_light_rim->absoluteTransformationMatrix();

    m_shader.setLightDir1(V.transformVector({0.5, 0.5, -1}).normalized())
        .setLightDir2(V.transformVector({1, -1, 1}).normalized());

    // TODO NEW
    // m_shader.setLightPosition1((V * L1).transformPoint({0, 0, 0}))
    //     .setLightPosition2((V * L2).transformPoint({0, 0, 0}));
    // if (Debug::get<bool>("beautymode"))
    //   m_shader.setLightColor1(Color3{255 / 255.0f, 253 / 255.0f, 220 /
    //   255.0f})
    //       .setLightColor2(Color3{200 / 255.0f, 200 / 255.0f, 100 / 255.0f});
    // else
    //   m_shader.setLightColor1(Color3{255 / 255.0f, 253 / 255.0f, 220 /
    //   255.0f})
    //       .setLightColor2(Color3{66 / 255.0f, 134 / 255.0f, 244 / 255.0f});

    sg_camera->draw(sg_drawables);
  }

  SphericalCamera &camera() { return m_camera; }

  bool render_yarns = true;
  bool render_grid  = true;
  bool render_white = false;

 private:
  Scene3D sg_root;                   // add objects for transformation hierarchy
  Object3D *sg_visual_scale_object;  // parent of everthing drawn for scaling
  Object3D *sg_object_camera;
  Matrix4 m_coordinateRotation;
  Object3D *sg_object_light_main;
  Object3D *sg_object_light_rim;
  SceneGraph::DrawableGroup3D
      sg_drawables;  // add drawables for combined drawing call
  SceneGraph::Camera3D
      *sg_camera;  // draws all drawables in the group using camera perspective
  SphericalCamera m_camera;

  Phong2Material m_material;
  Phong2Lights m_lights;
  Phong2 m_shader;

  Shaders::VertexColor3D m_shader_vertexcolor;
  Magnum::SceneGraph::DrawablePointsVC3D *m_points;
  Magnum::SceneGraph::DrawableLinesVC3D *m_lines;
  Magnum::SceneGraph::DrawableTrianglesVC3D *m_triangles;
  Magnum::SceneGraph::DrawableMesh *m_mesh;

  ColorMap<Color3> cm_twist;
  ColorMap<Color3> cm_twist_white;
  ColorMap<Color3> cm_falsecolors;

};

#endif /* end of include guard: _SCENE_H_ */
