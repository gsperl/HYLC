#ifndef _SIMULATIONDRAWINGSTATE_H_
#define _SIMULATIONDRAWINGSTATE_H_

#include <Magnum/Magnum.h>
#include <Magnum/Math/Vector3.h>
#include <iostream>
#include "../../simulation/statics/Simulation.h"

#include <vector>

struct SimulationDrawingState {
  template <typename T>
  using vec = std::vector<T>;

  struct RodDrawingState {
    vec<bool> periodic;
    vec<Magnum::Vector3> vertices;
    vec<Magnum::Vector3> frame_n;
    vec<Magnum::Vector3> frame_bn;
  };

  vec<RodDrawingState> rod_drawingstates;
  int grid_n, grid_m;
  MatrixXXs grid_phi;

  // NOTE: CANNOT access GL stuff (e.g. setting vertex buffers), because
  // simulation thread has no GL
  // context! need to pass all info back to main thread and let it deal with GL
  void synchronize(Simulation &sim) {
    rod_drawingstates.clear();
    grid_phi.resize(0,3);
    grid_n = 0;
    grid_m = 0;
    if (!sim.isInitialized())
      return;

    const auto& pyp = *sim.getPYP();
    const auto& gyp = *sim.getGYP();
    const auto& lwcs = gyp.getLWCYarns();

    const auto& yl = gyp.getYL(); // yarn list


    rod_drawingstates.resize(yl.size());
    
    for (size_t i = 0; i < yl.size(); i++) {
      auto &rds  = rod_drawingstates[i];
      // vertices and periodic flag
      auto &yarn = yl[i];
      rds.vertices.reserve(yarn.size());
      rds.periodic.reserve(yarn.size());
      for (size_t j = 0; j < yarn.size(); j++) {
        auto vtx = gyp.getVertex(yarn[j]).head<3>();
        bool periodic = gyp.isPeriodic(yarn[j]);
        rds.vertices.emplace_back(vtx(0), vtx(1), vtx(2));
        rds.periodic.push_back(periodic);
      }
      // material frames
      const auto& lwcss = lwcs[i]->m_strandState;
      rds.frame_n.reserve(yarn.size());
      rds.frame_bn.reserve(yarn.size());
      auto &mf1 = lwcss->m_materialFrames1;
      auto &mf2 = lwcss->m_materialFrames2;
      // auto &mf1 = lwcss->m_referenceFrames1;
      // auto &mf2 = lwcss->m_referenceFrames2;
      for (size_t j = 0; j < yarn.size(); j++) {
        rds.frame_n[j] = convert_m2e(mf1[j] * pyp.r);
        rds.frame_bn[j] = convert_m2e(mf2[j] * pyp.r);
      }
    }
    // expansion grid
    {
      const auto &expansion = *sim.getExpansion().get();
      grid_n                = expansion.getN();
      grid_m                = expansion.getM();
      grid_phi              = expansion.getValues();
    }
  }

 private:
  Magnum::Vector3 convert_m2e(const Vector3s &e) {
    return Magnum::Vector3(e(0), e(1), e(2));
  }
};

#endif /* end of include guard: _SIMULATIONDRAWINGSTATE_H_ */
