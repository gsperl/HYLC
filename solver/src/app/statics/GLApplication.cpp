#include "GLApplication.h"
#include "../../simulation/debug/debug_includes.h"
#include <sstream>

using namespace Magnum;
using namespace Magnum::Math::Literals;

// #include <Eigen/Core>

GLApplication::GLApplication(const Arguments &arguments)
    : Base{arguments, Configuration{}
                          .setTitle("Micro Optimization")
                          .setWindowFlags(Configuration::WindowFlag::Resizable |
                                          Configuration::WindowFlag::Maximized),
           GLConfiguration{}.setVersion(GL::Version::GL330), false},
      m_simsettings{} {
  // Eigen::initParallel(); // this might not be necessary since c++11

  m_simsettings.pypfile = "../pyp/stockinette.pyp";

  resetSimulation();

  // add periodic task for update function
  m_simulationloop->addTaskPeriodic([this](Simulation &sim) {
    std::chrono::high_resolution_clock::time_point t1 =
        std::chrono::high_resolution_clock::now();

    sim.step();

    std::chrono::high_resolution_clock::time_point t2 =
        std::chrono::high_resolution_clock::now();
    gui_sim_ms =
        std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
  });
  m_simulationloop->addTaskPeriodic([this](Simulation &sim) {
    // maybe: lock_guard<mutex> guard(m_statemutex);
    sim.settings = m_simsettings;  // overwrite sim settings with gui settings
  });

  m_simulationloop->addTaskPeriodic([this](Simulation &sim) {
#ifdef NDEBUG
    if (sim.isInitialized()) {
      if (gui_Eplot.size() == 0) {
        gui_Eplot.resize(256);
        gui_Eplotix = 0;
      }
      // gui_Eplot[gui_Eplotix] = sim.getYarnSolver().getLastEnergy();
      last_E                 = sim.getTotalEnergy();
      last_A                 = sim.getPYP()->px * sim.getPYP()->py;
      gui_Eplot[gui_Eplotix] = last_E;
      gui_Eplotix            = (gui_Eplotix + 1) % 256;
    }
#endif
  });

  m_simulationloop->startThread();
}

void GLApplication::pauseSimulation() {
  m_simulationloop->m_paused = !m_simulationloop->m_paused;
  // m_sync = !m_simulationloop->m_paused;
  // m_sync should be always true, its synced only when dirty anyway
}

void GLApplication::singleStep() {
  if (m_simulationloop->m_paused) {
    // this task might not be necessary if we otherwise enforce sync to happen
    // after sim update in the loop
    m_simulationloop->addTaskOnce([this](Simulation &sim) {
      {
        lock_guard<mutex> guard(m_statemutex);
        m_drawingstate.synchronize(sim);
        m_drawingstate_dirty = true;
      }
    });
    m_simulationloop->m_single_step = true;
  }
}

void GLApplication::resetSimulation() {
  m_simulationloop->addTaskOnce([this](Simulation &sim) {
    sim.settings = m_simsettings;
    sim.reset();
  });
}

void GLApplication::serialize_simulation_state(const std::string &file) {
  // m_simulationloop->addTaskOnce([file](Simulation &sim) {
  //   // rescale area
  //   if (sim.settings.ek(1) != 0 || sim.settings.ek(3) != 0 ||
  //       sim.settings.ek(4) != 0 || sim.settings.ek(5) != 0)
  //     Debug::warning(
  //         "Serializing not just xy-stretched proto. p1 and p2 will "
  //         "be wrong!\n");

  //   // assume eps=(sx^2,0;0,sy^2), ie rescale by sqrt of entries
  //   // scalar sp1 = std::sqrt(sim.settings.ek(0));
  //   // scalar sp2 = std::sqrt(sim.settings.ek(2));
  //   scalar sp1 = sim.settings.ek(0); // sx
  //   scalar sp2 = sim.settings.ek(2); // sy

  //   sim.serialize(file, sp1, sp2);
  //   printf("Serialized current state\n");
  // });
}

void GLApplication::drawGUI_MenuBar() {
  if (ImGui::BeginMainMenuBar()) {
    // if (ImGui::BeginMenu("Import")) {
    //   ImGui::SameLine();
    //   ImGui::InputText("##m_simsettings.pypfile", &m_simsettings.pypfile);
    //   ImGui::SameLine();
    //   ImGui::FileButton(0, "Open", m_simsettings.pypfile);
    //   ImGui::EndMenu();
    // }

    // if (ImGui::BeginMenu("Export##menu")) {
    //   ImGui::InputText("##gui_output_file", &gui_output_file);
    //   ImGui::SameLine();
    //   ImGui::FileButton(0, "Open", gui_output_file);
    //   if (ImGui::MenuItem("Export##item", "O")) {
    //     // TODO DEBUG WINDOW WITH STRING EDIT
    //     this->serialize_simulation_state(gui_output_file);
    //   }
    //   if (ImGui::MenuItem("Export to Input", "Shift+O")) {
    //     this->serialize_simulation_state(m_simsettings.pypfile);
    //   }
    //   ImGui::EndMenu();
    // }

    if (ImGui::BeginMenu("Rendering")) {
      drawGUI_Rendering();
      ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Commands")) {
      if (ImGui::MenuItem("Reset Simulation", "R")) {
        this->resetSimulation();
      }
      if (ImGui::MenuItem("Reset Camera", "C")) {
        m_scene.resetCamera();
      }
      if (ImGui::MenuItem("Single Step", "S")) {
        this->singleStep();
      }
      if (ImGui::MenuItem("Synchronize Drawing State", "D")) {
        m_simulationloop->addTaskOnce([this](Simulation &sim) {
          {
            lock_guard<mutex> guard(m_statemutex);
            m_drawingstate.synchronize(sim);
            m_drawingstate_dirty = true;
          }
        });
        this->loadDrawingState();
      }
      ImGui::EndMenu();
    }
    ImGui::EndMainMenuBar();
  }
}

void GLApplication::drawGUI_Rendering() {
  ImGui::Checkbox("white", &m_scene.render_white);
  ImGui::Checkbox("yarns", &m_scene.render_yarns);
  ImGui::Checkbox("grid", &m_scene.render_grid);
  ImGui::Checkbox("axis", &Debug::get<bool>("drawaxis"));
  ImGui::Checkbox("disconnected", &Debug::get<bool>("render:disconnected"));
  ImGui::DragFloat("render:radius", &Debug::get<float>("render:radius", 1.0),
                   0.01f, 0.1f, 1.0f);
}

void GLApplication::drawGUI_Output() {
  ImGui::Text("GUI: %.1f FPS (%.3f ms)", ImGui::GetIO().Framerate,
              1000.0f / ImGui::GetIO().Framerate);
  if (gui_sim_ms > 0)
    ImGui::Text("SIM: %.1f FPS (%.3f ms)", 1000.0f / gui_sim_ms,
                (float)gui_sim_ms);
  else
    ImGui::Text("SIM: - FPS (0 ms)");
  // ignoring thread safety
  ImGui::Text("T: %d", m_simulationloop->getSimulation()->steps);

  ConstrainedNewtonSolver::StopFlag flag =
      m_simulationloop->getSimulation()->getStopFlag();
  std::string flagstr;
  if (flag == ConstrainedNewtonSolver::StopFlag::converged)
    flagstr = "converged";
  else if (flag == ConstrainedNewtonSolver::StopFlag::linearsolve)
    flagstr = "lin. solve failed";
  else if (flag == ConstrainedNewtonSolver::StopFlag::linesearch)
    flagstr = "linesearch failed";
  else
    flagstr = "running";
  ImGui::Text("Stop: %s", flagstr.c_str());
  ImGui::Text("E: %.8e", last_E);
  ImGui::Text("E/A: %.8e", last_E / last_A);

  static float pp = 1.0;
  ImGui::DragFloat("plotzoom", &pp, 0.01f, 0.0f, 1.0f);
  const auto &values       = gui_Eplot;
  const int &values_offset = gui_Eplotix;
  if (values.size() > 0) {
    // Debug::log(values_offset, values[0]);
    float range0, range1;
    {
      // range0 = values[0];
      // range1 = values[0];
      // for (int i = 1; i < (int)values.size(); ++i) {
      //   range0 = std::min(range0, values[i]);
      //   range1 = std::max(range1, values[i]);
      // }
      range0 = 1e20;
      range1 = -1e20;
      for (int i = (int)(values.size() * (1 - pp)); i < (int)(values.size());
           ++i) {
        int k  = (gui_Eplotix + i) % values.size();
        range0 = std::min(range0, values[k]);
        range1 = std::max(range1, values[k]);
      }
      float dr = (range1 - range0) * 0.1f;
      range0   = range0 - dr;
      range1   = range1 + dr;
    }
    std::ostringstream ostr;
    ostr << "E (range: ";
    ostr << std::scientific;
    ostr << range1-range0;
    ostr << ")";

    ImGui::PlotLines(
        "Lines", &values[0], (int)values.size(), values_offset,
        // str(boost::format("E (range: %.2e)") % (range1 - range0)).c_str(), // needs boost installed e.g. via vcpkg...
        ostr.str().c_str(),
        range0, range1, ImVec2(0, 80));
  }
}

void GLApplication::drawGUI() {
  drawGUI_MenuBar();

  // ImGuiIO &io = ImGui::GetIO();

  ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 4.0f);

  // if (ImGui::Begin("invisible_title", NULL, ImGuiWindowFlags_NoTitleBar)) {
  // if (ImGui::Begin(">>>>")) {
  // }
  // ImGui::End();

  if (ImGui::Begin("GUI")) {
    if (ImGui::CollapsingHeader("Debug Settings",
                                ImGuiTreeNodeFlags_DefaultOpen)) {
      ImGui::Indent();

      if (ImGui::TreeNode("debug:generic")) {
        ImGui::Checkbox("1", &Debug::get<bool>("DBG_FLAG1"));
        ImGui::Checkbox("2", &Debug::get<bool>("DBG_FLAG2"));
        ImGui::Checkbox("3", &Debug::get<bool>("DBG_FLAG3"));
        ImGui::Checkbox("4", &Debug::get<bool>("DBG_FLAG4"));
        ImGui::Checkbox("5", &Debug::get<bool>("DBG_FLAG5"));
        ImGui::Checkbox("6", &Debug::get<bool>("DBG_FLAG6"));
        ImGui::Checkbox("7", &Debug::get<bool>("DBG_FLAG7"));
        ImGui::TreePop();
      }

      ImGui::Checkbox("ignore translation constraint",
                      &Debug::get<bool>("no_translation_constr"));
      ImGui::Checkbox("ignore twist constraint",
                      &Debug::get<bool>("no_twist_constr"));
      // ImGui::Checkbox("ignore sliding constraint",
      //                 &Debug::get<bool>("no_slide_constr"));

      if (ImGui::HueButton(125, "Freeze Rest State")) {
        m_simulationloop->addTaskOnce(
            [this](Simulation &sim) { sim.freezeRestshapes(); });
      }

      {
        ImGui::ItemWidthGuard iwg(100.0f);
        ImGui::DragFloat("RL scale", &RLscale, 0.01f, 0.0f, 1.0f, "%.2f");
        ImGui::DragFloat("RK scale", &RKscale, 0.01f, 0.0f, 1.0f, "%.2f");
        ImGui::DragFloat("RT scale", &RTscale, 0.01f, 0.0f, 1.0f, "%.2f");
      }
      if (ImGui::HueButton(125, "Rescale Rest State")) {
        m_simulationloop->addTaskOnce([this](Simulation &sim) {
          sim.scaleRestshapes(RLscale, RKscale, RTscale);
        });
      }

      // serializePYP
      ImGui::InputText("Export dir", &gui_output_file);
      if (ImGui::HueButton(125, "Export current state as PYP")) {
        m_simulationloop->addTaskOnce([this](Simulation &sim) {
          bool override_period = true;
          sim.serializePYP(gui_output_file, override_period);
        });
      }

      // if (ImGui::TreeNodeEx("debug:reststate")) {
      //   ImGui::TreePop();
      // }

      ImGui::Unindent();
    }

    if (ImGui::CollapsingHeader("Simulation Settings",
                                ImGuiTreeNodeFlags_DefaultOpen)) {
      ImGui::Indent();
      drawSimulationSettings(m_simsettings);
      ImGui::Unindent();
    }
  }
  ImGui::End();

  if (ImGui::Begin("Output")) {
    drawGUI_Output();
  }
  ImGui::End();

  ImGui::PopStyleVar();
}

//---Input Events----------------------------------------

void GLApplication::keyPressEvent(KeyEvent &event) {
  if (m_imgui.handleKeyPressEvent(event))
    return;

  // temporary fix against text input events not being eaten by e.g. dragfloat
  if (ImGui::GetIO().WantTextInput /* || ImGui::GetIO().WantCaptureMouse*/)
    return;

  // static int ii = 0;

  switch (event.key()) {
    case KeyEvent::Key::Esc:
      this->exit();
      break;
    case KeyEvent::Key::Space:
      this->pauseSimulation();
      break;
    case KeyEvent::Key::R:
      this->resetSimulation();
      break;
    case KeyEvent::Key::O:
      if (event.modifiers() & KeyEvent::Modifier::Shift)
        this->serialize_simulation_state(m_simsettings.pypfile);
      else
        this->serialize_simulation_state(gui_output_file);
      break;
    // case KeyEvent::Key::G:
    //   m_simulationloop->addTaskOnce([this](Simulation &sim) {
    //     int ny = sim.getExpansion().getN();
    //     int nx = sim.getExpansion().getM();
    //     std::ostringstream stringStream;
    //     stringStream << "tmp/grid_" << ny << "_" << nx << ".txt";
    //     sim.serialize_grid(stringStream.str());
    //   });
    //   break;

    // case KeyEvent::Key::X:
    //   m_simulationloop->addTaskOnce([this](Simulation &sim) {
    //     int ny = sim.getExpansion().getN();
    //     int nx = sim.getExpansion().getM();
    //     std::ostringstream stringStream;
    //     stringStream << "tmp/grid" << ny << "_" << nx << "_0000.grid";
    //     sim.serialize_grid(stringStream.str());
    //   });
    //   if (event.modifiers() & KeyEvent::Modifier::Shift) {
    //     if (ii != 0)
    //       ii = 0;
    //     else {
    //       m_simulationloop->addTaskPeriodic([&](Simulation &sim) {
    //         char name[50];
    //         sprintf(name, "tmp/%04d.proto", ii);
    //         sim.serialize(name);
    //         ii++;
    //       });
    //     }
    //   }
    //   break;

    case KeyEvent::Key::F:
      m_simulationloop->addTaskOnce(
          [this](Simulation &sim) { sim.freezeRestshapes(); });
      break;
    case KeyEvent::Key::H:
      m_simulationloop->addTaskOnce([this](Simulation &sim) {
        sim.scaleRestshapes(RLscale, RKscale, RTscale);
      });
      break;

    case KeyEvent::Key::C:
      m_scene.resetCamera();
      break;
    case KeyEvent::Key::S:
      this->singleStep();
      break;
    case KeyEvent::Key::T:
    // m_simulationloop->addTaskOnce([this](Simulation &sim) {
    //   // h, A, M
    //   auto stats = sim.getYarnPatch().getTileStats();
    //   scalar h = std::get<0>(stats);
    //   scalar A = std::get<1>(stats);
    //   scalar M = std::get<2>(stats);
    //   Debug::logf(
    //       "Pattern Tiling Stats:\n  Height (currently): %.10e\n  Area: "
    //       "%.10e\n  Mass: %.10e\n  Area Density: %.10e\n",
    //       h, A, M, M / A);
    // });
    // break;
    // case KeyEvent::Key::B:
    //   m_simulationloop->addTaskOnce([this](Simulation &sim) {
    //     // h, A, M
    //     auto &q = sim.getYarnPatch().getX();
    //     for (int i = 0; i < q.rows() / 4; ++i) {
    //       q(4 * i + 3) += 0.1;
    //     }

    //     VectorXs y = sim.getYarnSolver().reproject(sim.getYarnPatch());
    //     sim.getYarnSolver().setState(sim.getYarnPatch(), y);

    //     scalar E = sim.getYarnSolver().getEnergy(sim.getYarnPatch());
    //     Debug::logf("ENERGY AFTER CONST TWIST %.15e\n", E);

    //     sim.getYarnSolver().resetMeritFunction();
    //   });
    //   break;
    // case KeyEvent::Key::Eight:
    //   m_simulationloop->addTaskOnce([this](Simulation &sim) {
    //     sim.debugSerializeU("tmp_uw.m");
    //   });
    //   break;
    // case KeyEvent::Key::Nine:
    //   m_simulationloop->addTaskOnce([this](Simulation &sim) {
    //     sim.debugDeserializeU("tmp_uw24.m");
    //   });
    //   break;
    // case KeyEvent::Key::Zero:
    //   m_simulationloop->addTaskOnce([this](Simulation &sim) {
    //     sim.debugDeserializeU("tmp_uw25.m");
    //   });
    //   break;
    // case KeyEvent::Key::J:
    //   m_simulationloop->addTaskOnce([this](Simulation &sim) {
    //     double E = std::get<0>(sim.getResults());
    //     std::cout << "Energy: " << E << "\n";
    //     // Debug::log("Energy:",E);
    //   });
    default:
      break;
  }
}

void GLApplication::keyReleaseEvent(KeyEvent &event) {
  if (m_imgui.handleKeyReleaseEvent(event))
    return;
}

void GLApplication::mousePressEvent(MouseEvent &event) {
  if (m_imgui.handleMousePressEvent(event))
    return;

  if (event.button() == MouseEvent::Button::Right) {
    m_previousMousePosition = event.position();
  }

  event.setAccepted();
}

void GLApplication::mouseReleaseEvent(MouseEvent &event) {
  if (m_imgui.handleMouseReleaseEvent(event))
    return;

  event.setAccepted();
}

void GLApplication::mouseMoveEvent(MouseMoveEvent &event) {
  if (m_imgui.handleMouseMoveEvent(event))
    return;

  auto &camera        = m_scene.camera();
  const Vector2 delta = 1.0f *
                        Vector2{event.position() - m_previousMousePosition} /
                        Vector2{GL::defaultFramebuffer.viewport().size()};

  if ((event.buttons() & MouseMoveEvent::Button::Right)) {
    // non-wheel zoom with ctrl and right-drag updown
    if (event.modifiers() & MouseMoveEvent::Modifier::Ctrl)
      camera.zoom(100 * 0.1 * delta[1]);
    // 2D unlocked y rotation: shift + right-drag
    else if (event.modifiers() & MouseMoveEvent::Modifier::Shift)
      camera.orbit(-delta[0], delta[1]);
  } else if ((event.buttons() & MouseMoveEvent::Button::Left)) {
    // if (event.modifiers() & MouseMoveEvent::Modifier::Alt)
    camera.translate({-delta[0], delta[1]});
  }
  m_previousMousePosition = event.position();
  event.setAccepted();
}

void GLApplication::mouseScrollEvent(MouseScrollEvent &event) {
  if (m_imgui.handleMouseScrollEvent(event))
    return;

  auto &camera = m_scene.camera();
  camera.zoom(-1 * 0.1 * event.offset()[1]);
  event.setAccepted();
}

// creates main function
MAGNUM_APPLICATION_MAIN(GLApplication)
