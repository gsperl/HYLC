#include "camera.h"

#define PI 3.14159265358979

SphericalCamera::SphericalCamera()
    : SphericalCamera(1.0, 0, 0, Magnum::Vector3(0, 0, 0)) {}

SphericalCamera::SphericalCamera(double r0, double phi0, double theta0,
                                 Magnum::Vector3 target, double speed,
                                 bool perspective)
    : m_near(0.01),
      m_far(100.0),
      m_r(r0),
      m_phi(phi0),
      m_theta(theta0),
      m_target(target),
      m_speed(speed),
      m_perspective(perspective) {}

void SphericalCamera::orbit(double dphi, double dtheta) {
  m_phi += m_speed * dphi;
  m_theta += m_speed * dtheta;
  if (m_theta > PI * 0.5 * 0.99)
    m_theta = PI * 0.5 * 0.99;
  if (m_theta < -PI * 0.5 * 0.99)
    m_theta = -PI * 0.5 * 0.99;
}

void SphericalCamera::zoom(double dr) {
  m_r += m_speed * dr;
  m_r = std::max(m_r, 0.01);
}

Magnum::Vector3 SphericalCamera::screenToWorld(Magnum::Vector2 screenpoint,
                                               Magnum::Vector2 viewport) {
  // UNTESTED AND PROBABLY WRONG BC Z COORDINATE

  // unproject two rays from pixelcoords, then move along the rays

  // TODO USE THE distance to target as z
  Magnum::Vector3 x_viewport = {screenpoint[0] / viewport[0],
                                -screenpoint[1] / viewport[1],
                                0.99};  // middle of frustum

  auto x_view =
      this->getProjectionMatrix(viewport).inverted().transformPoint(x_viewport);
  auto x_world = this->getViewMatrix().invertedRigid().transformPoint(x_view);
  return x_world;
}

void SphericalCamera::translate(Magnum::Vector3 dx) {
  m_target += m_speed * dx;
}

void SphericalCamera::translate(Magnum::Vector2 dx) {
  Magnum::Vector3 c_right(1, 0, 0);
  Magnum::Vector3 c_up(0, 1, 0);
  auto vinv    = getViewMatrix().invertedRigid();
  auto w_right = vinv.transformVector(c_right);
  auto w_up    = vinv.transformVector(c_up);

  m_target += m_speed * m_r / 5 * (dx[0] * w_right + dx[1] * w_up);
}

Magnum::Matrix4 SphericalCamera::getViewMatrix() {
  double r = m_r;

  if (!m_perspective) {
    // orthographic: have 0 at half frustum depth
    // zooming is done by recasting m_r as the orthosize
    r = (m_near + m_far) * 0.5;
  }

  // if (!m_perspective)
  //   r *= 35.0; // fake-orthographic needs more distance

  Magnum::Vector3 up(0, 1, 0);
  Magnum::Vector3 eye(
      r * cos(m_phi) * cos(m_theta), r * sin(m_theta),
      -r * sin(m_phi) *
          cos(m_theta));  // position of the camera relative to target

  return Magnum::Matrix4::lookAt(eye + m_target, m_target, up).invertedRigid();
}

Magnum::Matrix4 SphericalCamera::getProjectionMatrix(Magnum::Vector2 viewport) {
  // perspective

  float orthosize_x = m_r;  // 8.0f;
  if (m_perspective)
    return Magnum::Matrix4::perspectiveProjection(m_fov, viewport.aspectRatio(),
                                                  m_near, m_far);
  else {
    // return Magnum::Matrix4::perspectiveProjection(
    //     1.0_degf, viewport.aspectRatio(), m_near,
    //     std::numeric_limits<float>::infinity());
    return Magnum::Matrix4(
        {2 / orthosize_x, 0, 0, 0},
        {0, 2 / (orthosize_x / viewport.aspectRatio()), 0, 0},
        {0, 0, 2 / (float)(m_near - m_far), 0},
        {0, 0, 2 * (float)(m_near / (m_near - m_far)) - 1, 1});
    return Magnum::Matrix4::orthographicProjection(
        {orthosize_x, orthosize_x / viewport.aspectRatio()}, m_near, m_far);
  }
}
