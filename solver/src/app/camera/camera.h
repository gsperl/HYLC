#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <Magnum/Magnum.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Math/Vector3.h>
#include <algorithm> // max
#include <limits>    // orthographic infinite far
#include <math.h>    // sin, cos

using namespace Magnum::Math::Literals;
class SphericalCamera {

public:
  SphericalCamera();
  SphericalCamera(double r0, double phi0, double theta0, Magnum::Vector3 target,
                  double speed = 1.0, bool perspective = true);
  // TODO construcotr using camera position to get radius phi theta

  void orbit(double dphi, double dtheta);
  void zoom(double dr);
  Magnum::Vector3 screenToWorld(Magnum::Vector2 screenpoint,
                                Magnum::Vector2 viewport);
  void translate(Magnum::Vector3 dx);
  void translate(Magnum::Vector2 dx);
  bool isPerspective() { return m_perspective; }
  void setPerspective(bool val) { m_perspective = val; }

  Magnum::Matrix4 getViewMatrix();
  Magnum::Matrix4 getProjectionMatrix(Magnum::Vector2 viewport);

  double m_near, m_far;
  Magnum::Deg m_fov = 35.0_degf;

private:
  double m_r, m_phi, m_theta;
  Magnum::Vector3 m_target;

  double m_speed;
  bool m_perspective = true;
};

#endif /* end of include guard: _CAMERA_H_ */
