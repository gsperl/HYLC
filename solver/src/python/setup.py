import os
import re
import sys
import platform
import subprocess

from setuptools import setup, Extension
from setuptools.command.build_ext import build_ext
from distutils.version import LooseVersion

assert(platform.python_version().startswith("3")) # to avoid compiling with wrong pip on cluster

class CMakeExtension(Extension):
    def __init__(self, name, sourcedir=''):
        Extension.__init__(self, name, sources=[])
        self.sourcedir = os.path.abspath(sourcedir)


class CMakeBuild(build_ext):
    def run(self):
        try:
            out = subprocess.check_output(['cmake', '--version'])
        except OSError:
            raise RuntimeError("CMake must be installed to build the following extensions: " +
                               ", ".join(e.name for e in self.extensions))

        if platform.system() == "Windows":
            cmake_version = LooseVersion(
                re.search(r'version\s*([\d.]+)', out.decode()).group(1))
            if cmake_version < '3.1.0':
                raise RuntimeError("CMake >= 3.1.0 is required on Windows")

        for ext in self.extensions:
            self.build_extension(ext)

    def build_extension(self, ext):
        extdir = os.path.abspath(os.path.dirname(
            self.get_ext_fullpath(ext.name)))
        # hard coded vcpkg toolchain, but vcpkg is checked out with repo as submodule
        vcpkg_toolchain = "../../../vcpkg/scripts/buildsystems/vcpkg.cmake"
        vcpkg_toolchain = os.path.join(os.getcwd(),vcpkg_toolchain)
        cmake_args = ['-DCMAKE_TOOLCHAIN_FILE=' + vcpkg_toolchain,
                      '-DCMAKE_LIBRARY_OUTPUT_DIRECTORY=' + extdir,
                      '-DPYTHON_EXECUTABLE=' + sys.executable]

        cfg = 'Debug' if self.debug else 'Release'
        build_args = ['--config', cfg]

        if platform.system() == "Windows":
            cmake_args += [
                '-DCMAKE_LIBRARY_OUTPUT_DIRECTORY_{}={}'.format(cfg.upper(), extdir)]
            if sys.maxsize > 2**32:
                cmake_args += ['-A', 'x64']
            build_args += ['--', '/m']
        else:
            cmake_args += ['-DCMAKE_BUILD_TYPE=' + cfg]
            build_args += ['--', '-j']

        env = os.environ.copy()
        env['CXXFLAGS'] = '{} -DVERSION_INFO=\\"{}\\"'.format(env.get('CXXFLAGS', ''),
                                                              self.distribution.get_version())
        
        # override for aesthetics, putting python temp build into common build
        self.build_temp = os.path.join(os.getcwd(),"..","..","build","build-python")

        if not os.path.exists(self.build_temp):
            os.makedirs(self.build_temp)
        subprocess.check_call(['cmake', ext.sourcedir] +
                              cmake_args, cwd=self.build_temp, env=env)
        subprocess.check_call(['cmake', '--build', '.'] +
                              build_args, cwd=self.build_temp)

# NOTE: annoyingly I couldn't figure out how to make setup redirect cmake compilation output to the console. currently, it prints output only after cmake fails. 

setup(
    name='HYLC',
    version='1.0.0',
    author='Georg Sperl',
    author_email='gsperl@ist.ac.at',
    description='Python interface to periodic yarn simulation',
    long_description='',
    ext_modules=[ # extension modules
        CMakeExtension('HYLC.PatternSolver')],
    cmdclass=dict(build_ext=CMakeBuild),
    zip_safe=False,
    packages=[],  # python packages to be found e.g. HYLC.*** for HYLC/***/__init__.py
)
