"""Root module of HYLC package"""

 # to avoid compiling with wrong pip or running with wrong python on machines with python/pip != python3/pip3
import platform
assert(platform.python_version().startswith("3"))