import os
import numpy as np

def combine_files(input_dir, output_merged, fmt='%.18e', header=None):
    data = []
    for f in sorted(os.listdir(input_dir)):
        if os.path.isfile(os.path.join(input_dir,f)):
            data.append(np.loadtxt(os.path.join(input_dir,f)))
    data = np.stack(data)
    np.savetxt(output_merged, data,fmt=fmt,header=header)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dir')
    parser.add_argument('output_merged')

    args = parser.parse_args()
    # input_dir = "test_data/out"
    # output_merged = "test_data/out.txt"
    combine_files (args.input_dir, args.output_merged)
