def set_material_settings(settings, name):
    """override the material parameters within settings by looking up values as submitted in [Sperl et al. 2020] given a name"""
    if "basket" in name:
        settings.material.yE      = 1e5
        settings.material.G       = 4e4
        settings.material.gamma   = 0.1
        settings.material.kc      = 1.2e1
        settings.material.density = 1.2e2
    elif "honey" in name:
        settings.material.yE      = 5e5
        settings.material.G       = 2e5
        settings.material.gamma   = 0.1
        settings.material.kc      = 6e1
        settings.material.density = 1.2e2
    elif "rib" in name:
        settings.material.yE      = 5e5
        settings.material.G       = 2e5
        settings.material.gamma   = 0.001
        settings.material.kc      = 6e1
        settings.material.density = 6e1
    elif "satin" in name:
        if "small" in name:
            settings.material.yE      = 1e6
            settings.material.G       = 4e5
            settings.material.gamma   = 1
            settings.material.kc      = 1e2
            settings.material.density = 1.2e3
        else:
            settings.material.yE      = 1e5
            settings.material.G       = 4e4
            settings.material.gamma   = 0.1
            settings.material.kc      = 1.2e1
            settings.material.density = 1.2e2
    elif "stock" in name:
        if "small" in name:
            settings.material.yE      = 1e6
            settings.material.G       = 4e5
            settings.material.gamma   = 1
            settings.material.kc      = 1e2
            settings.material.density = 1.2e3
        else:
            settings.material.yE      = 5e5
            settings.material.G       = 2e5
            settings.material.gamma   = 0.001
            settings.material.kc      = 6e1
            settings.material.density = 1.2e2

def debug_collision_logless(settings, name):
    """override the material parameters within settings by looking up values as submitted in [Sperl et al. 2020] given a name"""
    if "basket" in name:
        settings.material.kc      = 1.4e3
    elif "honey" in name:
        settings.material.kc      = 1.1e4
    elif "rib" in name:
        settings.material.kc      = 3e4
    elif "satin" in name:
        if "small" in name:
            settings.material.kc      = 5e4
        else:
            settings.material.kc      = 1.4e3
    elif "stock" in name:
        if "small" in name:
            settings.material.kc      = 5e4
        else:
            settings.material.kc      = 5e3

def set_simulation_settings(settings, name):
    """override other settings such as regularization and stopping condition thresholds"""
    settings.extpx = 0.8 #0.6
    settings.extpy = 0.8 #0.6
    settings.gridResolution = 0.25 # 0.75
    settings.newtonSettings.linear_reg_start = 2
    settings.newtonSettings.linear_reg_stop = 1e-10
    settings.newtonSettings.linear_reg_steps = 600
    settings.newtonSettings.linesearch_mult = 0.5
    settings.newtonSettings.linesearch_steps = 13#10
    settings.newtonSettings.lagrange_epsilon = 1e-04
    settings.newtonSettings.projgrad_epsilon = 1e-06 # 5e-05
    settings.solvableSettings.constraint_scale = 1.0
    settings.solvableSettings.step_factor = 0.3
    # settings.solvableSettings.unit_Nm = 1e-05
    # settings.solvableSettings.unit_pos = 1.0
    # settings.solvableSettings.unit_rad = 1.0 #0.01