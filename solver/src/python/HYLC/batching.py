import HYLC.PatternSolver as ps
from HYLC.parallel import run_parallel
import numpy as np
import os
import time

# guard against accidental use of python2.x on cluster
import platform
assert(platform.python_version().startswith("3"))

# define additional stopping conditions
StopFlag_Steps = 4
StopFlag_Time = 5


# RUN.PY
#  def STRAINGENERATOR: yield strain
#  def SETTINGSFUNC(strain): makebasesettings, settings.stepfactor = stepfactorfunc(), settings.strain=strain
# use batch()
# use email() or maybe exec some py script wrapped in an email logger?
#  eg. python batchingscript.py > 2&>1 | tee log.txt; python email.py log.txt

def _simulation_args(strain_generator, settings_func, max_steps=int(1e6), max_time=1e10):
    for strain in strain_generator:
        settings = settings_func(strain)
        yield settings_func, strain, max_steps, max_time


def _simulation_task(args):
    settings_func, strain, max_steps, max_time = args
    T0 = time.time()
    settings = settings_func(strain)
    sim = ps.Simulation(settings)
    steps = 0
    while 1:
        if sim.isFinished():
            stop = int(sim.getStopFlag())  # 0 ... 3
            break
        if (time.time() - T0 >= max_time):
            stop = StopFlag_Steps
            break
        if steps >= max_steps:
            stop = StopFlag_Time
            break
        sim.step()
        steps += 1
    E = sim.getAverageEnergy()

    return settings.strains, E, stop, time.time() - T0


def np2str(arr, fmt="%.20g"):
    return " ".join([fmt % val for val in np.array(arr).reshape(-1)])

def batch(outputfile, strain_generator, settings_func, max_steps=int(1e6), max_time=1e10, n_cpus=-1, chunksize=1, output_every=10, ffmt="%.10g", ifmt="%d"):
    res = run_parallel(_simulation_task,
                       _simulation_args(strain_generator,
                                        settings_func, max_steps, max_time),
                       order=True, n_cpus=n_cpus, chunksize=chunksize, output_every=output_every)

    outdir = os.path.dirname(outputfile)
    if outdir:
        os.makedirs(outdir, exist_ok=True)
    with open(outputfile, 'w') as rfile:
        for strain, E, stop, T in res:
            line = np2str(strain, fmt=ffmt)
            line += " ".join(["", ffmt, ifmt, ffmt]) % (E, stop, T)
            line += "\n"
            rfile.write(line)
