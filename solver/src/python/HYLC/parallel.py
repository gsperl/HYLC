import multiprocessing
from multiprocessing import Pool
import platform
import os
import numpy as np


def _wrapped_task(_args):
    """wrap task with task number"""
    i, task, a = _args
    return i, task(a)


def _wrapped_arg_gen(arg_gen, task):
    """wrap task and arg with task number"""
    for i, a in enumerate(arg_gen):
        yield i, task, a


def _order(results):
    """order wrapped results using task number"""
    res_ordered = [None]*len(results)
    for res in results:
        res_ordered[res[0]] = res[1]
    return res_ordered


def run_parallel(task_fun, arg_gen, order=True, n_cpus=-1, chunksize=1, output_every=1):
    """run tasks in parallel, and optionally order the result by arg order before returning. Usage e.g.:

    def task(args):
        return args[0]**2+args[1]

    def arg_gen():
        for i in range(20):
            yield [(i+1),0]

    res = run_parallel(task,arg_gen(),order=True,output_every=-1)
    """
    if order:
        _task = _wrapped_task
        _arg_gen = _wrapped_arg_gen(arg_gen, task_fun)
    else:
        _task = task_fun
        _arg_gen = arg_gen

    if n_cpus is None or n_cpus <= 0:
        n_cpus = multiprocessing.cpu_count()
    pool = Pool(n_cpus)
    pool_it = pool.imap_unordered(_task, _arg_gen, chunksize=chunksize)

    results = []
    try:
        n_finished = 0
        for res in pool_it:
            results.append(res)
            n_finished += 1
            if (n_finished % output_every == 0) and output_every >= 0:
                print("Finished %d tasks." % n_finished)
    except KeyboardInterrupt:
        print("KeyboardInterrupt, terminating workers")
        pool.terminate()
    else:
        pool.close()

    if order:
        results = _order(results)
    return results


def bind_cpus_linux(n_bind=-1, p_bind=-1):
    """bind the first n_bind cores or p_bind% cores on linux platforms and return the number of bound cores"""
    if n_bind < 0 and p_bind < 0:
        print("Skipping cpu binding.")
        return 0

    if not 'linux' in platform.system().lower():
        print("Skipping cpu binding (not supported on platform %s)." %
              platform.system())
        return 0
    
    if n_bind > 0 and p_bind > 0:
        print("Supplied n_bind and p_bind, defaulting to n_bind")
        p_bind = 0

    # find number of available cpus
    n_avail = multiprocessing.cpu_count()

    # convert percentage input
    if p_bind > 0:
        n_bind = int(p_bind * n_avail)

    # clamp bind
    n_bind = min(max(n_bind, 1), n_avail)

    print("Binding %d/%d cpus" % (n_bind, n_avail))

    # create the cpu affinity mask
    mask = np.array([0] * n_avail)
    mask[:n_bind] = 1
    mask = str(hex(int("".join(map(str, mask)), 2)))

    # set cpu affinity
    print("taskset -p %s %d" % (mask, os.getpid()))
    os.system("taskset -p %s %d" % (mask, os.getpid()))
    return n_bind
