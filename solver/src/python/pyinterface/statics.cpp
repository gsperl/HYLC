#include "../../simulation/statics/Simulation.h"

#include <pybind11/eigen.h>
#include <pybind11/iostream.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;
using namespace pybind11::literals;  // for shorter kwargs

#ifndef VERSION_INFO  // should get defined in setup.py
#define VERSION_INFO "dev"
#endif

#include <iostream>
#include <memory>
#include <tuple>

// NOTE .def(name,func,redirect()) to every method that should redirect
// stdout/stderr, e.g. for python notebooks
typedef typename py::call_guard<py::scoped_ostream_redirect,
                                py::scoped_estream_redirect>
    redirect;

typedef Eigen::Matrix<double, 11, 1> Vec11;
typedef Eigen::Matrix<double, 6, 1> Vec6;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VecX;

template <typename T>
void setDebug(const std::string& str, T value) {
  Debug::get<T>(str) = value;
}

PYBIND11_MODULE(PatternSolver, m) {
  // general module metadata
  m.doc() = "homogenized yarn level cloth - statics solver module";
  m.attr("__version__") = VERSION_INFO;

  // m.def("pyfun", &cppfun, "docstring",
  //       "kwvarname"_a, "kwvarname"_a = default_value);

  m.def("setDebug", setDebug<bool>)
      .def("setDebug", setDebug<double>)
      .def("setDebug", setDebug<int>);

  // EXPOSE SETTINGS

  py::class_<Material>(m, "Material")
      .def(py::init<>(), redirect())
      .def_readwrite("yE", &Material::yE)
      .def_readwrite("G", &Material::G)
      .def_readwrite("gamma", &Material::gamma)
      .def_readwrite("kc", &Material::kc)
      .def_readwrite("density", &Material::density);

  py::class_<SolvableSettings>(m, "SolvableSettings")
      .def(py::init<>(), redirect())
      .def_readwrite("step_factor", &SolvableSettings::step_factor)
      .def_readwrite("unit_Nm", &SolvableSettings::unit_Nm)
      .def_readwrite("unit_pos", &SolvableSettings::unit_pos)
      .def_readwrite("unit_rad", &SolvableSettings::unit_rad)
      .def_readwrite("constraint_scale", &SolvableSettings::constraint_scale)
      .def_readwrite("disable_slide_constraint", &SolvableSettings::disable_slide_constraint);

  py::class_<NewtonSolverSettings>(m, "NewtonSolverSettings")
      .def(py::init<>(), redirect())
      .def_readwrite("linear_reg_start",
                     &NewtonSolverSettings::linear_reg_start)
      .def_readwrite("linear_reg_stop", &NewtonSolverSettings::linear_reg_stop)
      .def_readwrite("linear_reg_steps",
                     &NewtonSolverSettings::linear_reg_steps)
      .def_readwrite("projgrad_epsilon",
                     &NewtonSolverSettings::projgrad_epsilon)
      .def_readwrite("lagrange_epsilon",
                     &NewtonSolverSettings::lagrange_epsilon)
      .def_readwrite("linesearch_steps",
                     &NewtonSolverSettings::linesearch_steps)
      .def_readwrite("linesearch_mult", &NewtonSolverSettings::linesearch_mult);

  // EXPOSE SIMULATION CLASS

  py::class_<SimulationSettings>(m, "SimulationSettings")
      .def(py::init<>(), redirect())
      .def_readwrite("pypfile", &SimulationSettings::pypfile)
      .def_readwrite("strains", &SimulationSettings::strains)
      .def_readwrite("extpx", &SimulationSettings::extpx)
      .def_readwrite("extpy", &SimulationSettings::extpy)
      .def_readwrite("gridResolution", &SimulationSettings::gridResolution)
      .def_readwrite("material", &SimulationSettings::material)
      .def_readwrite("newtonSettings", &SimulationSettings::newtonSettings)
      .def_readwrite("solvableSettings", &SimulationSettings::solvableSettings);

  py::class_<Simulation> sim_class(m, "Simulation");
  sim_class
      .def(py::init([](const SimulationSettings& settings) {
             Simulation* sim = new Simulation();
             sim->settings   = settings;
             sim->reset();
             return sim;
           }),
           redirect())
      .def("step", &Simulation::step, redirect())
      .def("getTotalEnergy", &Simulation::getTotalEnergy, redirect())
      .def("getAverageEnergy", &Simulation::getAverageEnergy, redirect())
      .def("isFinished", &Simulation::isFinished, redirect())
      .def("getStopFlag", &Simulation::getStopFlag, redirect())
      .def("getArea", &Simulation::getArea, redirect())
      .def("getAreaDensity", &Simulation::getAreaDensity, redirect())
      // [...]
      .def("getSurfaceS",
           [](Simulation& sim) { return sim.getExpansion()->S(); }, // OLD delete
           redirect())
      .def("xbar", // X --> phi + h n
           [](Simulation& sim, const Vector3s& X) { return sim.getExpansion()->xbar(X); },
           redirect())
      .def("Jbar", // jac of X --> phi + h n
           [](Simulation& sim, const Vector3s& X) { return sim.getExpansion()->Jbar(X); },
           redirect())
      .def("getGT",  // [[gix giy giz thetai]]
           [](Simulation& sim) {
             VectorXs y = sim.getNewton()->getY();
             VectorXs g = sim.getSolvable()->gFromY(y);
             MatrixXXs GT(g.rows() / 4, 4);
             for (int i = 0; i < GT.rows(); i++) {
               GT.row(i) = g.segment<4>(4 * i);
             }
             return GT;
           },
           redirect())
      .def("getQ",  // [[x y z t]] as stored in pyp, undeformed
           [](Simulation& sim) {
              return sim.getPYP()->Q;
           },
           redirect())
      .def("getQd",  // [[x y z t]] as stored in pyp, deformed
           [](Simulation& sim) {
              return sim.getGYP()->getQd();
           },
           redirect())
      .def("computeQrd",  // [[x y z t]] shaped as pyp.Q but current deformed state mapped back into reference space
           [](Simulation& sim, scalar eps_r, int max_steps) {
              return sim.getGYP()->computeQrd(eps_r, max_steps);
           }, "eps_r"_a=0.1, // default target error 0.1 radius
            "max_steps"_a=10, // num newton steps
           redirect())
      .def("getRL",
           [](Simulation& sim) {
             return sim.getPYP()->getRestLengths();
           },
           redirect())
      .def("getYarnLists",
           [](Simulation& sim) {
             return sim.getPYP()->computePeriodicallyConnectedYarns();
           },
           redirect())
      // debugging: gradient tests, etc.
      .def("_setX",
           [](Simulation& sim, const VectorXs& g) {
             sim.getPFM()->setState(g, VectorXs::Zero(g.rows()), 0.0);
           },
           redirect())
      .def("_F", [](Simulation& sim) { return sim.getPFM()->E(); }, redirect())
      .def("_G", [](Simulation& sim) { return sim.getPFM()->gradE(); },
           redirect())
      .def("_H", [](Simulation& sim) { return sim.getPFM()->hessE(); },
           redirect())
      .def("_N", [](Simulation& sim) { return sim.getPFM()->getNumDof(); },
           redirect());

  // bind stopflag within simulation scope, i.e. Simulation.StopFlag
  py::enum_<ConstrainedNewtonSolver::StopFlag>(sim_class, "StopFlag")
      .value("running", ConstrainedNewtonSolver::StopFlag::running)
      .value("converged", ConstrainedNewtonSolver::StopFlag::converged)
      .value("linesearch", ConstrainedNewtonSolver::StopFlag::linesearch)
      .value("linearsolve", ConstrainedNewtonSolver::StopFlag::linearsolve);
}
