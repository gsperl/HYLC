#ifndef _MATHEMATICADEFINITIONS_H_
#define _MATHEMATICADEFINITIONS_H_

#include "EigenDefinitions.h"
#include <math.h>

namespace Mathematica {

typedef scalar Real;
inline Real Power(Real A, Real B) { return std::pow(A, B); }
// inline Real Power(const Real &A, const int &B) { return std::pow(A, B); }
inline Real Power(Real A, int B) {
  assert(abs(B) < 8 && "require higher pow, implement using std.");
  Real out = 1.0;
  if (B >= 0) {
    for (int i = 0; i < B; ++i)
      out *= A;
  } else {
    Real invA = 1.0 / A;
    for (int i = 0; i < -B; ++i)
      out *= invA;
  }
  return out;
}

inline Real Sqrt(Real A) { return std::sqrt(A); }
inline Real Exp(Real A) { return std::exp(A); }
inline Real Sin(Real A) { return std::sin(A); }
inline Real Cos(Real A) { return std::cos(A); }
inline Real Log(Real A) { return std::log(A); }

} // namespace Mathematica

#endif /* end of include guard: _MATHEMATICADEFINITIONS_H_ */
