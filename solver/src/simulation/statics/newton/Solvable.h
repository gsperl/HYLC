#ifndef __SOLVABLE__H__
#define __SOLVABLE__H__

#include "../../pyp/PeriodicForceModel.h"
#include "../constraints/LagrangeConstraint.h"

// TODO to CPP
#include "../../debug/debug_includes.h"
#include "../constraints/SlidingConstraint.h"
#include "../constraints/TranslationConstraint.h"
#include "../constraints/TwistNullspaceConstraint.h"

struct SolvableSettings {
  scalar step_factor            = 0.3;  // max step factor of radius
  scalar constraint_scale       = 1.0;  // scaling Cy=d to b Cy = b d
  bool disable_slide_constraint = false;
  // DEPRECATED STUFF (NOTE: currently code instead does some diagonal
  // unit-scaling estimate every frame)
  scalar unit_Nm  = 1e-5;  // reference energy unit
  scalar unit_pos = 1.0;   // define ref. pos. unit as multiple of radius
  scalar unit_rad = 1.0;   // 0.01;  // define ref. twist unit in radians
};

// DEPRECATED NOTE about unit scaling:
//   interface to newton solver with scaled units
//   yhat = 1/a y ---> minimizing f(yhat)
//   -> grad_hat = dfdy a
//   -> hess_hat = hess a^2
//   -> constraint residual r_hat = 1/a_cols r
//       (from C(y+dy) = d -> C dy = -(Cy-d) = -r
//             C a (yhat+dyhat) = d -> Ca dyhat = - (C a yhat - d) = -r
//                                     a_cols C dyhat = - a_cols r
//                                     C dyhat = -r_hat
//   <- y = a yhat, dy = a dyhat (from setstate and limitstep)
//   additionally internally scale constraints b C y = b d
//   then we will have a scaled newton system:
//   | a^2 H  b C | .  |  dyhat | = _ |  a     grad_hat |
//   |  b C    0  |    | lambda |     | 1/a b (C y - d) |
//   which gives us the control to scale the system for better conditioning
//   where we try to set a to get a^2 H to have units of 1, same as C
//   and we can scale C relative to H with b

class Solvable {
 public:
  Solvable(SolvableSettings settings, std::shared_ptr<PFM> pfm)
      : m_settings(settings), s_pfm(pfm) {
    int nfree = s_pfm->getNumDof();
    m_LC      = std::make_shared<LagrangeConstraint>(nfree);

    // add translation constraint: \int_Omega g dOmega = 0
    if (!Debug::get<bool>("no_translation_constr"))
      m_LC->addConstraint(std::static_pointer_cast<Constraint>(
          std::make_shared<TranslationConstraint>(s_pfm->getGYP()->getPYP())));

    // add twist nullspace constraint per periodic yarn
    // \int_Omega theta dOmega = 0
    if (!Debug::get<bool>("no_twist_constr"))
      m_LC->addConstraint(std::static_pointer_cast<Constraint>(
          std::make_shared<TwistNullspaceConstraint>(
              s_pfm->getGYP()->getPYP())));

    // add sliding constraint: g.a_i = 0 on the boundary
    // NOTE: new constraint not in hylc2020, removes parametric translation
    // nullspace per periodic yarn
    if (!m_settings.disable_slide_constraint)
      m_LC->addConstraint(std::static_pointer_cast<Constraint>(
          std::make_shared<SlidingConstraint>(
              s_pfm->getGYP()->getPYP(), s_pfm->getGYP()->getExpansion())));

    // prd_edges = s_pfm->getGYP()->getPYP()->getBoundaryEdges(); // DEBUG
    // sliding

    m_LC->rebuild(m_settings.constraint_scale);

    computeUnitScales();
  }

  void limitStep(VectorXs& dyhat) {
    // VectorXs dg   = m_S * dyhat;
    VectorXs dg   = dyhat;
    auto gyp      = s_pfm->getGYP();
    scalar max_dg = 0;
    int nverts    = gyp->getNumVerticesPeriodic();
    for (int i = 0; i < nverts; i++) {
      scalar d = dg.segment<3>(i * 4).squaredNorm();
      if (d > max_dg)
        max_dg = d;
    }
    max_dg = sqrt(max_dg);

    // limit quasistatic movement according to radius
    scalar max_displacement = gyp->getPYP()->r * m_settings.step_factor;

    if (max_dg > max_displacement) {
      scalar factor = max_displacement / max_dg;
      // rescale
      dyhat *= factor;
    }
  }

  VectorXs gFromY(const VectorXs& yhat) const {
    // NOTE: g := [R^T utilde, theta]
    VectorXs g = yhat;  // m_S * yhat;
    return g;
  }

  void setState(const VectorXs& yhat) {
    VectorXs g = gFromY(yhat);
    VectorXs dgdt;  // empty velocity vector
    scalar dt = 0;
    s_pfm->setState(g, dgdt, dt);
  }

  scalar val() {
    scalar E = s_pfm->E();
    return E;
  }
  VectorXs grad() {
    // VectorXs gradE = m_S * s_pfm->gradE();
    VectorXs gradE = s_pfm->gradE();
    return gradE;
  }
  SparseXXs hess() { return s_pfm->hessE(); }
  // SparseXXs hess() { return m_S * s_pfm->hessE() * m_S; }

  int getNumDof() const { return s_pfm->getNumDof(); }
  int getNumConstraintRows() const { return m_LC->numRows(); }

  VectorXs computeConstraintResidual(const VectorXs& yhat) const {
    // VectorXs g = m_S * yhat;
    // return m_SCrowsInv * m_LC->computeResidual(g);
    VectorXs g = yhat;
    return m_LC->computeResidual(g);
  }

  void computeUnitScales() {
    // NOTE if using any ctilde then check which output rows are generated from
    // which input dofs and use the respective scale
    scalar r = s_pfm->getGYP()->getPYP()->r;

    scalar sqrtNm = std::sqrt(m_settings.unit_Nm);
    scalar a_x = r * m_settings.unit_pos / sqrtNm;  // factor for positional dof
    scalar a_t = m_settings.unit_rad / sqrtNm;      // factor for twist dof

    Debug::logf("UNIT SCALE: x %.2e, t %.2e\n", a_x, a_t);

    // construct unit scaling matrix for dofs
    VectorXs units(s_pfm->getNumDof());
    for (int i = 0; i < units.rows(); i++) {
      units(i) = (i % 4 != 3) ? a_x : a_t;
    }
    m_S = units.asDiagonal();

    // construct inverse scale of constraint row units for residual
    const auto& lcunits = m_LC->getUnits();
    units.resize(lcunits.size());
    for (size_t i = 0; i < lcunits.size(); i++) {
      scalar a;
      if (lcunits[i] == Constraint::Unit::Position)
        a = a_x;
      else if (lcunits[i] == Constraint::Unit::Twist)
        a = a_t;
      else {
        a = 1.0;
        assert(false && "constraint with unspecified row units");
      }
      units[i] = 1.0 / a;
    }
    m_SCrowsInv = units.asDiagonal();
  }

  // return dof-type for dofs and for rows in constraints, and number of sets
  std::tuple<std::vector<int>, std::vector<int>, int> getDofSets() const {
    // 0 for positional, 1 for twist
    int ndofs = s_pfm->getNumDof();
    std::vector<int> dofs;
    dofs.reserve(ndofs);
    for (int i = 0; i < ndofs; i++) {
      dofs.push_back((i % 4 == 3) ? 1 : 0);
    }
    std::vector<int> constrdofs;
    const auto& lcunits = m_LC->getUnits();
    constrdofs.reserve(lcunits.size());
    for (size_t i = 0; i < lcunits.size(); i++) {
      if (lcunits[i] == Constraint::Unit::Position)
        constrdofs.push_back(0);
      else if (lcunits[i] == Constraint::Unit::Twist)
        constrdofs.push_back(1);
      else {
        constrdofs.push_back(0);
        assert(false && "constraint with unspecified row units");
      }
    }

    return std::make_tuple(dofs, constrdofs, 2);
  }

  // compute and return Clt = CL Ct, dlt = dL - CL dt
  // NOTE for now Ct=I dt=0
  const SparseXXs& getCL() const { return m_LC->getC(); }

  void saveState() { s_pfm->getGYP()->saveLWCState(); }
  void restoreState() { s_pfm->getGYP()->restoreLWCState(); }

 private:
  SolvableSettings m_settings;
  std::shared_ptr<PFM> s_pfm;
  std::shared_ptr<LagrangeConstraint> m_LC;

  Eigen::DiagonalMatrix<scalar, Eigen::Dynamic, Eigen::Dynamic>
      m_S;  // unit scaling: y = m_S * yhat
  Eigen::DiagonalMatrix<scalar, Eigen::Dynamic, Eigen::Dynamic>
      m_SCrowsInv;  // unit scaling: rhat = m_SCrowsInv * r
};

#endif  // __SOLVABLE__H__