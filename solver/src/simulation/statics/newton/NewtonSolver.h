#ifndef __NEWTONSOLVER__H__
#define __NEWTONSOLVER__H__

#include "Solvable.h"

struct NewtonSolverSettings {
  // linear solver regularization
  scalar linear_reg_start = 2;
  scalar linear_reg_stop  = 1e-10;
  int linear_reg_steps    = 1000;
  int gd_steps = 100;  // allowed gradient steps when newton system fails
  // stopping criterion
  scalar projgrad_epsilon = 1e-7;  // |z| < eps * |z0|
  scalar lagrange_epsilon = 1e-4;  // |Cq-d|/neqs < eps
  // linesearch
  int linesearch_steps   = 13;
  scalar linesearch_mult = 0.5;
};

class ConstrainedNewtonSolver {
 public:
  enum class StopFlag {
    running     = 0,
    converged   = 1,
    linesearch  = 2,
    linearsolve = 3,
  } m_stopped;

  ConstrainedNewtonSolver(NewtonSolverSettings settings,
                          std::shared_ptr<Solvable> solvable);

  void resetMeritFunction();
  void step();
  const VectorXs& getY() const { return m_y; }

 private:
  NewtonSolverSettings m_settings;
  std::shared_ptr<Solvable> s_solvable;
  int m_steps;
  int m_num_failed_newton;
  VectorXs m_y;
  VectorXi m_nnzCLrows;

  // stopping criterion
  Eigen::SparseQR<SparseXXs, Eigen::COLAMDOrdering<int>> m_CCsolver;
  scalar m_z0;     // initial norm of proj grad
  scalar m_fprev;  // previous function value

  scalar computeProjectedGradientNorm(const VectorXs& dfdy);
  bool backtrackingLinesearch(const VectorXs& dy);
};

#endif  // __NEWTONSOLVER__H__