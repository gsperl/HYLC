#ifndef _LINEARSOLVER_H_
#define _LINEARSOLVER_H_

#include "../../EigenDefinitions.h"

// solve A + I_free * alpha * |b|_inf x = b
bool solve_constrained_regularized(SparseXXs &A, const VectorXs &b, VectorXs &x,
                                   int nfree = -1, scalar reg = 0);


// lower bound of the condition number using Gershgorin discs
scalar gershgorin_bound_condition_number(const SparseXXs &A);
// signed lower bound of eigenvalues
scalar gershgorin_bound_most_neg_eigval(const SparseXXs &A);

#endif /* end of include guard: _LINEARSOLVER_H_ */
