#include "NewtonSolver.h"
#include "../../ThreadUtils.h"
#include "LinearSolver.h"

// compute NNZ of the inner dimension (row for col-major)
VectorXi computeMinorNNZs(const SparseXXs& A) {
  VectorXi NNZ = VectorXi::Zero(A.innerSize());
  for (int k = 0; k < A.outerSize(); ++k)
    for (SparseXXs::InnerIterator it(A, k); it; ++it) ++NNZ(it.index());
  return NNZ;
}

ConstrainedNewtonSolver::ConstrainedNewtonSolver(
    NewtonSolverSettings settings, std::shared_ptr<Solvable> solvable)
    : m_settings(settings), s_solvable(solvable) {
  int ndof = s_solvable->getNumDof();
  m_y      = VectorXs::Zero(ndof);
  s_solvable->setState(m_y);

  // assuming CL is const for sim: cache nnz per row
  const auto& CL = s_solvable->getCL();
  m_nnzCLrows    = computeMinorNNZs(CL);  // num of nnzs per row
  assert(m_nnzCLrows.rows() == CL.rows());

  // temp hijack to reveal rank of constraint matrix
  m_CCsolver.compute(CL);
  Debug::logf("Constraint: %d/%d (rank/rows)\n", m_CCsolver.rank(), CL.rows());

  m_CCsolver.compute(CL * CL.transpose());
  if (m_CCsolver.info() != Eigen::Success) {
    Debug::error("Could not factorize lagrange constraint projection solver");
  }
  resetMeritFunction();
}

void ConstrainedNewtonSolver::resetMeritFunction() {
  m_fprev             = s_solvable->val();
  m_z0                = computeProjectedGradientNorm(s_solvable->grad());
  m_stopped           = StopFlag::running;
  m_steps             = 0;
  m_num_failed_newton = 0;
}

// add resize H and add CL^T to top right, CL to bottom left
void addConstraintParts(SparseXXs& H, const SparseXXs& CL,
                        const VectorXi& CL_nnzrows) {
  // | H  CL^T |
  // | CL  0   |
  // allocate nonzeros
  assert(H.cols() == CL.cols());
  VectorXi cols_nnz(H.cols() + CL.rows());  // #nonzeros per col
  for (int i = 0; i < H.cols(); i++) {
    cols_nnz(i) = H.innerVector(i).nonZeros() + CL.innerVector(i).nonZeros();
  }
  for (int i = 0; i < CL.rows(); i++) {  // for the CL^T part
    // need to know nonzeros along rows of CL (which we cached in m_nnzCLrows)
    cols_nnz(H.cols() + i) = CL_nnzrows(i);
  }
  int Hrows = H.rows();
  int Hcols = H.cols();
  H.conservativeResize(H.rows() + CL.rows(), H.cols() + CL.rows());

  // most importantly, allocate the computed nnz per col
  H.reserve(cols_nnz);

  // now sorted insertion (i.e. for increasing row ixs)
  // iteration should automatically be in increasing insertion order
  for (int k = 0; k < CL.outerSize(); ++k)
    for (SparseXXs::InnerIterator it(CL, k); it; ++it) {
      // add bottom left CL
      H.insert(Hrows + it.row(), it.col()) = it.value();
      // add top right CL^T
      H.insert(it.col(), Hcols + it.row()) = it.value();
    }
}

scalar ConstrainedNewtonSolver::computeProjectedGradientNorm(
    const VectorXs& dfdy) {
  // gradient norm of free variables y in tangent space of lagrange constraint
  // CL q = dL:
  // A A.T lam = A z
  // |P(z)| = sqrt(z.T z - lam.T A z)
  // where A = CL Ctilde, z = grad_y f

  const auto& z = dfdy;
  const auto& A = s_solvable->getCL();
  VectorXs Az   = A * z;
  VectorXs lam  = m_CCsolver.solve(Az);
  if (m_CCsolver.info() != Eigen::Success) {
    Debug::error("Could not solve lagrange projection!");
    assert(false);
    return std::numeric_limits<scalar>::max();
  }
  scalar znorm = std::sqrt(z.squaredNorm() - lam.dot(Az));
  return znorm;
}

void ConstrainedNewtonSolver::step() {
  if (m_stopped != StopFlag::running)
    return;

  Debug::Timer clk;

  // compute forces
  VectorXs grad = s_solvable->grad();

  int nfree = grad.rows();
  int neqs  = s_solvable->getNumConstraintRows();

  // assemble rhs: -[grad_y f(y); CL y - dL]
  VectorXs rhs(nfree + neqs);
  rhs.head(nfree) = -grad;
  rhs.tail(neqs)  = -s_solvable->computeConstraintResidual(m_y);

  Debug::log("CLK: rhs", clk.tock<std::chrono::milliseconds>());

  // stopping criterion
  scalar znorm       = computeProjectedGradientNorm(rhs.head(nfree));
  bool crit_projgrad = znorm < m_z0 * m_settings.projgrad_epsilon;
  bool crit_lagrange =
      rhs.tail(neqs).norm() / neqs < m_settings.lagrange_epsilon;
  assert(
      crit_lagrange);  // given initial projection and consistent Newton updates
  if (crit_projgrad && (crit_lagrange || neqs == 0)) {
    m_stopped = StopFlag::converged;
    Debug::log("Stopping criterion fulfilled.");
    return;
  }

  // compute Hessian
  SparseXXs H = s_solvable->hess();
  Debug::log("CLK: hess", clk.tock<std::chrono::milliseconds>());
  // assemble LHS: [H, CL^T; CL, 0]
  addConstraintParts(H, s_solvable->getCL(), m_nnzCLrows);
  Debug::log("CLK: LHS <- CL", clk.tock<std::chrono::milliseconds>());

  assert(H.cols() == H.rows());
  assert(H.cols() == rhs.rows());

  Eigen::DiagonalMatrix<scalar, Eigen::Dynamic, Eigen::Dynamic> S;
  {
    std::vector<int> dofs;
    std::vector<int> constrdofs;
    int nsets;
    std::tie(dofs, constrdofs, nsets) = s_solvable->getDofSets();
    assert(int(dofs.size()) == nfree && int(constrdofs.size()) == neqs);
    std::vector<scalar> avgs(nsets, 0.0);
    std::vector<scalar> invavgs(nsets, 0.0);
    std::vector<int> counts(nsets, 0);

    for (int i = 0; i < nfree; i++) {
      int k = dofs[i];
      avgs[k] += std::abs(H.coeff(i, i));
      ++counts[k];
    }

    for (int k = 0; k < nsets; k++) {
      if (counts[k] > 0) {
        scalar sqrta = std::sqrt(avgs[k] / counts[k]);
        avgs[k]      = sqrta;
        invavgs[k]   = 1.0 / sqrta;
      } else {
        avgs[k]    = 1.0;
        invavgs[k] = 1.0;
      }
    }

    // construct scaling matrix S = [1/A, A] where A are dof-set average sqrts
    VectorXs Sdiag(nfree + neqs);
    for (int i = 0; i < nfree; i++) {
      int k    = dofs[i];
      Sdiag(i) = invavgs[k];
    }
    for (int i = 0; i < neqs; i++) {
      int k            = constrdofs[i];
      Sdiag(nfree + i) = avgs[k];
    }
    S = Sdiag.asDiagonal();
  }

  // scalar Hxmin = 1e14, Hxmax = -1e14, Hxmaxabs = -1e14, Hxavgabs = 0;
  // scalar Htmin = 1e14, Htmax = -1e14, Htmaxabs = -1e14, Htavgabs = 0;
  // for (int i = 0; i < nfree; i++) {
  //   scalar h = H.coeff(i, i);
  //   if (i % 4 != 3) {
  //     Hxmin    = std::min(Hxmin, h);
  //     Hxmax    = std::max(Hxmax, h);
  //     Hxmaxabs = std::max(Hxmaxabs, std::abs(h));
  //     Hxavgabs += std::abs(h);
  //   } else {
  //     Htmin    = std::min(Htmin, h);
  //     Htmax    = std::max(Htmax, h);
  //     Htmaxabs = std::max(Htmaxabs, std::abs(h));
  //     Htavgabs += std::abs(h);
  //   }
  // }
  // Hxavgabs /= nfree * 3.0 / 4.0;
  // Htavgabs /= nfree * 1.0 / 4.0;
  // Debug::logf("Hxdiag:: %.2e / %.2e  || %.2e / %.2e\n", Hxmin, Hxmax,
  // Hxavgabs,
  //             Hxmaxabs);
  // Debug::logf("Htdiag:: %.2e / %.2e  || %.2e / %.2e\n", Htmin, Htmax,
  // Htavgabs,
  //             Htmaxabs);

  // solve for newton increment
  VectorXs dy;
  // NOTE about regularization:
  // using lower bound of most negative eigenvalue to project H(topleft) to SPD
  // but bound is overly conservative and for final good convergence need
  // less-regularized H
  // also near an optimum H is assumed to become naturally SPD
  // therefore starting with some strong regularization such that bound of
  // eigenvalues is >0
  // and decaying over time while H improves
  scalar reg =
      m_settings.linear_reg_start *
      std::pow(m_settings.linear_reg_stop / m_settings.linear_reg_start,
               std::min(m_steps, m_settings.linear_reg_steps) * 1.0 /
                   m_settings.linear_reg_steps);
  Debug::logf("USING REG %.2e\n", reg);
  // bool solved = solve_constrained_regularized(H, rhs, dy, nfree, reg);
  H           = S * H * S;
  bool solved = solve_constrained_regularized(H, S * rhs, dy, nfree, reg);
  if (solved) {
    dy = (S * dy)
             .head(nfree)
             .eval();  // NOTE: this eval() is apparently crucial in
                       // self assigning vector parts
  } else {
    // NOTE this includes the case where the solver returns a direction dy
    // with dy.(-grad) < 0, i.e. not a descent direction
    // defaulting to a projected gradient descent direction instead
    ++m_num_failed_newton;
    if (m_num_failed_newton < m_settings.gd_steps) {
      const auto& CL = s_solvable->getCL();
      VectorXs lam   = m_CCsolver.solve(CL * grad);
      assert(m_CCsolver.info() == Eigen::Success);
      dy = -grad + CL.transpose() * lam;
    } else {
      Debug::warning("Aborting Newton due to linear solve failing, and max number of gradient steps reached.");
      m_stopped = StopFlag::linearsolve;
      return;
    }
  }
  Debug::log("CLK: solve", clk.tock<std::chrono::milliseconds>());

  scalar cos = dy.dot(rhs.head(nfree));
  Debug::logf("Descent CosAngle: %.2e\n", cos);
  if (cos < 0)
    Debug::warning("Newton produced ascent, using gradient direction instead.");
  assert(cos >= 0);

  // limit maximum displacement
  s_solvable->limitStep(dy);
  Debug::log("CLK: limit", clk.tock<std::chrono::milliseconds>());

  // simple backtracking linesearch
  bool lssuccess = backtrackingLinesearch(dy);
  if (!lssuccess) {
    Debug::log("Linesearch could not improve function");
    m_stopped = StopFlag::linesearch;
  }
  Debug::log("CLK: LS", clk.tock<std::chrono::milliseconds>());

  m_steps++;
}

bool ConstrainedNewtonSolver::backtrackingLinesearch(const VectorXs& dy) {
  // store parallel transport quantities
  s_solvable->saveState();

  VectorXs y0 = m_y;
  for (int i = 0; i < m_settings.linesearch_steps; i++) {
    m_y = y0 + std::pow(m_settings.linesearch_mult, i) * dy;  // step

    if (i > 0)
      s_solvable->restoreState();

    // guard against Eigen solver crashing with values like 1e-150
    for (int i = 0; i < m_y.rows(); ++i) {
      if (std::abs(m_y(i)) < 1e-20) {
        m_y(i) = 0;
      }
    }

    s_solvable->setState(m_y);
    scalar f = s_solvable->val();
    if (f < m_fprev) {
      m_fprev = f;
      Debug::log("LS iter:", i);
      return true;  // success
    }
  }

  // E(y + a dy) !< E(y)
  // either because dy was not a decrease direction
  // or because E(y) is so close to a local minimum along dy
  // that even the smallest attempted step size a increases the energy

  m_y = y0;
  s_solvable->restoreState();
  s_solvable->setState(m_y);
  return false;
}