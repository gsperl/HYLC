#include "LinearSolver.h"

#include "../../debug/debug_includes.h"

typedef Eigen::SimplicialLDLT<SparseXXs> DirectSolverType;

scalar gershgorin_bound_condition_number(const SparseXXs &A) {
  assert(A.cols() == A.rows());
  auto absA =  A.cwiseAbs();
  VectorXs rabs = absA * VectorXs::Ones(A.cols());
  VectorXs cabs = VectorXs::Ones(A.rows()).transpose() * absA;

  scalar lmax = 0;
  scalar lmin = std::numeric_limits<scalar>::max();
  for (int i = 0; i < A.rows(); i++) {
    // disc for i: D(C=Aii, r=sum_j!=i |Aij|)
    scalar C = A.coeff(i, i);
    scalar r = std::min(rabs(i) - std::abs(C), cabs(i) - std::abs(C));
    scalar low = C - r;
    scalar high = C + r;

    // if any disc contains 0, then 0 is a possible lowest eigenvalue
    // and thus the worst-case condition number is infinity
    if (low <= 0 && high >= 0) {
      return std::numeric_limits<scalar>::max();
    }

    // otherwise find the lowest and highest by abs value potential eigenvalues
    scalar vmax = std::abs(high);
    scalar vmin = std::abs(low);
    lmax = std::max(lmax, vmax);
    lmin = std::min(lmin, vmin);
  }
  return lmax/lmin;
}

scalar gershgorin_bound_most_neg_eigval(const SparseXXs &A) {
  assert(A.cols() == A.rows());
  auto absA =  A.cwiseAbs();
  VectorXs rabs = absA * VectorXs::Ones(A.cols());
  VectorXs cabs = VectorXs::Ones(A.rows()).transpose() * absA;

  scalar lmin = std::numeric_limits<scalar>::max();
  for (int i = 0; i < A.rows(); i++) {
    // disc for i: D(C=Aii, r=sum_j!=i |Aij|)
    scalar C = A.coeff(i, i);
    scalar r = std::min(rabs(i) - std::abs(C), cabs(i) - std::abs(C));
    lmin = std::min(lmin, C - r);
  }
  return lmin;
}

bool solve_constrained_regularized(SparseXXs &A, const VectorXs &b, VectorXs &x,
                                   int nfree, scalar alpha) {
  if (nfree < 0)
    nfree = A.rows();

  // bound on most negative eigenvalue
  scalar eig_bound = gershgorin_bound_most_neg_eigval(A.topLeftCorner(nfree,nfree));
  Debug::logf("Most neg. eig(H):    %.2e\n", eig_bound);
  
  // want lowest eigenvalue to be at least:
  scalar min_eig=1e-10; // TODO magic (param via nwt settings?)

  scalar base_reg;
  if (eig_bound < min_eig) 
    base_reg = min_eig - eig_bound; // adding whats missing
  else {
    base_reg = 0;
  }

  // compute regularization
  // scalar reg = alpha * b.cwiseAbs().maxCoeff(); // old
  scalar reg = alpha * base_reg;
  for (int i = 0; i < nfree; ++i) A.coeffRef(i, i) += reg;
  Debug::logf("Reg:  %.4e (%.4e * %.4e)\n", reg, alpha, base_reg);
  Debug::logf("Most neg. eig(H+rI): %.2e\n", gershgorin_bound_most_neg_eigval(A.topLeftCorner(nfree,nfree)));

  // NOTE: LDLT solver works for KKT systems [[H, C.T; C, 0]]
  // if H is SPD and C has rank==rows
  // https://forum.kde.org/viewtopic.php?f=74&t=106962#p337135
  DirectSolverType solver;

  // factorize
  solver.compute(A);

  if (solver.info() != Eigen::Success) {
    Debug::logf("Linear solver failed to factorize!\n");
    return false;
  }
  // solve (A x = b) for x
  x = solver.solve(b);

  if (solver.info() != Eigen::Success) {
    Debug::logf("Linear solver failed to solve!\n");
    return false;
  }

  // check for non-positive-definitness of solution
  // since for quasistatics b is gradient descent direction
  // the solution should be aligned similarly
  // also x.b = x.A.x should be > 0
  scalar dot = x.dot(b);
  if (dot <= 0.0) {
    if (std::abs(dot) < 1e-15 ) {
      Debug::warningf("System was semidefinite!\n");
    } else {
      Debug::warningf("System was indefinite!\n");
      return false;
    }
  }

  // success
  return true;
}
