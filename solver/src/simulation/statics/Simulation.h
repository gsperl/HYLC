
#ifndef NDEBUG
#define EIGEN_INITIALIZE_MATRICES_BY_NAN
#endif

#ifndef _SIMULATION_H_
#define _SIMULATION_H_
#include "../EigenDefinitions.h"
#include "../debug/debug_includes.h"
#include "../pyp/expansion/GridExpansion.h"
#include "../serialization/serialization.h"
// #include "serialization/serialization.h"

#include <math.h>
#include <unistd.h>
#include <memory>
#include <random>
#include <thread>
#include <tuple>
#include <vector>

#include "newton/NewtonSolver.h"

// note currently some settings can be modified during simulation
// others only at the start (bc only used in initialization..)
struct SimulationSettings {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;  // storing fixed size Eigen matrix
  std::string pypfile;
  // YarnSolverSettings solver;
  Vector6s strains;  // strains (sx,sa,sy,II00,II01,II11)
  scalar extpx          = 0.6;
  scalar extpy          = 0.6;
  scalar gridResolution = 1.0;  // width as multiple of pattern radius
  Material material;
  NewtonSolverSettings newtonSettings;
  SolvableSettings solvableSettings;

  SimulationSettings() { strains << 0, 0, 0, 0, 0, 0; }
};

class Simulation {
 public:
  Simulation() { initialized = false; }

  ~Simulation() {}

  void step() {
    if (!initialized)
      return;

    m_newton->step();

    if (Debug::get<bool>("DBG_FLAG3"))
      m_gyp->projectPeriodicFrames();

    steps++;
  }

  void reset() {
    initialized = false;
    steps       = 0;

    m_pyp                = std::make_shared<PYP>();
    std::string filename = settings.pypfile;
    bool success         = deserialize_pyp(filename, *m_pyp.get());
    if (!success) {
      Debug::errorf("Couldn't read pyp file: '%s'\n", filename.c_str());
      m_expansion.reset();
      m_gyp.reset();
      m_pfm.reset();
      m_solvable.reset();
      m_newton.reset();
      return;
    }

    const auto& mat = settings.material;

    m_gyp = std::make_shared<GYP>();
    // set ref config form pyp extended with ghosts
    m_gyp->initialize(m_pyp, mat, settings.extpx, settings.extpy);

    // solve expansion surface grid
    Vector3s extents = m_gyp->computeExtents();
    scalar gridsx    = extents(0) * 1.05;
    scalar gridsy    = extents(1) * 1.05;
    m_expansion      = std::make_shared<GridExpansion>(
        settings.strains, 0, m_pyp->r * settings.gridResolution, gridsx,
        gridsy);

    // precompute R, phi + hn
    m_gyp->precomputeExpansionData(m_expansion);

    m_pfm      = std::make_shared<PFM>(m_gyp, mat);
    m_solvable = std::make_shared<Solvable>(settings.solvableSettings, m_pfm);
    m_newton   = std::make_shared<ConstrainedNewtonSolver>(
        settings.newtonSettings, m_solvable);

    initialized = true;
  }

  // void serialize_grid(const std::string &file) {
  //   // int ny = m_expansion->getN();
  //   // int nx = m_expansion->getM();
  //   const MatrixXXs &phi = m_expansion->getValues();
  //   serialize_matrix(file, phi);
  // }

  std::shared_ptr<PYP> getPYP() { return m_pyp; }
  std::shared_ptr<const PYP> getPYP() const { return m_pyp; }
  std::shared_ptr<const GYP> getGYP() const { return m_gyp; }
  std::shared_ptr<const ConstrainedNewtonSolver> getNewton() const {
    return m_newton;
  }
  std::shared_ptr<const Solvable> getSolvable() const {
    return m_solvable;
  }
  std::shared_ptr<const GridExpansion> getExpansion() const {
    return m_expansion;
  }
  std::shared_ptr<PFM> getPFM() { return m_pfm; }  // for debugging

  scalar getTotalEnergy() const { return m_pfm->E(); }
  scalar getAverageEnergy() const {
    return m_pfm->E() / (m_pyp->px * m_pyp->py);
  }
  ConstrainedNewtonSolver::StopFlag getStopFlag() const {
    if (m_newton)
      return m_newton->m_stopped;
    return ConstrainedNewtonSolver::StopFlag::running;
  }
  void freezeRestshapes() {
    m_gyp->freezeRestshapes();
    m_newton->resetMeritFunction();
  }
  void scaleRestshapes(scalar rl = 1.0, scalar rk = 1.0, scalar rt = 1.0) {
    m_gyp->scaleRestshapes(rl, rk, rt);
    m_newton->resetMeritFunction();
  }
  void serializePYP(const std::string& filename, bool override_period = false) {
    // NOTE: this deprecates the pyp for the running simulation
    // so this should only be called after finishing. otherwise need to
    // instead write the information into a new pyp object (TODO)
    m_gyp->setPYPFromLWC();  // override pyp Q/rest/ref data from lwc
    scalar sx = settings.strains(0) + 1;
    scalar sy = settings.strains(2) + 1;
    if (override_period) {
      if (!(settings.strains(1) == 0 && settings.strains(3) == 0 &&
            settings.strains(4) == 0 && settings.strains(5) == 0)) {
        Debug::error(
            "Serializing pyp with period override under non-supported "
            "deformation!");
        assert(false);  // only serialize for sx sy deformation!
      }
      m_pyp->px *= sx;
      m_pyp->py *= sy;
    }

    bool success = serialize_pyp(filename, *m_pyp.get());
    if (!success) {
      Debug::errorf("Couldn't write to file '%s'\n", filename.c_str());
    }
  }

  bool isInitialized() const { return initialized; }
  bool isFinished() {
    if (m_newton)
      return m_newton->m_stopped != ConstrainedNewtonSolver::StopFlag::running;
    return false;
  }

  double getArea() const { return m_pyp->px * m_pyp->py; }
  double getAreaDensity() const {
    return settings.material.density * m_pyp->computeYarnVolume() / getArea();
  }

  int steps;
  SimulationSettings settings;

 private:
  std::shared_ptr<ConstrainedNewtonSolver> m_newton;
  std::shared_ptr<Solvable> m_solvable;
  std::shared_ptr<PYP> m_pyp;
  std::shared_ptr<GYP> m_gyp;
  std::shared_ptr<PFM> m_pfm;
  std::shared_ptr<GridExpansion> m_expansion;
  bool initialized = false;
};

#endif /* end of include guard: _SIMULATION_H_ */
