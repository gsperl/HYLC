#ifndef _CONSTRAINT_H_
#define _CONSTRAINT_H_

#include <utility>
#include <vector>
#include "../../EigenDefinitions.h"

// Base class for constraints C q = d
class Constraint {
 public:
  Constraint() {}
  virtual ~Constraint() {}

  enum class Unit { Default, Position, Twist };

  // "builds" C and d for C q = d, C is stored as triplets
  // such that it can be added to global constraints
  virtual std::tuple<std::vector<Triplet>, VectorXs, std::vector<Unit>>
  build(scalar scale = 1.0) = 0;
};

#endif /* end of include guard: _CONSTRAINT_H_ */
