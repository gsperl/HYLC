#ifndef _SLIDINGCONSTRAINT_H_
#define _SLIDINGCONSTRAINT_H_

#include <memory>
#include "../../pyp/PeriodicYarnPattern.h"
#include "Constraint.h"

// Constrain g . a_i = 0 on boundary i with outward surface tangent a_i
// i.e. utilde . R . S . e_i = 0, utilde not allowed to distance itself from
//   the boundary, it can only slide on the boundary
class SlidingConstraint : public Constraint {
 public:
  SlidingConstraint(std::shared_ptr<PYP> pyp, std::shared_ptr<const Expansion> exp) : s_pyp(pyp), s_exp(exp) {}
  ~SlidingConstraint() {}

  std::tuple<std::vector<Triplet>, VectorXs, std::vector<Unit>> build(
      scalar scale) {
    // find periodic boundary crossing edges
    // select vertex on the boundary and constrain its movement

    // list of edge ixs crossing the boundary with dx,dy != 0,0
    std::vector<int> prd_edges = s_pyp->getBoundaryEdges();
    // s_pyp->computePeriodicallyConnectedYarns // TODO maybe constrain only first crossing per yarn!

    std::vector<Triplet> triplets;
    triplets.reserve(3 * (int)prd_edges.size());

    assert(s_exp);
    Matrix3s S = s_exp->S();

    std::vector<std::vector<int>> pyl =
        s_pyp->computePeriodicallyConnectedYarns();
    const auto& vet = s_pyp->getVertexEdgeTable();
    for (int i = 0; i < (int)pyl.size(); i++) {  // constraint per p. yarn
      int vix_ = pyl[i].front();
      int eix_prev = vet(vix_,0); // assumed periodic edge at start of yarn
      const auto edge = s_pyp->E.row(eix_prev); // edge across boundary
      assert(abs(edge(2))+abs(edge(3)) == 1 &&
             "period across multiple boundaries or periodic edge with dx=dy=0");
      assert(edge(2) != 0 || edge(3) != 0);
      int vix = edge(1); // ix of vertex at start of boundary
      assert(vix_ == vix); // DEBUG once choosing pyl instead of all prdedges, simplify

      Vector3s a; // tangent vector orthogonal to crossing boundary
      if (edge(2) == 0)
        a = S.col(0);
      else
        a = S.col(1);
      scalar tmp = a(0);
      a(0) = a(1);
      a(1) = -tmp;

      // Debug::log("Constr for dir",edge(2)==0?1:0,"a=",a);

      for (int j = 0; j < 3; j++)
        triplets.emplace_back(i, 4 * vix + j, a(j) * scale);
    }

    VectorXs d = VectorXs::Zero(pyl.size());  // * scale
    std::vector<Unit> units(d.rows(), Unit::Position);
    return std::make_tuple(triplets, d, units);
  }

 private:
  std::shared_ptr<PYP> s_pyp;
  std::shared_ptr<const Expansion> s_exp;
};

#endif /* end of include guard: _SLIDINGCONSTRAINT_H_ */
