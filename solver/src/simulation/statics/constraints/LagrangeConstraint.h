#ifndef __LAGRANGECONSTRAINT__H__
#define __LAGRANGECONSTRAINT__H__

#include <memory>
#include "Constraint.h"

// class to combine multiple constraints C_i q = d_i into one global
// C = [C_0; C_1; ...], d = [d_0; d_1; ...]
class LagrangeConstraint {
 public:
  LagrangeConstraint(int ndof) : m_ndof(ndof) {
    m_C.resize(0, m_ndof);
    m_d.resize(0);
  }
  ~LagrangeConstraint() {}

  void addConstraint(std::shared_ptr<Constraint> cnstr) {
    m_constraints.push_back(cnstr);
  }

  // build individual constraint triplets and rhs vectors
  // and concatenate into global C and d
  void rebuild(scalar scale = 1.0) {
    m_d.resize(0);
    std::vector<Triplet> triplets;
    triplets.reserve(m_constraints.size() * m_ndof);  // arbitrary estimate
    for (auto cnstr : m_constraints) {
      std::vector<Triplet> triplets_i;
      VectorXs d_i;
      std::vector<Constraint::Unit> units_i;
      std::tie(triplets_i, d_i, units_i) = cnstr->build(scale);
      if (units_i.size() < 1)
        units_i = std::vector<Constraint::Unit>(d_i.rows(),
                                                Constraint::Unit::Default);
      triplets.reserve(triplets.size() + triplets_i.size());
      int rows_before = m_d.rows();
      for (const auto& triplet : triplets_i) {
        assert(triplet.col() < m_ndof);
        triplets.emplace_back(triplet.row() + rows_before, triplet.col(),
                              triplet.value());
      }
      triplets.insert(triplets.end(), triplets_i.begin(), triplets_i.end());
      m_d.conservativeResize(m_d.rows() + d_i.rows());
      m_d.tail(d_i.rows()) = d_i;
      m_units.insert(m_units.end(), units_i.begin(), units_i.end());
    }
    m_C.resize(m_d.rows(), m_ndof);
    m_C.setFromTriplets(triplets.begin(), triplets.end());
  }

  int numRows() const { return m_d.rows(); }

  // compute C q - d
  VectorXs computeResidual(const VectorXs& q) const { return m_C * q - m_d; }

  const SparseXXs& getC() const { return m_C; }
  const std::vector<Constraint::Unit>& getUnits() const { return m_units; }

 private:
  int m_ndof;
  std::vector<std::shared_ptr<Constraint>> m_constraints;
  SparseXXs m_C;
  VectorXs m_d;
  std::vector<Constraint::Unit> m_units;
};

#endif  // __LAGRANGECONSTRAINT__H__