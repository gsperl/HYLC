#ifndef _TWISTNULLSPACECONSTRAINT_H_
#define _TWISTNULLSPACECONSTRAINT_H_

#include <memory>
#include "../../pyp/PeriodicYarnPattern.h"
#include "Constraint.h"

// For each periodically connected yarn remove the nullspace of constant twist
// by requiring \int_Omega theta dOmega = 0 ---> C . g = d
class TwistNullspaceConstraint : public Constraint {
 public:
  TwistNullspaceConstraint(std::shared_ptr<PYP> pyp) : s_pyp(pyp) {}
  ~TwistNullspaceConstraint() {}

  std::tuple<std::vector<Triplet>, VectorXs, std::vector<Unit>> build(
      scalar scale) {
    // C will look something like this:
    //   1 1 1 1 1                     1 1
    //                   1 1 1 1
    //             1 1 1         1 1 1
    // where each row sums over the twist dofs of the edges that belong
    // to that row's periodic yarn.
    // similar to translation constraint, we rescale each row to units O(1),
    // here by multiplying 1/N 1/r, such that Cq-d has units O(rad)

    // compute periodically connected yarns as lists of vertices
    std::vector<std::vector<int>> pyl =
        s_pyp->computePeriodicallyConnectedYarns();
    const VectorXs& rl = s_pyp->getRestLengths();

    int N = s_pyp->Q.rows();  // note that #verts=#edges for periodic yp
    std::vector<Triplet> triplets;
    triplets.reserve(N);  // each edge will appear once
    scalar rinv = 1.0 / s_pyp->r;
    for (int i = 0; i < (int)pyl.size(); i++) {  // constraint per p. yarn
      scalar fac = rinv / (int)pyl[i].size() * scale;
      for (int vix : pyl[i]) {
        scalar l_i = rl(vix);
        triplets.emplace_back(i, 4 * vix + 3, fac * l_i);
      }
    }

    // NOTE assuming that pyp is pruned/compact, i.e. all its vertices part
    // of the periodic topology; otherwise this constraint would add
    // entries for unused vertices

    VectorXs d = VectorXs::Zero(pyl.size());  // * scale
    std::vector<Unit> units(d.rows(), Unit::Twist);
    return std::make_tuple(triplets, d, units);
  }

 private:
  std::shared_ptr<PYP> s_pyp;
};

#endif /* end of include guard: _TWISTNULLSPACECONSTRAINT_H_ */
