#ifndef _TRANSLATIONCONSTRAINT_H_
#define _TRANSLATIONCONSTRAINT_H_

#include <memory>
#include "../../pyp/PeriodicYarnPattern.h"
#include "Constraint.h"

// Constrain average of R^T u == g = 0
// \int_Omega g dOmega = 0 ---> C . g = d
class TranslationConstraint : public Constraint {
 public:
  TranslationConstraint(std::shared_ptr<PYP> pyp) : s_pyp(pyp) {}
  ~TranslationConstraint() {}

  std::tuple<std::vector<Triplet>, VectorXs, std::vector<Unit>> build(
      scalar scale) {
    // triplets with vertex i getting V_i = l_i r^2
    // where l_i = voronoi length of vertex
    //   V_i
    //       V_i      ....
    //           V_i
    // for unit scaling in Newton system we would like C's units to be O(1)
    // thus C q - d has units O(m)
    // so we divide the constraint by N r^3--> 1/N V_i/r^3 = 1/N 1/r l_i
    int nverts = s_pyp->Q.rows();  // note that #verts=#edges for periodic yp
    std::vector<Triplet> triplets;
    triplets.reserve(3 * nverts);
    const VectorXs& rl = s_pyp->getRestLengths();
    const auto& vet    = s_pyp->getVertexEdgeTable();
    scalar fac         = 1.0 / nverts * 1.0 / s_pyp->r * scale;
    for (int i = 0; i < nverts; i++) {
      scalar lprev = rl(s_pyp->E(vet(i, 0), 0));
      scalar lnext = rl(s_pyp->E(vet(i, 1), 0));
      scalar l_i = 0.5 * (lnext + lprev);
      for (int r = 0; r < 3; r++)
        triplets.emplace_back(r, 4 * i + r, fac * l_i);
    }

    // NOTE assuming that pyp is pruned/compact, i.e. all its vertices part
    // of the periodic topology; otherwise this constraint would add
    // entries for unused vertices

    VectorXs d = VectorXs::Zero(3);  // * scale
    std::vector<Unit> units(d.rows(), Unit::Position);
    return std::make_tuple(triplets, d, units);
  }

 private:
  std::shared_ptr<PYP> s_pyp;
};

#endif /* end of include guard: _TRANSLATIONCONSTRAINT_H_ */
