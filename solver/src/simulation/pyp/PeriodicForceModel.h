#ifndef __PERIODICFORCEMODEL__H__
#define __PERIODICFORCEMODEL__H__

#include "GhostYarnPattern.h"
#include "Material.h"
#include "forces/GYPForce.h"

#include "collision/AABBTreeBroadphase.h"

// wrapping ghostyarnpattern, list of forces,
// and broadphase for collision detection
class PeriodicForceModel {
 public:
  PeriodicForceModel(std::shared_ptr<GYP> gyp, const Material& material);

  void setState(const VectorXs& g, const VectorXs& dgdt, scalar dt);
  int getNumDof() const { return s_gyp->getNumDofPeriodic(); }
  auto getGYP() { return s_gyp; }

  scalar E();
  VectorXs gradE();
  SparseXXs hessE();
  void applyVelocityFilters();

 private:
  std::shared_ptr<GYP> s_gyp;
  std::shared_ptr<AABBTree> m_broadphase;
  std::vector<std::shared_ptr<GYPForce>> m_forces;
};
using PFM = PeriodicForceModel;

#endif  // __PERIODICFORCEMODEL__H__