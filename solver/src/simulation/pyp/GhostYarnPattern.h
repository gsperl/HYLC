#ifndef __GHOSTYARNPATTERN_H__
#define __GHOSTYARNPATTERN_H__

#include <memory>
#include "PeriodicYarnPattern.h"
#include "expansion/Expansion.h"
#include "forces/libWetCloth/DER/StrandForce.h"

#include "Material.h"

// an extension of the periodicyarnpattern by duplicating yarn segments
// this serves as an easy implementation of periodic forces (computing bending
// etc. via the duplicate degress of freedom and later reducing down onto
// only the original degrees of freedom of the periodic pattern)
// in addition this extension includes the shell-expansion, ie defining
// the the bent/in-plane-deformed midsurface and in turn the fluctuations u
// and the exposed degrees of freedom g = R^T u
class GhostYarnPattern : public LwcInterface,
                         public std::enable_shared_from_this<GhostYarnPattern> {
 public:
  GhostYarnPattern() {}
  // initialize reference config from pyp, extending with ghosts
  void initialize(std::shared_ptr<PYP> pyp, const Material& material,
                  scalar extpx = 0.1, scalar extpy = 0.1);
  // compute dimensions of pattern including ghost segments
  Vector3s computeExtents();
  // assuming vertices are in reference config [xi1, xi2, h]
  // precompute R and phi+hn per vertex
  void precomputeExpansionData(std::shared_ptr<Expansion> exp);
  // project ghost segment reference frames to be periodic to original
  void projectPeriodicFrames();

  // set main and ghost dofs and vels given pure dofs/vels
  void setGhostsFromPeriodic(const VectorXs& g, const VectorXs& dgdt,
                             scalar dt);
  // reduce from ghost gradient to true gradient
  VectorXs reduceGradient(const VectorXs& grad);
  // reduce from ghost hessian to true hessian
  SparseXXs reduceHessian(const SparseXXs& hess);

  // LwcInterface
  const VectorXs& getQ() const { return m_q; }
  VectorXs& getQ() { return m_q; }
  scalar computeWeight(const std::vector<int>& ixs) const;

  void saveLWCState();
  void restoreLWCState();

  // some other getters
  VectorXs& getdQdt() { return m_dqdt; }
  std::shared_ptr<PYP> getPYP() { return s_pyp; }
  std::shared_ptr<const PYP> getPYP() const { return s_pyp; }
  std::shared_ptr<const Expansion> getExpansion() const { return s_exp; }
  const std::vector<std::vector<int>>& getYL() const { return m_yl; }

  const auto getYLCoord(int i) const {
    assert(m_ycoords(i, 0) >= 0 && "trying to access coords of cut yarn");
    return m_ycoords.block<1, 2>(i, 0);
  }
  bool isPeriodic(int i) const { return m_p(i) != i; }
  template <int N = 4>
  const auto getVertex(int i) const {
    return m_q.segment<N>(4 * i);
  }
  // const auto getR(int i) const { return m_R.block<3, 3>(3 * i, 0); }
  scalar getRestLength(int i) const;

  int getNumDofPeriodic() const { return s_pyp->Q.rows() * 4; }
  int getNumDofTotal() const { return m_q.rows(); }
  int getNumVerticesTotal() const { return m_q.rows() / 4; }
  int getNumVerticesPeriodic() const { return s_pyp->Q.rows(); }

  // const vector for iterating lwc but modifying (e.g. precomputing energies)
  const std::vector<std::shared_ptr<StrandForce>>& getLWCYarns() {
    return m_lwcyarns;
  }
  // const lwc access to lwc for rendering // NOTE copy for const casting
  std::vector<std::shared_ptr<const StrandForce>> getLWCYarns() const {
    return std::vector<std::shared_ptr<const StrandForce>>(m_lwcyarns.begin(),
                                                           m_lwcyarns.end());
  }

  // set current state as rest state
  void freezeRestshapes();
  // scale rest lengths, rest curvatures, and rest twists
  void scaleRestshapes(scalar rl = 1.0, scalar rk = 1.0, scalar rt = 1.0);
  // override vertices, ref and rest data of pyp from simulated lwc
  void setPYPFromLWC();
  // get current deformed state shaped like pyp.Q
  MatrixXs getQd() const;
  // get deformed state m_q in the shape of pyp.Q ie only the true dofs, and mapped back into reference space
  MatrixXs computeQrd(scalar eps_r, int max_steps = 10) const;

 private:
  std::shared_ptr<PYP> s_pyp;
  std::shared_ptr<Expansion> s_exp;
  std::vector<std::vector<int>> m_yl;  // yarnlist inc ghosts
  VectorXi m_p;                        // periodic original vix
  VectorXs m_q;                        // dofs but flattened, inc. ghosts
  VectorXs m_dqdt;
  std::vector<std::shared_ptr<StrandForce>> m_lwcyarns;

  VectorXs m_exp_x;  // precomputed expansion location
  // MatrixXXs m_R;        // precomputed rotation
  // dq_ghost/dq_true, basically blocks [R,0;0,1], i.e. storing rotations
  SparseXXs m_dqdg;
  MatrixXXi m_ycoords;  // yarn index and in-yarn segment index per vertex

  Matrix3s getR(int i) const {
    return m_dqdg.block<3, 3>(4 * i, 4 * m_p[i], 3, 3);
  }
};
using GYP = GhostYarnPattern; // short alias of GhostYarnPattern

#endif  // __GHOSTYARNPATTERN_H__