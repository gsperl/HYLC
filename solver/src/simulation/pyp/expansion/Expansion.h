#ifndef _EXPANSION_H_
#define _EXPANSION_H_

#include "../../EigenDefinitions.h"

class Expansion {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW  // storing fixed size Eigen matrix
      Expansion() {}
  virtual ~Expansion() {}

  Vector3s x(const Vector3s &xi) const {
    return phi(xi.segment<2>(0)) + xi(2) * R(xi.segment<2>(0)).col(2);
  }

  
  Vector3s xbar(const Vector3s &X) const {
    // map X to phi(X1, X2) + X3 n(X1, X2)
    return phi(X.segment<2>(0)) + X(2) * R(X.segment<2>(0)).col(2);
  }

  Matrix3s Jbar(const Vector3s &X) const {
    // jacobian of map X to phi(X1, X2) + X3 n(X1, X2)
    // J(X) = [R(X1,X2) S + h dN | n(X1,X2)]
    // for flat strains this simplifies to J = m_S

    Matrix3s J;
    J = R(X.head<2>()) * S(); // note: S stored with last column 001, makes J column be n
    J.block<2,2>(0,0) += X(2) * dN();
    // J.col(0) += X(2) * d1;
    // J.col(1) += X(2) * d2;
    return J;
  }

  virtual Vector3s phi(const Vector2s &xi) const = 0;
  virtual Matrix3s R(const Vector2s &xi) const   = 0;
  virtual Matrix3s S() const = 0;
  virtual Matrix2s dN() const = 0;

};

#endif /* end of include guard: _EXPANSION_H_ */
