#include "GridExpansion.h"

#include "../../ThreadUtils.h"
#include "../../debug/debug_includes.h"

GridExpansion::GridExpansion(const Vector6s &ek, scalar theta, scalar _dx,
                             scalar sx, scalar sy)
    : dx(_dx) {
  // set S and n,1 n,2
  computeLinearStrain(ek, theta);

  n = int(sy / dx) + 2;  // grid rows
  m = int(sx / dx) + 2;  // grid columns
  n = std::max(5, n);
  m = std::max(5, m);

  // require odd
  if (n % 2 == 0)
    n++;
  if (m % 2 == 0)
    m++;

  computeGrid();
}

Vector3s GridExpansion::phi(const Vector2s &xi) const {
  // interpolate in corresponding quad
  // phi(xi) = (phiBL, phiBR, phiTL, phiTR) . (1-b 1-a; 1-b a; b 1-a; b a)

  // find corresponding cell (BL vertex)
  int i = int(xi(1) / dx) + fixed_i;
  int j = int(xi(0) / dx) + fixed_j;
  assert(i >= 0 && j >= 0 && i < n - 1 && j < m - 1 &&
         "accessing outside grid!");
  i = std::min(std::max(i, 0), n - 2);  // clamp anyway
  j = std::min(std::max(j, 0), m - 2);

  scalar xiL = (j - fixed_j) * dx;  // precompute?
  scalar xiB = (i - fixed_i) * dx;
  scalar a   = (xi(0) - xiL) / dx;  // interpolation factors
  scalar b   = (xi(1) - xiB) / dx;

  auto phiBL        = phi_grid.row(i * m + j);
  auto phiBR        = phi_grid.row(i * m + j + 1);
  auto phiTL        = phi_grid.row((i + 1) * m + j);
  auto phiTR        = phi_grid.row((i + 1) * m + j + 1);
  Vector3s phivalue = (1 - b) * (1 - a) * phiBL + (1 - b) * a * phiBR +
                      b * (1 - a) * phiTL + b * a * phiTR;
  return phivalue;
}

Matrix3s GridExpansion::R(const Vector2s &xi) const {
  // NOTE: could consider using the analytic expression for R, instead of using
  // finite differences (for singly curved deformations) dn = xi(0) * dn,1 +
  // xi(1) * dn,2; computeR(dn(0), dn(1));

  // F(xi) = R(xi) S
  // we can construct R at any xi from local derivatives
  // ie compute a1, a2, n=(a1xa2)/|a1xa2|
  // R = (a1/|a1|, nxa1/|nxxa1|, n)
  // a1 = (phiBL, phiBR, phiTL, phiTR) . (1-b -da; 1-b da; b -da; b da)
  // a2 = (phiBL, phiBR, phiTL, phiTR) . (-db 1-a; -db a; db 1-a; db a)

  // find corresponding cell (BL vertex)
  int i = int(xi(1) / dx) + fixed_i;
  int j = int(xi(0) / dx) + fixed_j;
  assert(i >= 0 && j >= 0 && i < n - 1 && j < m - 1 &&
         "accessing outside grid!");
  i = std::min(std::max(i, 0), n - 2);  // clamp anyway
  j = std::min(std::max(j, 0), m - 2);

  scalar invdx = 1.0 / dx;
  scalar xiL   = (j - fixed_j) * dx;  // precompute?
  scalar xiB   = (i - fixed_i) * dx;
  scalar a     = (xi(0) - xiL) / dx;  // interpolation factors
  scalar b     = (xi(1) - xiB) / dx;
  auto phiBL   = phi_grid.row(i * m + j);
  auto phiBR   = phi_grid.row(i * m + j + 1);
  auto phiTL   = phi_grid.row((i + 1) * m + j);
  auto phiTR   = phi_grid.row((i + 1) * m + j + 1);
  // compute tangents from derivative of bilinear interpolation
  Vector3s a1 = (1 - b) * (-invdx) * phiBL + (1 - b) * invdx * phiBR +
                b * (-invdx) * phiTL + b * invdx * phiTR;
  Vector3s a2 = (-invdx) * (1 - a) * phiBL + (-invdx) * a * phiBR +
                invdx * (1 - a) * phiTL + invdx * a * phiTR;

  // normal
  Vector3s nn = (a1.cross(a2)).normalized();

  Matrix3s R;

  if (true) {
    // using S = sqrt[FtF], ie R = polar_rot (F)
    Matrix3s F;
    F.col(0) = a1;
    F.col(1) = a2;
    F.col(2) = nn;
    Eigen::JacobiSVD<Matrix3s> svd(F,
                                   Eigen::ComputeFullU | Eigen::ComputeFullV);
    R = svd.matrixU() * svd.matrixV().transpose();

  } else {
    // using my S, ie R = my R
    R.col(0) = a1.normalized();
    R.col(1) = nn.cross(a1.normalized());  // already normalized.
    R.col(2) = nn;
  }

  return R;
}

void GridExpansion::computeLinearStrain(const Vector6s &ek, scalar theta) {
  // note: we only use theta=0, but this angle can be used to rate the alignment
  // of input strain axes.

  Matrix2s Q = Eigen::Rotation2D<scalar>(theta).toRotationMatrix();

  Matrix2s S_theta;
  scalar sx = ek(0), sa = ek(1), sy = ek(2);
  S_theta << sx + 1, sa * (sy + 1), 0, std::sqrt(1 - sa * sa) * (sy + 1);

  Eigen::JacobiSVD<Matrix2s> svd(S_theta,
                                 Eigen::ComputeFullU | Eigen::ComputeFullV);
  S_theta = svd.matrixV() * svd.singularValues().asDiagonal() *
            svd.matrixV().transpose();

  m_S.setIdentity();
  m_S.block<2, 2>(0, 0) = Q * S_theta * Q.transpose();

  Matrix2s K_theta;
  K_theta << ek(3), ek(4), ek(4), ek(5);
  Matrix2s K = Q * K_theta * Q.transpose();
  Matrix2s A = m_S.block<2, 2>(0, 0);

  m_dN = -A.transpose().inverse() * K;  // K == II
}

// sin(x)/x, with taylor around 0 for numerical stability
scalar GridExpansion::sinc(scalar x) const {
  if (std::abs(x) < 1e-10) {
    // 4th order taylor approximation
    // 2nd might be enough but is cheaper than sin anyway
    scalar x2                = x * x;
    static const scalar i6   = 1.0 / 6.0;
    static const scalar i120 = 1.0 / 120.0;
    return 1 - x2 * i6 + x2 * x2 * i120;
  } else {
    return std::sin(x) / x;
  }
}

Matrix3s GridExpansion::computeR(const scalar &a, const scalar &b) const {
  Matrix3s R;

  // this is a numerically safe (because using sinc) formula of constructing
  // a rodrigues rotation matrix with u={-b,a,0}
  // or equivalently of computing e^(sum_alpha Omega_alpha xi_alpha)
  // i.e. e^(0,0,a; 0,0,b; -a,-b,0)
  scalar theta = std::sqrt(a * a + b * b);
  scalar c0    = sinc(theta);
  scalar c1    = sinc(theta / 2);
  c1           = 0.5 * c1 * c1;

  R << 1 - a * a * c1, -a * b * c1, a * c0, -a * b * c1, 1 - b * b * c1, b * c0,
      -a * c0, -b * c0, std::cos(theta);

  // assert((R.transpose() * R - Matrix3s::Identity()).norm() < 1e-15);
  return R;
}

Matrix3s GridExpansion::computeF(const Vector2s &xi) const {
  // F = R (a,b) S
  // (a; b) = xi1 n,1 + xi2 n,2
  Vector2s dn = m_dN * xi;  // xi(0) * d1 + xi(1) * d2;
  Matrix3s R  = computeR(dn(0), dn(1));
  return R * m_S;
}

// solve \nabla^2 phi = \nabla \cdot F
// with Neumann boundary \nabla phi |cdot N = F \cdot N
void GridExpansion::computeGrid() {
  int ndof = n * m;  // scalar for each grid node

  // Dirichlet vertex (assuming here that this is vertex at xi1,xi2=0,0)
  fixed_i = int(n / 2);
  fixed_j = int(m / 2);

  MatrixXXs rhs(ndof, 3);  // all three right hand sides as columns

  // build lhs, i.e. discrete laplacian with neumann boundary conditions
  // each row can have up to 5 entries (if not used, simply set to 0)
  std::vector<Triplet> triplets(ndof * 5);

  threadutils::parallel_for(0, ndof, [&](int ix) {  // parallel per vertex
    // for (int ix = 0; ix < ndof; ++ix) {
    int i = int(ix / m);
    int j = ix % m;

    Vector2s xi;
    xi << (j - fixed_j) * dx, (i - fixed_i) * dx;

    addLaplaceNeumannRow(i, j, n, m, dx, xi, triplets, rhs);
  });
  // }

  // remove nullspace from rhs by projecting with (I - ee^T), e is vec of ones
  {
    Vector3s sum = Vector3s::Zero();
    for (int i = 0; i < ndof; ++i) {
      sum += rhs.row(i);
    }
    sum *= 1.0 / ndof;

    threadutils::parallel_for(0, ndof, [&](int ix) { rhs.row(ix) -= sum; });
  }

  // construct L
  SparseXXs L(ndof, ndof);
  L.setFromTriplets(triplets.begin(), triplets.end());

  // factorize
  Eigen::SimplicialLDLT<SparseXXs> solver(L);
  // Eigen::ConjugateGradient<SparseXXs,Eigen::Lower|Eigen::Upper> solver(L);

  if (solver.info() != Eigen::Success)
    Debug::errorf("Linear solver failed to factorize!\n");

  // solve (column-wise)
  phi_grid.resize(ndof, 3);
  for (int i = 0; i < 3; ++i) {  // serial for now, bc fast anyway
    phi_grid.col(i) =
        solver.solve(rhs.col(i)).head(ndof);  // headndof bc lambda

    if (solver.info() != Eigen::Success) {
      Debug::errorf("Linear solver failed to solve!\n");
      Debug::logf("Error %d: %.2e\n", i,
                  (L * phi_grid.col(i) - rhs.col(i)).norm());
    }
  }

  // shift solution s.t. phi(0,0)=(0,0,0)
  Vector3s dphi = phi_grid.row(fixed_i * m + fixed_j);
  threadutils::parallel_for(0, ndof, [&](int ix) { phi_grid.row(ix) -= dphi; });
}

void GridExpansion::addLaplaceNeumannRow(int i, int j, int n, int m, scalar dx,
                                         const Vector2s &xi,
                                         std::vector<Triplet> &triplets,
                                         MatrixXXs &rhs) {
  bool bdryL = j == 0;
  bool bdryR = j == m - 1;
  bool bdryB = i == 0;
  bool bdryT = i == n - 1;
  int ix     = i * m + j;

  int n_bounds = int(bdryL) + int(bdryR) + int(bdryT) + int(bdryB);
  DECLARE_UNUSED(n_bounds);
  assert(!(bdryL && bdryR) && !(bdryB && bdryT) && n_bounds < 3);

  Triplet empty(ix, ix, 0.0);
  rhs.row(ix).setZero();
  int diag = 0.0;

  Vector2s dx1, dx2;
  dx1 << dx, 0;
  dx2 << 0, dx;

  if (!bdryL) {
    rhs.row(ix) += computeF(xi - 0.5 * dx1).col(0) * dx;
    diag++;
    triplets[ix * 5 + 4] = Triplet(ix, i * m + j - 1, -1);
  }
  if (!bdryR) {
    rhs.row(ix) -= computeF(xi + 0.5 * dx1).col(0) * dx;
    diag++;
    triplets[ix * 5 + 3] = Triplet(ix, i * m + j + 1, -1);
  }
  if (!bdryB) {
    rhs.row(ix) += computeF(xi - 0.5 * dx2).col(1) * dx;
    diag++;
    triplets[ix * 5 + 2] = Triplet(ix, (i - 1) * m + j, -1);
  }
  if (!bdryT) {
    rhs.row(ix) -= computeF(xi + 0.5 * dx2).col(1) * dx;
    diag++;
    triplets[ix * 5 + 1] = Triplet(ix, (i + 1) * m + j, -1);
  }

  triplets[ix * 5 + 0] = Triplet(ix, ix, diag);
}
