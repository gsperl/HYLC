#ifndef _GRIDEXPANSION_H_
#define _GRIDEXPANSION_H_

#include "Expansion.h"
// #include "../EigenDefinitions.h"

typedef Eigen::Matrix<scalar, 6, 1> Vector6s;

class GridExpansion : public Expansion {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;  // storing fixed size Eigen matrix
  GridExpansion(const Vector6s &ek, scalar theta, scalar _dx, scalar sx,
                scalar sy);
  virtual ~GridExpansion() {}

  virtual Vector3s phi(const Vector2s &xi) const;
  virtual Matrix3s R(const Vector2s &xi) const;
  // virtual Matrix3s Fh0(const Vector2s &xi) const;
  virtual Matrix3s S() const { return m_S; }
  virtual Matrix2s dN() const { return m_dN; }
  // virtual void initializeState(YarnPatch &yp); // DEBUG tried out volume
  // preserving init

  int getN() const { return n; }
  int getM() const { return m; }
  const MatrixXXs &getValues() const { return phi_grid; }

 private:
  int n, m;
  int fixed_i, fixed_j;  // vertex grid index where phi = (0,0)
  scalar dx;
  Matrix3s m_S;          // local tangent/normal frame
  Matrix2s m_dN;          // normal derivatives
  MatrixXXs phi_grid;  // surface values

  // compute S and dN from ek=(sx,sa,sy,II_00,II_01,II_11)
  void computeLinearStrain(const Vector6s &ek, scalar theta);

  // sin(x)/x, with taylor around 0 for numerical stability
  scalar sinc(scalar x) const;
  Matrix3s computeR(const scalar &a, const scalar &b) const;
  Matrix3s computeF(const Vector2s &xi) const;

  // solve \nabla^2 phi = \nabla \cdot F
  // with Neumann boundary \nabla phi |cdot N = F \cdot N
  void computeGrid();
  
  void addLaplaceNeumannRow(int i, int j, int n, int m, scalar dx,
                            const Vector2s &xi, std::vector<Triplet> &triplets,
                            MatrixXXs &rhs);
};

#endif /* end of include guard: _GRIDEXPANSION_H_ */
