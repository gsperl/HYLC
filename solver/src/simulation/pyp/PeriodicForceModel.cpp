#include "PeriodicForceModel.h"
#include "../ThreadUtils.h"
#include "../debug/debug_includes.h"
#include "forces/DERForce.h"
#include "forces/YarnCollisionForce.h"

PFM::PeriodicForceModel(std::shared_ptr<GYP> gyp, const Material& material)
    : s_gyp(gyp) {
  if (!Debug::get<bool>("no_DER_force"))
    m_forces.push_back(std::make_shared<DERForce>(gyp));

  // initialize collision broadphase and force
  if (material.kc > 0 && !Debug::get<bool>("no_COL_force")) {
    // ensure that GYP is in curved deformed state for static AABB hierarchy
    VectorXs g = VectorXs::Zero(s_gyp->getNumDofPeriodic());
    VectorXs dgdt;  // empty
    scalar dt = -1;
    s_gyp->setGhostsFromPeriodic(g, dgdt, dt);

    // compute minimum neighboring segments to ignore self-collision
    // should be 1 + ceil(2r/lmin), taking 3r for extra tolerance
    scalar minlen = std::numeric_limits<scalar>::max();
    for (const auto& yarn : s_gyp->getLWCYarns())
      for (int i = 0; i < yarn->getNumEdges(); ++i)
        minlen = std::min(minlen, yarn->m_strandState->m_lengths[i]);
    int selfcollisiontolerance =
        1 + std::ceil(0.1 + 3 * s_gyp->getPYP()->r / minlen);
    Debug::log("Self-col. tol.:", selfcollisiontolerance);

    int min_quadrature_steps = 5;
    double max_quadrature_step  = s_gyp->getPYP()->r * 0.75;

    m_broadphase = std::make_shared<AABBTree>(
        s_gyp, s_gyp->getNumVerticesTotal(), selfcollisiontolerance,
        max_quadrature_step, min_quadrature_steps);
    const auto& yl = s_gyp->getYL();
    for (auto& yarn : yl) {
      for (int i = 0; i < (int)yarn.size() - 1; ++i) {
        int v0        = yarn[i];
        int v1        = yarn[i + 1];
        bool periodic = s_gyp->isPeriodic(v0) && s_gyp->isPeriodic(v1);
        AABB aabb =
            AABB::fromPoints(s_gyp->getVertex<3>(v0), s_gyp->getVertex<3>(v1));
        aabb.fatten(s_gyp->getPYP()->r);
        m_broadphase->insertLeaf(aabb, v0, periodic);
      }
    }
    m_broadphase->finalize();
    m_broadphase->computeLeafAABBs();
    m_broadphase->estimateCollisionPairs();

    m_forces.push_back(
        std::make_shared<YarnCollisionForce>(s_gyp, m_broadphase, material.kc));
  }
}

void PFM::setState(const VectorXs& g, const VectorXs& dgdt, scalar dt) {
  s_gyp->setGhostsFromPeriodic(g, dgdt, dt);

  if (m_broadphase) {
    // update aabbs of edges and propagate in static AABB tree
    m_broadphase->computeLeafAABBs();
    m_broadphase->parallelAABBupdate(true);
    // estimate broadphase collision pairs
    m_broadphase->estimateCollisionPairs();
  }
}

scalar PFM::E() {
  scalar E = 0;
  threadutils::parallel_for((size_t)0, m_forces.size(), [&](size_t i) {
    m_forces[i]->precompute(true, false, false);
  });
  for (size_t i = 0; i < m_forces.size(); i++) {
    m_forces[i]->addE(E);
  }
  threadutils::parallel_for((size_t)0, m_forces.size(),
                        [&](size_t i) { m_forces[i]->postcompute(); });
  return E;
}

VectorXs PFM::gradE() {
  VectorXs gradE = VectorXs::Zero(s_gyp->getNumDofTotal());
  threadutils::parallel_for((size_t)0, m_forces.size(), [&](size_t i) {
    m_forces[i]->precompute(false, true, false);
  });
  for (size_t i = 0; i < m_forces.size(); i++) {
    m_forces[i]->addGradE(gradE);
  }
  threadutils::parallel_for((size_t)0, m_forces.size(),
                        [&](size_t i) { m_forces[i]->postcompute(); });

  return s_gyp->reduceGradient(gradE);
}

SparseXXs PFM::hessE() {
  int num_hess = 0;
  int offset = 0;
  std::vector<int> tripletOffsets;
  threadutils::parallel_for((size_t)0, m_forces.size(), [&](size_t i) {
    m_forces[i]->precompute(false, false, true);
  });

  tripletOffsets.resize(m_forces.size());
  for (size_t i = 0; i < m_forces.size(); ++i) {
    tripletOffsets[i] = num_hess;
    num_hess += m_forces[i]->numHess();
  }

  std::vector<Triplet> triplets;
  if (offset + num_hess > (int)triplets.size())
    triplets.resize(offset + num_hess);

  // hessian
  threadutils::parallel_for(0, (int)m_forces.size(), [&](int i) {
    m_forces[i]->addHessE(triplets, offset + tripletOffsets[i]);
  });

  threadutils::parallel_for((size_t)0, m_forces.size(),
                        [&](size_t i) { m_forces[i]->postcompute(); });

  int ntotal = s_gyp->getNumDofTotal();
  SparseXXs H(ntotal, ntotal);
  H.setFromTriplets(triplets.begin(), triplets.end());

  return s_gyp->reduceHessian(H);
}

void PFM::applyVelocityFilters() {
  Debug::error("velocity filters not implemented");
  assert(false);
}
