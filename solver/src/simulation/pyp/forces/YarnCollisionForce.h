#ifndef _YARNCOLLISIONFORCE_H_
#define _YARNCOLLISIONFORCE_H_

#include "../collision/YarnBroadphase.h"
#include "GYPForce.h"

#define YARNCOLLISIONFORCEFUNCTION 0 // 0 for (h)ylc, 2 for C2-continuous

class YarnCollisionForce : public GYPForce {
public:
  typedef Eigen::Matrix<scalar, 12, 1> LocalGradType;
  typedef Eigen::Matrix<scalar, 12, 12> LocalHessType;


  YarnCollisionForce(std::shared_ptr<GYP> gyp,std::shared_ptr<YarnBroadphase> broadphase,
                     scalar kc);
  virtual ~YarnCollisionForce();

  virtual void precompute(bool energy = true, bool gradient = true,
                          bool hessian   = true);
  virtual void addE(scalar &E);
  virtual void addGradE(VectorXs &gradE);
  virtual void addHessE(std::vector<Triplet> &hessE, int offset);
  virtual int numHess();
  virtual void postcompute();

private:
  scalar m_E;
  VectorXs m_gradE;
  std::vector<Triplet> m_hessE;

  std::shared_ptr<YarnBroadphase> s_broadphase;

  scalar m_kc;

  void accumulateLocal(scalar &E, const Vector3s &e0v0, const Vector3s &e0v1,
                    const Vector3s &e1v0, const Vector3s &e1v1,
                    const scalar &rs, const scalar &rt, const scalar &s,
                    const scalar &t);
  void accumulateLocal(LocalGradType &gradE, const Vector3s &e0v0,
                    const Vector3s &e0v1, const Vector3s &e1v0,
                    const Vector3s &e1v1, const scalar &rs, const scalar &rt,
                    const scalar &s, const scalar &t);
  void accumulateLocal(LocalHessType &hessE, const Vector3s &e0v0,
                    const Vector3s &e0v1, const Vector3s &e1v0,
                    const Vector3s &e1v1, const scalar &rs, const scalar &rt,
                    const scalar &s, const scalar &t);

};

#endif /* end of include guard: _YARNCOLLISIONFORCE_H_ */
