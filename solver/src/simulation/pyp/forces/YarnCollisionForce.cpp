#include "YarnCollisionForce.h"
#include <math.h>
#include "../../ThreadUtils.h"
#include "../../debug/debug_includes.h"

YarnCollisionForce::YarnCollisionForce(
    std::shared_ptr<GYP> gyp, std::shared_ptr<YarnBroadphase> broadphase,
    scalar kc)
    : GYPForce(gyp), s_broadphase(broadphase), m_kc(kc) {}

YarnCollisionForce::~YarnCollisionForce() {}

void YarnCollisionForce::precompute(bool energy, bool gradient, bool hessian) {
  auto &collisions              = s_broadphase->getCollisions();
  const auto &quadraturesamples = s_broadphase->getQuadratureSamples();

  bool use_hessian = true;

  // Narrow phase collision detection

  // temporary threadstafe per collision output
  std::vector<scalar> v_E(collisions.size());
  AlignedVector<LocalGradType> v_gradE(collisions.size());
  AlignedVector<LocalHessType> v_hessE(collisions.size());
  // compute parallel foreach collision pair
  threadutils::parallel_for(0, (int)collisions.size(), [&](int i) {
    const auto &clsn = collisions[i];

    // fractional counting
    scalar mult_periodic;
    assert(clsn.e0v0 >= 0 && clsn.e0v1 >= 0 && clsn.e1v0 >= 0 &&
           clsn.e1v1 >= 0);
    int sum_prd = s_gyp->isPeriodic(clsn.e0v0) + s_gyp->isPeriodic(clsn.e0v1) +
                  s_gyp->isPeriodic(clsn.e1v0) + s_gyp->isPeriodic(clsn.e1v1);

    mult_periodic = (4 - sum_prd) * 0.25;
    assert(sum_prd < 4 &&
           "ghost on ghost collision not ignored in broadphase!");

    // prep local stuff
    scalar &local_E = v_E[i];
    local_E         = 0;

    LocalGradType &local_gradE = v_gradE[i];
    local_gradE.setZero();

    LocalHessType &local_hessE = v_hessE[i];
    local_hessE.setZero();

    int nsteps0 = quadraturesamples[clsn.e0v0];
    int nsteps1 = quadraturesamples[clsn.e1v0];
    assert(nsteps0 > 0 && nsteps0 < 100 && nsteps1 > 0 && nsteps1 < 100);

    // quadrature weights / step size
    scalar ds = 1.0 / (nsteps0 - 1);
    scalar dt = 1.0 / (nsteps1 - 1);
    // rectangle rule: dx [1 + 1 + ... + 1]
    // simpson's rule: dx 1/3 [1 + 4 + 2 + .. + 4 + 1]
    scalar simps1 = 1.0 / 3.0;
    scalar simps4 = 4.0 / 3.0;
    scalar simps2 = 2.0 / 3.0;

    // precompute (2r)^2 part, here potentially could allow different radii
    scalar r    = s_gyp->getPYP()->r;
    scalar r_e0 = r, r_e1 = r;
    scalar r2 = (r_e0 + r_e1) * (r_e0 + r_e1);

    // iterate quadrature points
    scalar s = 0;
    for (int is = 0; is < nsteps0; is++) {
      scalar t        = 0;
      scalar quadrw_s = 1.0;
      if (nsteps0 > 4)  // rectangle rule or simpsons rule
        quadrw_s = (is == 0 || is == nsteps0 - 1)
                       ? simps1
                       : (is % 2 == 1 ? simps4 : simps2);

      Vector3s e0v0_x = s_gyp->getVertex<3>(clsn.e0v0);
      Vector3s e0v1_x = s_gyp->getVertex<3>(clsn.e0v1);
      Vector3s e1v0_x = s_gyp->getVertex<3>(clsn.e1v0);
      Vector3s e1v1_x = s_gyp->getVertex<3>(clsn.e1v1);

      for (int it = 0; it < nsteps1; it++) {
        // NOTE: linear interpolation!
        const scalar d2 =
            (((1 - s) * e0v0_x + s * e0v1_x) - ((1 - t) * e1v0_x + t * e1v1_x))
                .squaredNorm() /
            r2;

        // if d >= 1 -> E = 0
        if (d2 < 1) {
          scalar quadrw_t = 1.0;
          if (nsteps1 > 4)  // rectangle rule or simpsons rule
            quadrw_t = (it == 0 || it == nsteps1 - 1)
                           ? simps1
                           : (it % 2 == 1 ? simps4 : simps2);

          if (energy) {
            scalar tmp = 0;
            accumulateLocal(tmp, e0v0_x, e0v1_x, e1v0_x, e1v1_x, r_e0, r_e1, s,
                            t);
            tmp *= quadrw_s * quadrw_t;
            local_E += tmp;
          }
          if (gradient) {
            LocalGradType tmp = LocalGradType::Zero();
            accumulateLocal(tmp, e0v0_x, e0v1_x, e1v0_x, e1v1_x, r_e0, r_e1, s,
                            t);
            tmp *= quadrw_s * quadrw_t;
            local_gradE += tmp;
          }
          if (hessian && use_hessian) {
            LocalHessType tmp = LocalHessType::Zero();
            accumulateLocal(tmp, e0v0_x, e0v1_x, e1v0_x, e1v1_x, r_e0, r_e1, s,
                            t);
            tmp *= quadrw_s * quadrw_t;
            local_hessE += tmp;
          }
        }

        t += dt;
      }
      s += ds;
    }

    // multiply remaining coefficients
    scalar l0 = s_gyp->getRestLength(clsn.e0v0);
    scalar l1 = s_gyp->getRestLength(clsn.e1v0);
    assert(l0 > 0 && l1 > 0 && "trying to get restlength of a tip vertex");
    scalar coeff = mult_periodic * m_kc * l0 * l1 * ds * dt;
    if (energy)
      local_E *= coeff;
    if (gradient)
      local_gradE *= coeff;
    if (hessian && use_hessian)
      local_hessE *= coeff;
  });

  // prep global quantities
  if (energy)
    m_E = 0;
  if (gradient) {
    m_gradE.resize(s_gyp->getNumDofTotal());
    m_gradE.setZero();
  }
  if (hessian) {
    m_hessE.clear();
    if (use_hessian)
      m_hessE.reserve(collisions.size() * 12 * 12);
  }
  // accumulate into global stuff (sequential)
  for (size_t i = 0; i < collisions.size(); ++i) {
    const auto &clsn = collisions[i];
    if (energy)
      m_E += v_E[i];
    if (gradient) {
      m_gradE.segment<3>(clsn.e0v0 * 4) += v_gradE[i].segment<3>(0);
      m_gradE.segment<3>(clsn.e0v1 * 4) += v_gradE[i].segment<3>(3);
      m_gradE.segment<3>(clsn.e1v0 * 4) += v_gradE[i].segment<3>(6);
      m_gradE.segment<3>(clsn.e1v1 * 4) += v_gradE[i].segment<3>(9);
    }
    if (hessian && use_hessian) {
      const auto &local_hessE = v_hessE[i];
      for (size_t hi = 0; hi < 12; hi++) {
        int row = hi < 3 ? (clsn.e0v0 * 4) + hi
                         : (hi < 6 ? (clsn.e0v1 * 4) + hi - 3
                                   : (hi < 9 ? (clsn.e1v0 * 4) + hi - 6
                                             : (clsn.e1v1 * 4) + hi - 9));

        for (size_t hj = 0; hj < 12; hj++) {
          int col = hj < 3 ? (clsn.e0v0 * 4) + hj
                           : (hj < 6 ? (clsn.e0v1 * 4) + hj - 3
                                     : (hj < 9 ? (clsn.e1v0 * 4) + hj - 6
                                               : (clsn.e1v1 * 4) + hj - 9));
          if (std::abs(local_hessE(hi, hj)) > hess_epsilon)
            m_hessE.emplace_back(row, col, local_hessE(hi, hj));
        }
      }
    }
  }
}

void YarnCollisionForce::addE(scalar &E) { E += m_E; }

void YarnCollisionForce::addGradE(VectorXs &gradE) {
  threadutils::parallel_for(0, (int)gradE.rows() / 4, [&](int i) {
    gradE.segment<3>(4 * i) += m_gradE.segment<3>(4 * i);
  });
}

void YarnCollisionForce::addHessE(std::vector<Triplet> &hessE,
                                  int forceTripletOffset) {
  threadutils::parallel_for(0, (int)m_hessE.size(), [&](int i) {
    hessE[forceTripletOffset + i] = m_hessE[i];
  });
}

int YarnCollisionForce::numHess() { return (int)m_hessE.size(); }
void YarnCollisionForce::postcompute() {
  // m_gradE.resize(0)
  m_hessE.clear();
}
