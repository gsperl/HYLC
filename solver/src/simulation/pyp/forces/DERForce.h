#ifndef _DERFORCE_H_
#define _DERFORCE_H_

#include <memory>
#include "GYPForce.h"

// (wrapper to libwetcloth strandforce)
// TODO sometime in the future replace this with
//  individual self-made bending / stretching / twisting forces
class DERForce : public GYPForce {
 public:
  DERForce(std::shared_ptr<GYP> gyp);
  virtual ~DERForce();

  virtual void precompute(bool energy = true, bool gradient = true,
                          bool hessian   = true);
  virtual void addE(scalar &E);
  virtual void addGradE(VectorXs &gradE);
  virtual void addHessE(std::vector<Triplet> &hessE, int offset);
  virtual int numHess();
  virtual void postcompute();
};

#endif /* end of include guard: _DERFORCE_H_ */
