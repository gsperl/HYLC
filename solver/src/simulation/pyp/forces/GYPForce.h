#ifndef _GYPFORCE_H_
#define _GYPFORCE_H_

#include <memory>
#include <vector>

#include "../../EigenDefinitions.h"
#include "../GhostYarnPattern.h"

// Abstract class for forces on 'ghostyarnpatterns' e.g. DERForce.h
class GYPForce {
 public:
  GYPForce(std::shared_ptr<GYP> gyp) : s_gyp(gyp) {}
  virtual ~GYPForce() {}

  virtual void precompute(bool energy = true, bool gradient = true,
                          bool hessian   = true) {}
  virtual void addE(scalar &E)           = 0;
  virtual void addGradE(VectorXs &gradE) = 0;
  virtual void addHessE(std::vector<Triplet> &hessE, int offset) {}
  virtual int numHess() { return 0; }
  virtual void postcompute() {}

 protected:
  std::shared_ptr<GYP> s_gyp;
  scalar hess_epsilon = 1e-12;  // deciding if hessian entry is zero
};

#endif /* end of include guard: _GYPFORCE_H_ */
