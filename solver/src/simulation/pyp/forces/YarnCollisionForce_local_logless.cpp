#include "YarnCollisionForce.h"
#if YARNCOLLISIONFORCEFUNCTION == 2
#include "../../MathematicaDefinitions.h"
using namespace Mathematica;

void YarnCollisionForce::accumulateLocal(scalar &E, const Vector3s &e0v0,
                                         const Vector3s &e0v1,
                                         const Vector3s &e1v0,
                                         const Vector3s &e1v1, const scalar &rs,
                                         const scalar &rt, const scalar &s,
                                         const scalar &t) {
  // define outputs
  auto out = [&](int i) -> Real & { return E; };
  // auto &out = E;

  // ---------------------------- //
  Real c7 = e1v0(0);
  Real c2 = -1 + s;
  Real c18 = e1v0(1);
  Real c29 = e1v0(2);
  Real c1 = rs + rt;
  Real c3 = e0v0(0);
  Real c4 = c2 * c3;
  Real c5 = e0v1(0);
  Real c6 = -(s * c5);
  Real c8 = -(t * c7);
  Real c9 = e1v1(0);
  Real c10 = t * c9;
  Real c11 = c10 + c4 + c6 + c7 + c8;
  Real c12 = Power(c11, 2);
  Real c14 = e0v0(1);
  Real c15 = c14 * c2;
  Real c16 = e0v1(1);
  Real c17 = -(s * c16);
  Real c19 = -(t * c18);
  Real c21 = e1v1(1);
  Real c22 = t * c21;
  Real c23 = c15 + c17 + c18 + c19 + c22;
  Real c24 = Power(c23, 2);
  Real c25 = e0v0(2);
  Real c26 = c2 * c25;
  Real c27 = e0v1(2);
  Real c28 = -(s * c27);
  Real c30 = -(t * c29);
  Real c31 = e1v1(2);
  Real c32 = t * c31;
  Real c33 = c26 + c28 + c29 + c30 + c32;
  Real c34 = Power(c33, 2);
  Real c35 = c12 + c24 + c34;
  out(0) +=
      (-3 + c1 / Sqrt(c35) + (3 * Sqrt(c35)) / c1 - c35 / Power(c1, 2)) / 2.;
  // ---------------------------- //
}

void YarnCollisionForce::accumulateLocal(
    Eigen::Matrix<scalar, 12, 1> &gradE, const Vector3s &e0v0,
    const Vector3s &e0v1, const Vector3s &e1v0, const Vector3s &e1v1,
    const scalar &rs, const scalar &rt, const scalar &s, const scalar &t) {

  // define outputs
  // auto out = [&](int i) -> Real & { return gradE(i); };
  auto &out = gradE;

  // ---------------------------- //
  Real c3 = -1 + s;
  Real c8 = e1v0(0);
  Real c1 = rs + rt;
  Real c4 = e0v0(0);
  Real c5 = c3 * c4;
  Real c6 = e0v1(0);
  Real c7 = -(s * c6);
  Real c9 = -(t * c8);
  Real c10 = e1v1(0);
  Real c11 = t * c10;
  Real c12 = c11 + c5 + c7 + c8 + c9;
  Real c21 = e1v0(1);
  Real c31 = e1v0(2);
  Real c15 = Power(c12, 2);
  Real c16 = e0v0(1);
  Real c17 = c16 * c3;
  Real c18 = e0v1(1);
  Real c19 = -(s * c18);
  Real c22 = -(t * c21);
  Real c23 = e1v1(1);
  Real c24 = t * c23;
  Real c25 = c17 + c19 + c21 + c22 + c24;
  Real c26 = Power(c25, 2);
  Real c27 = e0v0(2);
  Real c28 = c27 * c3;
  Real c29 = e0v1(2);
  Real c30 = -(s * c29);
  Real c32 = -(t * c31);
  Real c33 = e1v1(2);
  Real c34 = t * c33;
  Real c35 = c28 + c30 + c31 + c32 + c34;
  Real c36 = Power(c35, 2);
  Real c37 = c15 + c26 + c36;
  Real c2 = Power(c1, -2);
  Real c14 = Power(c1, 3);
  Real c38 = Power(c37, -1.5);
  Real c39 = -(c14 * c38);
  Real c40 = 1 / Sqrt(c37);
  Real c42 = 3 * c1 * c40;
  Real c43 = -2 + c39 + c42;
  Real c47 = c14 * c38;
  Real c48 = -3 * c1 * c40;
  Real c49 = 2 + c47 + c48;
  Real c53 = -t;
  Real c54 = 1 + c53;
  out(0) += (c12 * c2 * c3 * c43) / 2.;
  out(1) += (c2 * c25 * c3 * c43) / 2.;
  out(2) += (c2 * c3 * c35 * c43) / 2.;
  out(3) += (s * c12 * c2 * c49) / 2.;
  out(4) += (s * c2 * c25 * c49) / 2.;
  out(5) += (s * c2 * c35 * c49) / 2.;
  out(6) += (c12 * c2 * c43 * c54) / 2.;
  out(7) += (c2 * c25 * c43 * c54) / 2.;
  out(8) += (c2 * c35 * c43 * c54) / 2.;
  out(9) += (t * c12 * c2 * c43) / 2.;
  out(10) += (t * c2 * c25 * c43) / 2.;
  out(11) += (t * c2 * c35 * c43) / 2.;
  // ---------------------------- //
}

void YarnCollisionForce::accumulateLocal(
    Eigen::Matrix<scalar, 12, 12> &hessE, const Vector3s &e0v0,
    const Vector3s &e0v1, const Vector3s &e1v0, const Vector3s &e1v1,
    const scalar &rs, const scalar &rt, const scalar &s, const scalar &t) {

  // define outputs
  // auto out = [&](int i, int j) -> Real & { return hessE(i, j); };
  auto &out = hessE;

  // ---------------------------- //
  Real c1 = rs + rt;
  Real c3 = -1 + s;
  Real c10 = e1v0(0);
  Real c22 = e1v0(1);
  Real c32 = e1v0(2);
  Real c6 = e0v0(0);
  Real c7 = c3 * c6;
  Real c8 = e0v1(0);
  Real c9 = -(s * c8);
  Real c11 = -(t * c10);
  Real c12 = e1v1(0);
  Real c14 = t * c12;
  Real c15 = c10 + c11 + c14 + c7 + c9;
  Real c16 = Power(c15, 2);
  Real c17 = e0v0(1);
  Real c18 = c17 * c3;
  Real c19 = e0v1(1);
  Real c21 = -(s * c19);
  Real c23 = -(t * c22);
  Real c24 = e1v1(1);
  Real c25 = t * c24;
  Real c26 = c18 + c21 + c22 + c23 + c25;
  Real c27 = Power(c26, 2);
  Real c28 = e0v0(2);
  Real c29 = c28 * c3;
  Real c30 = e0v1(2);
  Real c31 = -(s * c30);
  Real c33 = -(t * c32);
  Real c34 = e1v1(2);
  Real c35 = t * c34;
  Real c36 = c29 + c31 + c32 + c33 + c35;
  Real c37 = Power(c36, 2);
  Real c38 = c16 + c27 + c37;
  Real c2 = Power(c1, -2);
  Real c5 = Power(c1, 3);
  Real c44 = Power(c38, -2.5);
  Real c39 = Power(c38, -1.5);
  Real c40 = -(c39 * c5);
  Real c42 = 1 / Sqrt(c38);
  Real c43 = 3 * c1 * c42;
  Real c77 = -t;
  Real c78 = 1 + c77;
  Real c66 = -2 + c40 + c43;
  Real c54 = 3 * c26 * c3 * c44 * c5;
  Real c55 = -3 * c1 * c26 * c3 * c39;
  Real c56 = c54 + c55;
  Real c4 = Power(c3, 2);
  Real c58 = 3 * c3 * c36 * c44 * c5;
  Real c59 = -3 * c1 * c3 * c36 * c39;
  Real c60 = c58 + c59;
  Real c62 = -3 * s * c15 * c44 * c5;
  Real c63 = 3 * s * c1 * c15 * c39;
  Real c64 = c62 + c63;
  Real c69 = -3 * s * c26 * c44 * c5;
  Real c70 = 3 * s * c1 * c26 * c39;
  Real c71 = c69 + c70;
  Real c67 = -(s * c2 * c3 * c66) / 2.;
  Real c73 = -3 * s * c36 * c44 * c5;
  Real c74 = 3 * s * c1 * c36 * c39;
  Real c75 = c73 + c74;
  Real c79 = 3 * c15 * c44 * c5 * c78;
  Real c80 = -3 * c1 * c15 * c39 * c78;
  Real c81 = c79 + c80;
  Real c85 = 3 * c26 * c44 * c5 * c78;
  Real c86 = -3 * c1 * c26 * c39 * c78;
  Real c87 = c85 + c86;
  Real c83 = (c2 * c3 * c66 * c78) / 2.;
  Real c89 = 3 * c36 * c44 * c5 * c78;
  Real c90 = -3 * c1 * c36 * c39 * c78;
  Real c91 = c89 + c90;
  Real c93 = 3 * t * c15 * c44 * c5;
  Real c94 = -3 * t * c1 * c15 * c39;
  Real c95 = c93 + c94;
  Real c99 = 3 * t * c26 * c44 * c5;
  Real c100 = -3 * t * c1 * c26 * c39;
  Real c101 = c100 + c99;
  Real c97 = (t * c2 * c3 * c66) / 2.;
  Real c103 = 3 * t * c36 * c44 * c5;
  Real c104 = -3 * t * c1 * c36 * c39;
  Real c105 = c103 + c104;
  Real c107 = 3 * c15 * c3 * c44 * c5;
  Real c108 = -3 * c1 * c15 * c3 * c39;
  Real c109 = c107 + c108;
  Real c112 = (c2 * c4 * c66) / 2.;
  Real c147 = c39 * c5;
  Real c148 = -3 * c1 * c42;
  Real c149 = 2 + c147 + c148;
  Real c143 = -3 * c15 * c3 * c44 * c5;
  Real c144 = 3 * c1 * c15 * c3 * c39;
  Real c145 = c143 + c144;
  Real c152 = -3 * c26 * c3 * c44 * c5;
  Real c153 = 3 * c1 * c26 * c3 * c39;
  Real c154 = c152 + c153;
  Real c150 = (s * c149 * c2 * c3) / 2.;
  Real c156 = -3 * c3 * c36 * c44 * c5;
  Real c157 = 3 * c1 * c3 * c36 * c39;
  Real c158 = c156 + c157;
  Real c160 = 3 * s * c15 * c44 * c5;
  Real c161 = -3 * s * c1 * c15 * c39;
  Real c162 = c160 + c161;
  Real c167 = 3 * s * c26 * c44 * c5;
  Real c168 = -3 * s * c1 * c26 * c39;
  Real c169 = c167 + c168;
  Real c164 = Power(s, 2);
  Real c165 = -(c149 * c164 * c2) / 2.;
  Real c171 = 3 * s * c36 * c44 * c5;
  Real c172 = -3 * s * c1 * c36 * c39;
  Real c173 = c171 + c172;
  Real c175 = -3 * c15 * c44 * c5 * c78;
  Real c176 = 3 * c1 * c15 * c39 * c78;
  Real c177 = c175 + c176;
  Real c181 = -3 * c26 * c44 * c5 * c78;
  Real c182 = 3 * c1 * c26 * c39 * c78;
  Real c183 = c181 + c182;
  Real c179 = (s * c149 * c2 * c78) / 2.;
  Real c185 = -3 * c36 * c44 * c5 * c78;
  Real c186 = 3 * c1 * c36 * c39 * c78;
  Real c187 = c185 + c186;
  Real c189 = -3 * t * c15 * c44 * c5;
  Real c190 = 3 * t * c1 * c15 * c39;
  Real c191 = c189 + c190;
  Real c195 = -3 * t * c26 * c44 * c5;
  Real c196 = 3 * t * c1 * c26 * c39;
  Real c197 = c195 + c196;
  Real c193 = (s * t * c149 * c2) / 2.;
  Real c199 = -3 * t * c36 * c44 * c5;
  Real c200 = 3 * t * c1 * c36 * c39;
  Real c201 = c199 + c200;
  Real c240 = -(s * c2 * c66 * c78) / 2.;
  Real c245 = Power(c78, 2);
  Real c246 = (c2 * c245 * c66) / 2.;
  Real c251 = (t * c2 * c66 * c78) / 2.;
  Real c292 = -(s * t * c2 * c66) / 2.;
  Real c301 = Power(t, 2);
  Real c302 = (c2 * c301 * c66) / 2.;
  out(0, 0) +=
      (c2 * c4 *
       (-2 + c40 + c43 -
        3 * c1 * c16 *
            (-Power(rs, 2) - 2 * rs * rt - Power(rt, 2) + c16 + c27 + c37) *
            c44)) /
      2.;
  out(0, 1) += (c15 * c2 * c3 * c56) / 2.;
  out(0, 2) += (c15 * c2 * c3 * c60) / 2.;
  out(0, 3) += (c15 * c2 * c3 * c64) / 2. + c67;
  out(0, 4) += (c15 * c2 * c3 * c71) / 2.;
  out(0, 5) += (c15 * c2 * c3 * c75) / 2.;
  out(0, 6) += (c15 * c2 * c3 * c81) / 2. + c83;
  out(0, 7) += (c15 * c2 * c3 * c87) / 2.;
  out(0, 8) += (c15 * c2 * c3 * c91) / 2.;
  out(0, 9) += (c15 * c2 * c3 * c95) / 2. + c97;
  out(0, 10) += (c101 * c15 * c2 * c3) / 2.;
  out(0, 11) += (c105 * c15 * c2 * c3) / 2.;
  out(1, 0) += (c109 * c2 * c26 * c3) / 2.;
  out(1, 1) += c112 + (c2 * c26 * c3 * c56) / 2.;
  out(1, 2) += (c2 * c26 * c3 * c60) / 2.;
  out(1, 3) += (c2 * c26 * c3 * c64) / 2.;
  out(1, 4) += c67 + (c2 * c26 * c3 * c71) / 2.;
  out(1, 5) += (c2 * c26 * c3 * c75) / 2.;
  out(1, 6) += (c2 * c26 * c3 * c81) / 2.;
  out(1, 7) += c83 + (c2 * c26 * c3 * c87) / 2.;
  out(1, 8) += (c2 * c26 * c3 * c91) / 2.;
  out(1, 9) += (c2 * c26 * c3 * c95) / 2.;
  out(1, 10) += (c101 * c2 * c26 * c3) / 2. + c97;
  out(1, 11) += (c105 * c2 * c26 * c3) / 2.;
  out(2, 0) += (c109 * c2 * c3 * c36) / 2.;
  out(2, 1) += (c2 * c3 * c36 * c56) / 2.;
  out(2, 2) += c112 + (c2 * c3 * c36 * c60) / 2.;
  out(2, 3) += (c2 * c3 * c36 * c64) / 2.;
  out(2, 4) += (c2 * c3 * c36 * c71) / 2.;
  out(2, 5) += c67 + (c2 * c3 * c36 * c75) / 2.;
  out(2, 6) += (c2 * c3 * c36 * c81) / 2.;
  out(2, 7) += (c2 * c3 * c36 * c87) / 2.;
  out(2, 8) += c83 + (c2 * c3 * c36 * c91) / 2.;
  out(2, 9) += (c2 * c3 * c36 * c95) / 2.;
  out(2, 10) += (c101 * c2 * c3 * c36) / 2.;
  out(2, 11) += (c105 * c2 * c3 * c36) / 2. + c97;
  out(3, 0) += c150 + (s * c145 * c15 * c2) / 2.;
  out(3, 1) += (s * c15 * c154 * c2) / 2.;
  out(3, 2) += (s * c15 * c158 * c2) / 2.;
  out(3, 3) += c165 + (s * c15 * c162 * c2) / 2.;
  out(3, 4) += (s * c15 * c169 * c2) / 2.;
  out(3, 5) += (s * c15 * c173 * c2) / 2.;
  out(3, 6) += c179 + (s * c15 * c177 * c2) / 2.;
  out(3, 7) += (s * c15 * c183 * c2) / 2.;
  out(3, 8) += (s * c15 * c187 * c2) / 2.;
  out(3, 9) += c193 + (s * c15 * c191 * c2) / 2.;
  out(3, 10) += (s * c15 * c197 * c2) / 2.;
  out(3, 11) += (s * c15 * c2 * c201) / 2.;
  out(4, 0) += (s * c145 * c2 * c26) / 2.;
  out(4, 1) += c150 + (s * c154 * c2 * c26) / 2.;
  out(4, 2) += (s * c158 * c2 * c26) / 2.;
  out(4, 3) += (s * c162 * c2 * c26) / 2.;
  out(4, 4) += c165 + (s * c169 * c2 * c26) / 2.;
  out(4, 5) += (s * c173 * c2 * c26) / 2.;
  out(4, 6) += (s * c177 * c2 * c26) / 2.;
  out(4, 7) += c179 + (s * c183 * c2 * c26) / 2.;
  out(4, 8) += (s * c187 * c2 * c26) / 2.;
  out(4, 9) += (s * c191 * c2 * c26) / 2.;
  out(4, 10) += c193 + (s * c197 * c2 * c26) / 2.;
  out(4, 11) += (s * c2 * c201 * c26) / 2.;
  out(5, 0) += (s * c145 * c2 * c36) / 2.;
  out(5, 1) += (s * c154 * c2 * c36) / 2.;
  out(5, 2) += c150 + (s * c158 * c2 * c36) / 2.;
  out(5, 3) += (s * c162 * c2 * c36) / 2.;
  out(5, 4) += (s * c169 * c2 * c36) / 2.;
  out(5, 5) += c165 + (s * c173 * c2 * c36) / 2.;
  out(5, 6) += (s * c177 * c2 * c36) / 2.;
  out(5, 7) += (s * c183 * c2 * c36) / 2.;
  out(5, 8) += c179 + (s * c187 * c2 * c36) / 2.;
  out(5, 9) += (s * c191 * c2 * c36) / 2.;
  out(5, 10) += (s * c197 * c2 * c36) / 2.;
  out(5, 11) += c193 + (s * c2 * c201 * c36) / 2.;
  out(6, 0) += (c109 * c15 * c2 * c78) / 2. + c83;
  out(6, 1) += (c15 * c2 * c56 * c78) / 2.;
  out(6, 2) += (c15 * c2 * c60 * c78) / 2.;
  out(6, 3) += c240 + (c15 * c2 * c64 * c78) / 2.;
  out(6, 4) += (c15 * c2 * c71 * c78) / 2.;
  out(6, 5) += (c15 * c2 * c75 * c78) / 2.;
  out(6, 6) += c246 + (c15 * c2 * c78 * c81) / 2.;
  out(6, 7) += (c15 * c2 * c78 * c87) / 2.;
  out(6, 8) += (c15 * c2 * c78 * c91) / 2.;
  out(6, 9) += c251 + (c15 * c2 * c78 * c95) / 2.;
  out(6, 10) += (c101 * c15 * c2 * c78) / 2.;
  out(6, 11) += (c105 * c15 * c2 * c78) / 2.;
  out(7, 0) += (c109 * c2 * c26 * c78) / 2.;
  out(7, 1) += (c2 * c26 * c56 * c78) / 2. + c83;
  out(7, 2) += (c2 * c26 * c60 * c78) / 2.;
  out(7, 3) += (c2 * c26 * c64 * c78) / 2.;
  out(7, 4) += c240 + (c2 * c26 * c71 * c78) / 2.;
  out(7, 5) += (c2 * c26 * c75 * c78) / 2.;
  out(7, 6) += (c2 * c26 * c78 * c81) / 2.;
  out(7, 7) += c246 + (c2 * c26 * c78 * c87) / 2.;
  out(7, 8) += (c2 * c26 * c78 * c91) / 2.;
  out(7, 9) += (c2 * c26 * c78 * c95) / 2.;
  out(7, 10) += c251 + (c101 * c2 * c26 * c78) / 2.;
  out(7, 11) += (c105 * c2 * c26 * c78) / 2.;
  out(8, 0) += (c109 * c2 * c36 * c78) / 2.;
  out(8, 1) += (c2 * c36 * c56 * c78) / 2.;
  out(8, 2) += (c2 * c36 * c60 * c78) / 2. + c83;
  out(8, 3) += (c2 * c36 * c64 * c78) / 2.;
  out(8, 4) += (c2 * c36 * c71 * c78) / 2.;
  out(8, 5) += c240 + (c2 * c36 * c75 * c78) / 2.;
  out(8, 6) += (c2 * c36 * c78 * c81) / 2.;
  out(8, 7) += (c2 * c36 * c78 * c87) / 2.;
  out(8, 8) += c246 + (c2 * c36 * c78 * c91) / 2.;
  out(8, 9) += (c2 * c36 * c78 * c95) / 2.;
  out(8, 10) += (c101 * c2 * c36 * c78) / 2.;
  out(8, 11) += c251 + (c105 * c2 * c36 * c78) / 2.;
  out(9, 0) += (t * c109 * c15 * c2) / 2. + c97;
  out(9, 1) += (t * c15 * c2 * c56) / 2.;
  out(9, 2) += (t * c15 * c2 * c60) / 2.;
  out(9, 3) += c292 + (t * c15 * c2 * c64) / 2.;
  out(9, 4) += (t * c15 * c2 * c71) / 2.;
  out(9, 5) += (t * c15 * c2 * c75) / 2.;
  out(9, 6) += c251 + (t * c15 * c2 * c81) / 2.;
  out(9, 7) += (t * c15 * c2 * c87) / 2.;
  out(9, 8) += (t * c15 * c2 * c91) / 2.;
  out(9, 9) += c302 + (t * c15 * c2 * c95) / 2.;
  out(9, 10) += (t * c101 * c15 * c2) / 2.;
  out(9, 11) += (t * c105 * c15 * c2) / 2.;
  out(10, 0) += (t * c109 * c2 * c26) / 2.;
  out(10, 1) += (t * c2 * c26 * c56) / 2. + c97;
  out(10, 2) += (t * c2 * c26 * c60) / 2.;
  out(10, 3) += (t * c2 * c26 * c64) / 2.;
  out(10, 4) += c292 + (t * c2 * c26 * c71) / 2.;
  out(10, 5) += (t * c2 * c26 * c75) / 2.;
  out(10, 6) += (t * c2 * c26 * c81) / 2.;
  out(10, 7) += c251 + (t * c2 * c26 * c87) / 2.;
  out(10, 8) += (t * c2 * c26 * c91) / 2.;
  out(10, 9) += (t * c2 * c26 * c95) / 2.;
  out(10, 10) += (t * c101 * c2 * c26) / 2. + c302;
  out(10, 11) += (t * c105 * c2 * c26) / 2.;
  out(11, 0) += (t * c109 * c2 * c36) / 2.;
  out(11, 1) += (t * c2 * c36 * c56) / 2.;
  out(11, 2) += (t * c2 * c36 * c60) / 2. + c97;
  out(11, 3) += (t * c2 * c36 * c64) / 2.;
  out(11, 4) += (t * c2 * c36 * c71) / 2.;
  out(11, 5) += c292 + (t * c2 * c36 * c75) / 2.;
  out(11, 6) += (t * c2 * c36 * c81) / 2.;
  out(11, 7) += (t * c2 * c36 * c87) / 2.;
  out(11, 8) += c251 + (t * c2 * c36 * c91) / 2.;
  out(11, 9) += (t * c2 * c36 * c95) / 2.;
  out(11, 10) += (t * c101 * c2 * c36) / 2.;
  out(11, 11) += c302 + (t * c105 * c2 * c36) / 2.;
  // ---------------------------- //
}
#endif
