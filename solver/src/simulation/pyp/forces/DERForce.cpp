#include "DERForce.h"
#include "../../ThreadUtils.h"
#include "../../debug/debug_includes.h"

DERForce::DERForce(std::shared_ptr<GYP> gyp) : GYPForce(gyp) {}
DERForce::~DERForce() {}

void DERForce::precompute(bool energy, bool gradient, bool hessian) {
  const auto &lwcsfs = s_gyp->getLWCYarns();
  if (energy)
    threadutils::parallel_for_each(
        lwcsfs, [&](const auto &sf) { sf->precomputeEnergy(); });
  if (gradient)
    threadutils::parallel_for_each(
        lwcsfs, [&](const auto &sf) { sf->precomputeForces(); });
  if (hessian)
    threadutils::parallel_for_each(
        lwcsfs, [&](const auto &sf) { sf->precomputeHessians(); });
}
void DERForce::addE(scalar &E) {
  const auto &lwcsfs = s_gyp->getLWCYarns();
  for (auto &sf : lwcsfs) E += sf->m_strandEnergyUpdate;
}
void DERForce::addGradE(VectorXs &gradE) {
  const auto &lwcsfs = s_gyp->getLWCYarns();

  for (auto &sf : lwcsfs) {
    const int nverts_local = sf->m_verts.size();
    threadutils::parallel_for(0, nverts_local, [&](int i) {
      if (i != nverts_local - 1)
        gradE.segment<4>(4 * sf->m_verts[i]) -=
            sf->m_strandForceUpdate.segment<4>(i * 4);
      else
        gradE.segment<3>(4 * sf->m_verts[i]) -=
            sf->m_strandForceUpdate.segment<3>(i * 4);
    });
  }
}

void DERForce::addHessE(std::vector<Triplet> &hessE, int forceTripletOffset) {
  const auto &lwcsfs = s_gyp->getLWCYarns();
  // precompute local triplet offset per yarn for this force
  std::vector<int> localTripletOffsets(lwcsfs.size());
  int num_hess = 0;
  for (int i = 0; i < (int)lwcsfs.size(); ++i) {
    localTripletOffsets[i] = num_hess;
    num_hess += lwcsfs[i]->numHessX() + lwcsfs[i]->numAngularHessX();
  }

  // add triplets
  threadutils::parallel_for(0, (int)lwcsfs.size(), [&](int j) {
    auto sf     = lwcsfs[j];
    int triplet = forceTripletOffset + localTripletOffsets[j];

    // need to combine lwc hessian and angular hessian
    // because for some reason they have been separated
    // although they are precomputed at the same time...

    // vertex position / linear hessian
    const int num_hess = sf->numHessX();
    threadutils::parallel_for(0, num_hess, [&](int i) {
      const Triplets &data = sf->m_strandHessianUpdate[i];
      int col_vert         = data.col() / 4;
      int col_r            = data.col() - col_vert * 4;
      int row_vert         = data.row() / 4;
      int row_r            = data.row() - row_vert * 4;

      hessE[triplet + i] =
          Triplets(4 * sf->m_verts[row_vert] + row_r,
                   4 * sf->m_verts[col_vert] + col_r, -data.value());
    });

    // edge twist / angular hessian
    const int num_angular_hess = sf->numAngularHessX();
    // for(int i = 0; i < num_angular_hess; ++i) {
    threadutils::parallel_for(0, num_angular_hess, [&](int i) {
      const Triplets &data = sf->m_strandAngularHessianUpdate[i];
      int col_vert         = data.col() / 4;
      int row_vert         = data.row() / 4;

      // dont want to write any triplet for the nonexisting twists of tips!
      assert(row_vert < (int)sf->m_verts.size() - 1 &&
             col_vert < (int)sf->m_verts.size() - 1);

      hessE[triplet + num_hess + i] =
          Triplets(4 * sf->m_verts[row_vert] + 3, 4 * sf->m_verts[col_vert] + 3,
                   -data.value());
    });
  });
}

int DERForce::numHess() {
  int num_hess = 0;
  for (auto &sf : s_gyp->getLWCYarns()) {
    num_hess += sf->numHessX() + sf->numAngularHessX();
  }
  return num_hess;
}

void DERForce::postcompute() {
  for (auto &sf : s_gyp->getLWCYarns()) {
    // sf->m_strandForceUpdate.resize
    sf->m_strandHessianUpdate.clear();
    sf->m_strandAngularHessianUpdate.clear();
  }
}