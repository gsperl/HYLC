#ifndef _LWCINTERFACE_H_
#define _LWCINTERFACE_H_

#include "../../../EigenDefinitions.h"

class LwcInterface {
 public:
  // virtual destructor not necessary as this class will only ever be deleted as
  // a subclass
  virtual const VectorXs& getQ() const = 0;
  virtual VectorXs& getQ()             = 0;
  virtual int getDof(int vtx) const { return vtx * 4; }
  // compute fractional periodic weight for forces
  virtual scalar computeWeight(const std::vector<int>& ixs) const = 0;
};

#endif /* end of include guard: _LWCINTERFACE_H_ */
