#ifndef __MATERIAL__H__
#define __MATERIAL__H__

struct Material {
  double yE = 1e5, G = 4e4, gamma = 0.1;
  double kc      = 1.2e1;    // collision force constant,  units: kg/s^2
  double density = 1.2e2;  // kg/m^3
};

#endif  // __MATERIAL__H__