#include "PeriodicYarnPattern.h"
#include <functional>
#include "../AlgorithmUtils.h"
#include "../ThreadUtils.h"
#include "../debug/debug_includes.h"

// std::vector<int> find_vertices_where(const PYP& pyp,
//                                      std::function<bool(int)> func) {
//   std::vector<int> where;
//   for (int vix = 0; (vix < pyp.V_xyzt.rows(); vix++) {
//     if (func(vix))
//       where.push_back(vix);
//   }
//   return where;
// }

bool PYP::verify() {
  // TODO other verification

  // verify edges
  for (int eix = 0; eix < (int)E.rows(); eix++) {
    auto e   = E.row(eix);  // [v0 v1 dx dy]
    int vix0 = e(0);
    int vix1 = e(1);
    int dx = e(2), dy = e(3);
    // no edge should connect a vertex to itself
    if (vix0 == vix1) {
      Debug::error("vertex edge to self");
      return false;
    }
    // no edge should have a periodic jump across more than one boundary
    if (std::abs(dx) + std::abs(dy) > 1) {
      Debug::error("periodic jump across more than 1 boundary");
      return false;
    }
  }

  return true;
}

const MatrixXXi& PYP::getVertexEdgeTable(bool force_recompute) {
  if (force_recompute || VE.rows() != Q.rows()) {
    recomputeVertexEdgeTable();
  }
  return VE;
}

void PYP::recomputeVertexEdgeTable() {
  VE = MatrixXXi::Constant(Q.rows(), 2, -1);

  for (int eix = 0; eix < (int)E.rows(); eix++) {
    auto e   = E.row(eix);  // [v0 v1 dx dy]
    int vix0 = e(0);
    int vix1 = e(1);
    VE(vix0, 1) = eix;  // set as next edge of v0
    VE(vix1, 0) = eix;  // set as prev edge of v1
  }
}

// ensure all non-periodic vertices are within [minx,minx+px) [miny,miny+py)
void PYP::rectangulize() {
  Debug::error("rectangulize not implemented");

  // NOTE: currently implemented in python in the db2pyp scripts
}

VectorXs PYP::computeEdgeLengths() {
  // reference to own VE member but with lazy recompute
  const auto& vet = getVertexEdgeTable();
  VectorXs restLengths(Q.rows());
  // restLengths.resize(Q.rows());
  for (int i = 0; i < Q.rows(); i++) {
    Vector3s v0    = Q.block<1, 3>(i, 0);
    auto edge      = E.row(vet(i, 1));
    Vector3s shift = Vector3s::Zero();
    shift << edge(2) * px, edge(3) * py, 0;  // account for periodic jump
    Vector3s v1    = Q.block<1, 3>(edge(1), 0);
    scalar l       = (v1 + shift - v0).norm();
    restLengths(i) = l; // NOTE stored per vertex' outgoing edge!
  }
  return restLengths;
}

VectorXs& PYP::getRestLengths(bool force_recompute) {
  if (force_recompute || restLengths.rows() != Q.rows())
    restLengths = computeEdgeLengths();
  return restLengths;
}

MatrixXXs& PYP::getRefD1s(bool force_recompute) {
  if (force_recompute || refd1.rows() != Q.rows())
    recomputeReferenceDirectors();
  return refd1;
}

// make v orthogonal to t, t is assumed to be normalized
void orthogonalize(Vector3s& v, const Vector3s& t) {
  assert(std::abs(t.squaredNorm() - 1) < 1e-15);
  v = (v - t * v.dot(t));
}

void orthonormalize(Vector3s& v, const Vector3s& t) {
  orthogonalize(v, t);
  v.normalize();
}

Vector3s getMostOrthoXY(const Vector3s& v) {
  // assume v is normalized
  Vector3s ortho;
  if (std::abs(v(0)) < std::abs(v(1)))       // v is more along y
    ortho << 1 * (v(1) > 0 ? -1 : 1), 0, 0;  // pick x (rotated sign)
  else
    ortho << 0, -1 * 1 * (v(0) > 0 ? 1 : -1), 0;
  // NOTE  signs chosen arbitrarily, but consistent

  // make ortho orthogonal to v and unit length
  orthonormalize(ortho, v);
  return ortho;
}

// parallel transport v from t0 to t1
// assuming v orthogonal to t0, t0 and t1 normalized
Vector3s _parallelTransport(const Vector3s& v, const Vector3s& t0,
                            const Vector3s& t1) {
  // This should be called only to transport an orthogonal vector
  assert(std::abs(v.dot(t0)) < 1e-15);
  assert(std::abs(t0.squaredNorm() - 1) < 1e-15);
  assert(std::abs(t1.squaredNorm() - 1) < 1e-15);

  Vector3s b         = t0.cross(t1);
  const scalar bNorm = b.norm();
  if (bNorm < 1e-15) {  // t0,t1 nearly collinear
    Vector3s vt = v;
    orthonormalize(vt, t1);
    return vt;
  }
  b /= bNorm;

  Vector3s n0 = t0.cross(b);
  Vector3s n1 = t1.cross(b);

  return v.dot(n0) * n1 + v.dot(b) * b;
  // NOTE not accounting for proj. length along tangent (v.dot( t0.normalized()
  // ) * t1.normalized()) since assumed orthogonal
}

// compute signed angle from v0 to v1 across e
// e is assumed normalized and orthogonal to both v0 and v1
scalar _signedAngle(const Vector3s& v0, const Vector3s& v1, const Vector3s& e) {
  assert(std::abs(e.squaredNorm() - 1) < 1e-15);
  assert(std::abs(e.dot(v0)) < 1e-15);
  assert(std::abs(e.dot(v1)) < 1e-15);
  // e.g. angle between v0(1,0,0) and v1(0,1,0) across e(0,0,1) is 90deg
  //      with v1(0,-1,0) it would be -90deg
  return atan2((v0.cross(v1)).dot(e), v0.dot(v1));
}

// Rodrigues rotation of v around unit k by angle theta
Vector3s rotate(const Vector3s& v, const Vector3s& k, scalar theta) {
  assert(std::abs(k.squaredNorm() - 1) < 1e-15);
  scalar c = std::cos(theta);
  scalar s = std::sin(theta);
  return c * v + s * k.cross(v) + k.dot(v) * (1.0 - c) * k;
}

void PYP::recomputeReferenceDirectors() {
  // since lwc's default parallel transport init of referenceFrames1
  // does not know about periodicity, this would lead to an angular defect
  // across the boundaries
  // instead here we initialize these directors by periodic parallel transport

  recomputeVertexEdgeTable();

  // precompute tangents
  MatrixXXs tangents(Q.rows(), 3);
  for (int i = 0; i < Q.rows(); i++) {
    Vector3s v0 = Q.block<1, 3>(i, 0);
    auto edge   = E.row(VE(i, 1));  // next edge
    int vnext   = edge(1);          // next vertex
    Vector3s shift;
    shift << edge(2) * px, edge(3) * py, 0;  // account for periodic boundary
    Vector3s v1     = Q.block<1, 3>(vnext, 0).transpose() + shift;
    tangents.row(i) = (v1 - v0).normalized();
  }

  refd1.resize(Q.rows(), 3);
  std::vector<std::vector<int>> pyarns = computePeriodicallyConnectedYarns();

  for (const auto& pyarn : pyarns) {
    // init frame on first edge from most orthogonal x or y
    Vector3s t_0        = tangents.row(pyarn[0]);
    Vector3s rd1_0      = getMostOrthoXY(t_0);
    refd1.row(pyarn[0]) = rd1_0;

    Vector3s rd1_cur = rd1_0;
    for (size_t i = 1; i < pyarn.size(); i++) {
      // transport refframe from prev tangent to current tangent
      rd1_cur = _parallelTransport(rd1_cur, tangents.row(pyarn[i - 1]),
                                   tangents.row(pyarn[i]));
      orthonormalize(rd1_cur, tangents.row(pyarn[i]));
      refd1.row(pyarn[i]) = rd1_cur;
    }

    // final periodic transport from last edge to first edge
    rd1_cur = _parallelTransport(rd1_cur, tangents.row(pyarn.back()), t_0);
    orthonormalize(rd1_cur, t_0);
    // angle from mismatching final transport to the first frame, i.e. to what
    // it should be
    scalar defect = _signedAngle(rd1_cur, rd1_0, t_0);

    // distribute defect, by rotating each frame around its tangent
    // by an angle of defect * i/n (s.t. i=0 stays the same, and i=n would
    // cancel the defect at its periodic copy)
    scalar ninv = 1.0 / (int)pyarn.size();
    for (int i = 1; i < (int)pyarn.size(); i++) {
      refd1.row(pyarn[i]) = rotate(refd1.row(pyarn[i]), tangents.row(pyarn[i]),
                                   defect * i * ninv);
    }

    // NOTE here, one could try specifying some target defect
    // e.g. that some yarn twists by exactly some amount when crossing the
    // periodic boundary. but this would have to be included also in the
    // ghost yarn copies. currently, this is not implemented and we default
    // to a target defect of 0.

    // NOTE currently not explicitly handling twists larger than +/- pi due to
    // signed angle computation, but rather defaulting to implicit twist
    // of k 2 pi and then only distributing the remainder defect
  }
}

std::vector<std::vector<int>> PYP::computePeriodicallyConnectedYarns() {
  // compute periodically connected yarns as lists of vertices

  // start where prevedge is periodic assert exists
  // follow until self vix // assert num < total num + 1
  // actually maybe computeperiodicyarns method of pyp
  std::vector<std::vector<int>> pyl;
  const auto& vet = getVertexEdgeTable();

  auto starts = find_where(
      [&](int i) {
        int prevedge = vet(i, 0);
        assert(prevedge >= 0 && "nonperiodic PYP!");
        if (E.row(prevedge).tail<2>().cwiseAbs().sum() > 0)  // dxdy nonzero
          return true;
        return false;
      },
      0, Q.rows());
  pyl.reserve(starts.size());

  // rough estimate per yarn
  int n_reserve_estimate = Q.rows() / (int)starts.size();

  // follow periodically until (excluding) same vertex
  std::vector<bool> visited(Q.rows(), false);
  for (size_t i = 0; i < starts.size(); i++) {
    int start = starts[i];
    if (visited[start])
      continue;
    pyl.push_back({});
    auto& yarn = pyl.back();
    yarn.reserve(n_reserve_estimate);
    int vix = start;
    int k   = 0;
    while (true) {
      if (k > Q.rows()) {  // catch loops
        assert(false &&
               "potential loop in yarn list, yarn has more vertices than "
               "pattern");
        Debug::error("potential loop in yarn list aborted!");
        break;
      }
      yarn.push_back(vix);
      visited[vix] = true;
      int next_eix = vet(vix, 1);
      assert(next_eix >= 0 && "nonperiodic PYP!");
      vix = E(next_eix, 1);
      if (vix == start)
        break;
      assert(!visited[vix]);  // only start should have been visited!
    }
  }

  // assert each edge visited once
  // otherwise this might break e.g. translation / twist nullspace constraints
  assert([&]() {
    for (size_t i = 0; i < visited.size(); i++)
      if (!visited[i])
        return false;  // some edge was not visited, ie pyp contains unlinked
                       // edges
    return true;
  }());

  return pyl;
}

std::vector<int> PYP::getBoundaryEdges() {
  std::vector<int> prd_edges;
  // reserve space under assumption that num edges in total and on boundary
  // scale by n^3 and n^2 respectively.
  prd_edges.reserve((int)(std::pow(E.rows(), 2.0 / 3.0)));
  for (int i = 0; i < E.rows(); i++) {
    if (E(i, 2) != 0 || E(i, 3) != 0)
      prd_edges.push_back(i);
  }
  return prd_edges;
}

scalar PYP::computeYarnVolume() {
  // sum up cylinder volumes
  scalar V        = 0;
  scalar cyl_base = r * r * M_PI;
  for (int i = 0; i < E.rows(); i++) {
    const auto edge = E.row(i);
    Vector3s v      = Vector3s(Q.block<1, 3>(edge(0), 0));
    Vector3s vnext  = Q.block<1, 3>(edge(1), 0);
    Vector3s shift;
    shift << edge(2) * px, edge(3) * py, 0;
    scalar l = (vnext + shift - v).norm();
    V += cyl_base * l;
  }
  return V;
}
