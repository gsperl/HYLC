#ifndef _AABBTREEBROADPHASE__
#define _AABBTREEBROADPHASE__

#include <vector>
#include "../../EigenDefinitions.h"
#include "../../ThreadUtils.h"
#include "../../debug/debug_includes.h"
#include "YarnBroadphase.h"

struct AABB {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;  // storing fixed size Eigen matrix
  Vector3s min, max;

  void fatten(scalar d) {
    for (int i = 0; i < 3; ++i) {
      min(i) -= d;
      max(i) += d;
    }
  }

  AABB combine(const AABB &other) const {
    AABB combined;  // copy
    for (int i = 0; i < 3; ++i) {
      combined.min(i) = std::min(min(i), other.min(i));
      combined.max(i) = std::max(max(i), other.max(i));
    }
    return combined;
  }

  scalar vol() const {
    return (max(0) - min(0)) * (max(1) - min(1)) * (max(2) - min(2));
  }

  bool overlaps(const AABB &other) {
    for (int i = 0; i < 3; ++i) {
      if (!(min(i) < other.max(i) && other.min(i) < max(i)))
        return false;
    }
    return true;
  }

  static AABB fromPoints(Vector3s a, Vector3s b) {
    AABB combined;  // copy
    for (int i = 0; i < 3; ++i) {
      combined.min(i) = std::min(a(i), b(i));
      combined.max(i) = std::max(a(i), b(i));
    }
    return combined;
  }
};

struct Node {
  AABB aabb;
  bool periodic;  // aggregate extra data to preemptively remove periodic
  // collisions, periodic flag is true if all children are ghosts
  Node *parent = nullptr;
  Node *left   = nullptr;
  Node *right  = nullptr;
  bool leaf    = false;
  int edgeix;  // reference to actual yarn segment
  // for intermediate nodes this actually is the rightmost reachable edgeix
  // since we inserted edges into the tree in a sorted way this should
  // still be nicely ordered the same way as the leaves
  // this value is also max-aggregated to avoid doubly counting collisions
  // in the tree's self-intersection

  // void setLeaf(Node* parent, const AABB &aabb, bool periodic, int edgeix) {
  //   leaf             = true;
  //   this->parent     = parent;
  //   this->aabb       = aabb;
  //   this->periodic   = periodic;
  //   this->edgeix     = edgeix;
  // }

  void setBranch(Node *leftix, Node *rightix) {
    leaf        = false;
    this->left  = leftix;
    this->right = rightix;
    assert(this->left && this->right);
    // NOTE aabb and periodic remain to be recomputed
  }

  void updateFromChildren() {
    assert(this->left && this->right);
    aabb     = this->left->aabb.combine(this->right->aabb);
    periodic = left->periodic && right->periodic; // subtree only periodic
    edgeix   = std::max(left->edgeix, right->edgeix);
  }

  void updateAABBFromChildren() {
    assert(this->left && this->right);
    aabb = this->left->aabb.combine(this->right->aabb);
  }

  bool isRoot() { return parent < 0; }
};

class AABBTree : public YarnBroadphase {
 public:
  AABBTree(std::shared_ptr<GYP> gyp, int n_leaves, int selfcollisiontolerance,
           scalar max_quadrature_step, int min_quadrature_steps)
      : YarnBroadphase(gyp, selfcollisiontolerance, max_quadrature_step,
                       min_quadrature_steps) {
    // reserve N + ceil N/2 + ceil N/4 etc !
    // important to keep adresses of node vector correct!
    int N       = n_leaves;
    int n_nodes = 1;
    while (N > 1) {
      n_nodes += N;
      N = int(0.5 * N + 0.6);
    }
    // scalar pad_percent = 0.2;
    m_nodes.reserve(n_nodes);

    dbg_initial_capacity = m_nodes.capacity();
  }

  void insertLeaf(AABB aabb, int edgeix, bool periodic) {
    Node newnode{aabb, periodic, nullptr, nullptr, nullptr, true, edgeix};
    if (m_nodes.size() == 0) {
      m_nodes.push_back(newnode);
    } else {
      m_nodes.push_back(newnode);
      Node &newnoderef = m_nodes.back();
      insert(root(), newnoderef);
    }
  }

  // compute levels and leaves array for efficient parallel traversal
  void finalize() {
    finalizeSubTree(root(), 0);
    // Debug::log("nodes / reserved", m_nodes.size(), dbg_initial_capacity);
    // Debug::log("leaves", m_leaves.size());
    // Debug::log("levels", m_levels.size());
    assert((int)m_nodes.size() <= dbg_initial_capacity &&
           "did not allocate enough space, node pointers corrupted!");
    if ((int)m_nodes.size() > dbg_initial_capacity) {
      Debug::error("Did not allocate enough space, node pointers corrupted!!");
    }
  }

  int getEdgeVertexNeighbor(int v0) {
    auto ylcoord   = s_gyp->getYLCoord(v0);
    const auto &yl = s_gyp->getYL();
    const auto y   = yl[ylcoord(0)];
    assert(yl[ylcoord(0)][ylcoord(1)] == v0);
    if ((int)y.size() <= ylcoord(1) + 1)
      return -1;  // tip of yarn
    else
      return y[ylcoord(1) + 1];
  }

  void computeLeafAABBs() {
    // par for all leaves set new fat AABB
    threadutils::parallel_for(0, (int)m_leaves.size(), [&](int nodeix) {
      Node *leaf = m_leaves[nodeix];

      int v0 = leaf->edgeix;
      int v1 = getEdgeVertexNeighbor(v0);
      assert(v1 >= 0 &&
             "aabb node with edgeix doesn't have a following neighbor");
      auto x0 = s_gyp->getVertex<3>(v0);
      auto x1 = s_gyp->getVertex<3>(v1);
      leaf->aabb  = AABB::fromPoints(x0, x1);
      leaf->aabb.fatten(s_gyp->getPYP()->r);
    });
  }

  void parallelAABBupdate(bool parallel = true) {
    // per level from lowest to highest
    // compute merged AABBs
    for (int level = (int)m_levels.size() - 2; level >= 0; level--) {
      int n_nodes = m_levels[level].size();
      auto task   = [&](int nodeix) {
        Node *node = m_levels[level][nodeix];
        if (!node->leaf)
          node->updateAABBFromChildren();
      };
      if (n_nodes > 10 && parallel)  // TODO actually test when threading helps
        threadutils::parallel_for(0, n_nodes, task);
      else
        for (int i = 0; i < n_nodes; ++i) task(i);
    }
  }

  virtual void estimateCollisionPairs() {
    splineCollisions.clear();
    // for each seg, col against each other seg
    std::vector<std::vector<int>> coll_lists(m_leaves.size());
    threadutils::parallel_for(0, (int)m_leaves.size(), [&](int nodeix) {
      coll_lists[nodeix] = intersect_self(nodeix);
    });

    // NOTE could be parallelized but might not matter or be worse
    // TODO check how slow
    for (size_t i = 0; i < coll_lists.size(); i++) {
      auto &coll_list = coll_lists[i];
      int e0v0        = m_leaves[i]->edgeix;
      int e0v1        = getEdgeVertexNeighbor(e0v0);
      assert(e0v1 >= 0);

      for (size_t j = 0; j < coll_list.size(); j++) {
        int e1v0 = coll_list[j];
        int e1v1 = getEdgeVertexNeighbor(e1v0);
        assert(e1v1 >= 0);

        auto ylcoord_e0v0 = s_gyp->getYLCoord(e0v0);
        auto ylcoord_e1v0 = s_gyp->getYLCoord(e1v0);

        // skip segments on the same yarn that are too close
        if (ylcoord_e0v0(0) == ylcoord_e1v0(0)) {
          if (std::abs(ylcoord_e0v0(1) - ylcoord_e1v0(1)) <=
              m_selfcollisiontolerance)
            continue;
        }
        // SplineCollision tst;
        // tst.e0v0;tst.e0v1; tst.e1v0; tst.e1v1;

        splineCollisions.push_back({e0v0, e0v1, e1v0, e1v1});
      }
    }
  }

 private:
  std::vector<Node> m_nodes;
  std::vector<Node *> m_leaves;
  std::vector<std::vector<Node *>> m_levels;

  int dbg_initial_capacity;

  Node &root() {
    assert(m_nodes.size() > 0);
    return m_nodes[0];
  }

  void insert(Node &subroot, Node &newnoderef) {
    if (subroot.leaf) {
      // split leaf and add newnode as child
      m_nodes.push_back(subroot);                 // copy
      Node &copyleaf    = m_nodes.back();         // get ref of copy
      copyleaf.parent   = &subroot;               // link copy
      newnoderef.parent = &subroot;               // link new
      subroot.setBranch(&copyleaf, &newnoderef);  // append
    } else {
      // check which child to recursively check for inserting into
      const auto &aabb0 = subroot.left->aabb;
      const auto &aabb1 = subroot.right->aabb;

      scalar dV0 = aabb0.combine(newnoderef.aabb).vol() - aabb0.vol();
      scalar dV1 = aabb1.combine(newnoderef.aabb).vol() - aabb1.vol();

      if (dV0 < dV1)
        insert(*subroot.left, newnoderef);
      else
        insert(*subroot.right, newnoderef);
    }

    // if subroot is not tree root, then recompute aabb etc from children
    if (subroot.parent != nullptr) {
      subroot.updateFromChildren();
    }
  }

  void finalizeSubTree(Node &subroot, int depth) {
    if ((int)m_levels.size() < depth + 1)
      m_levels.resize(depth + 1);
    m_levels[depth].push_back(&subroot);

    if (subroot.leaf) {
      m_leaves.push_back(&subroot);
    } else {  // NOTE leaves will be sorted left to right
      finalizeSubTree(*subroot.left, depth + 1);
      finalizeSubTree(*subroot.right, depth + 1);
    }
  }

  std::vector<int> intersect_self(int nodeix) {
    // input is index in leaves array
    // output is list of edgeixs
    std::vector<int> intersects;

    // no nodes
    if (m_nodes.size() == 0)
      return intersects;

    // root is leaf
    if (root().leaf) {
      intersects.push_back(root().edgeix);
      return intersects;
    }

    Node *this_node = m_leaves[nodeix];
    std::vector<Node *> stack;
    stack.push_back(&root());
    Node *node;

    // iterative stack check children for leaves
    while (stack.size() > 0) {
      node = stack.back();
      stack.pop_back();
      assert(!node->leaf);

      Node *left  = node->left;
      Node *right = node->right;

      // avoiding duplicates: we only pick collisions where edgeix0 < edgeix1
      // edgeixs are unique
      // and intermediate nodes have the max edgeixs stored
      // so if a node has maxedgeixs < thisedgeix then all in those node
      // can be ignored
      bool validL = left->edgeix > this_node->edgeix;
      bool validR = right->edgeix > this_node->edgeix;

      // ignore ghost on ghost yarn collisions
      validL &= !(left->periodic && this_node->periodic);
      validR &= !(right->periodic && this_node->periodic);

      if (validL) {
        bool overlapL = left->aabb.overlaps(this_node->aabb);
        if (overlapL) {
          if (left->leaf)
            intersects.push_back(left->edgeix);
          else
            stack.push_back(left);
        }
      }
      if (validR) {
        bool overlapR = right->aabb.overlaps(this_node->aabb);
        if (overlapR) {
          if (right->leaf)
            intersects.push_back(right->edgeix);
          else
            stack.push_back(right);
        }
      }
    }
    return intersects;
  }
};

#endif /* end of include guard: _AABBTREEBROADPHASE__ */
