#ifndef _YARNBROADPHASE_H_
#define _YARNBROADPHASE_H_

#include "../GhostYarnPattern.h"

struct SplineCollision {  // Collision between two yarn segments
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;  // storing fixed size Eigen matrix
  int e0v0,e0v1,e1v0,e1v1;
};

class YarnBroadphase {
 public:
  YarnBroadphase(std::shared_ptr<GYP> gyp, int selfcollisiontolerance,
                 scalar max_quadrature_step, int min_quadrature_steps)
      : s_gyp(gyp),
        m_selfcollisiontolerance(selfcollisiontolerance),
        m_max_quadrature_step(max_quadrature_step),
        m_min_quadrature_steps(min_quadrature_steps) {}
  virtual void estimateCollisionPairs() = 0;

  std::vector<SplineCollision> &getCollisions() { return splineCollisions; }

  void recomputeQuadratureSamples();
  const std::vector<int> &getQuadratureSamples() {
    // compute quadrature samples once only, such that we keep a fixed number
    // which is better for gradients
    if ((int)m_quadraturesamples.size() != s_gyp->getNumVerticesTotal())
      recomputeQuadratureSamples();
    return m_quadraturesamples;
  }

 protected:
  std::shared_ptr<GYP> s_gyp;
  std::vector<SplineCollision> splineCollisions;
  int m_selfcollisiontolerance;
  std::vector<int> m_quadraturesamples;
  scalar m_max_quadrature_step;
  int m_min_quadrature_steps;
};

#endif /* end of include guard: _YARNBROADPHASE_H_ */
