#include "YarnBroadphase.h"
#include "../../debug/debug_includes.h"
#include "../../ThreadUtils.h"


  void YarnBroadphase::recomputeQuadratureSamples() {
    Debug::log("Recomputing quadrature samples...");

    // reserve enough room
    m_quadraturesamples.resize(s_gyp->getNumVerticesTotal());

    // compute number of quadrature samples per edge
    const auto &yl = s_gyp->getYL();

    threadutils::parallel_for((size_t) 0, yl.size(), [&](size_t iy) {
      const auto& yarn = yl[iy];

      for (int iseg = 0; iseg < (int)yarn.size() - 1; ++iseg) {
        int vix = yarn[iseg];
        int vix_next = yarn[iseg+1];

        scalar len = (s_gyp->getVertex<3>(vix_next) - s_gyp->getVertex<3>(vix)).norm();

        int nsteps = std::max(int(len / m_max_quadrature_step) + 1,
                              m_min_quadrature_steps);
        // require odd for simpson's rule quadrature
        if (nsteps % 2 == 0)
          nsteps++;

        m_quadraturesamples[vix] = nsteps;
      }
    });
  }