#include "GhostYarnPattern.h"
#include <functional>
#include "../AlgorithmUtils.h"
#include "../ThreadUtils.h"
#include "../debug/debug_includes.h"

// compute a % b similar to python such that -1 % N = N-1
int negmod(int a, int b) {
  // a = r*b + s
  // int r = (int)(a/b);
  // int s = a - r*b; // remainder with |s| < b
  // return (s + b) % b;
  return (a - (int)(a / b) * b + b) % b;
}

// std::vector<int> accumulate_until(int start = 0,
//                             std::function<int(int)> stepfunc,
//                             std::function<bool(int)> stopfunc) {
//   std::vector<int> acc;
//   int& i = start;
//   while (!stopfunc(i)) {
//     acc.push_back(i);
//     i = stepfunc(i);
//   }
// }

MatrixXXi computeVertexEdgeTable(int max_vix, const MatrixXXi& E) {
  MatrixXXi vet = MatrixXXi::Constant(max_vix + 1, 2, -1);
  for (int eix = 0; eix < (int)E.rows(); eix++) {
    auto e   = E.row(eix);  // [v0 v1 ... ]
    int vix0 = e(0);
    int vix1 = e(1);
    if (vix0 < 0 || vix1 < 0)  // skip edge connecting any deleted vertex
      continue;
    vet(vix0, 1) = eix;  // set as next edge of v0
    vet(vix1, 0) = eix;  // set as prev edge of v1
  }
  return vet;
}

// std::vector<std::vector<int>> computeYarnList(int max_vix, const MatrixXXi&
// E) {
//   // compute the full yarn topology (inc ghost segments)
//   std::vector<std::vector<int>> yl;

//   // vet[vix] = [eixprev,eixnext]
//   auto vet = computeVertexEdgeTable(max_vix, E);

//   // find yarn starts as vertices with no previous edge
//   // NOTE: assumes no cyclic yarns
//   std::vector<int> starts;
//   for (int vix = 0; vix < V_xyzt.rows(); vix++) {
//     if (vet(vix, 0) < 0)
//       starts.push_back(vix);
//   }

//   // accumulate yarns
//   yl.resize(starts.size());
//   int n_reserve_estimate = V_xyzt.rows() / (int)starts.size();
//   threadutils::parallel_for(0, (int)starts.size(), [&](int i) {
//     auto& yarn = yl[i];
//     yarn.reserve(n_reserve_estimate);
//     int vix = starts[i];
//     int k   = 0;
//     while (true) {
//       if (k > V_xyzt.rows()) {  // catch loops
//         assert(
//             false &&
//             "potential loop in yarn list, yarn has more vertices than
//             pattern");
//         Debug::error("potential loop in yarn list aborted!");
//         break;
//       }
//       yarn.push_back(vix);
//       int next_eix = vet(vix, 1);
//       if (next_eix < 0)
//         break;
//       else
//         vix = E[next_eix].v1;
//     }
//     // yl.shrink_to_fit();
//   });

//   // for (int i = 0; i < yl.size(); i++) {
//   //   Debug::log("Yarn ", i);
//   //   for (int j = 0; j < yl[i].size(); j++) {
//   //     Debug::log("  ", yl[i][j], V_pid[yl[i][j]]);
//   //   }
//   //   Debug::logf("\n");
//   // }

//   return yl;
// }
// note periodic:
//  from (vix=pid,prev=-1) || (vix=pid,vprevix!=vprevpid), use "isperiodic(vix)"
//  over when vix!=pid use original pid instead
//  to  vix=startingvix // rterror when looping for longer than nverts
// note pstarts & dir:
//  find periodic edges, pick their v1 and its pid
//    pid[e.v1] for e with dx,dy != 0,0; dir given by dxdy

// Proto TEMP_protoFromPYP(PYP& pyp) {
//   // scalar extpx = -1, extpy = -1;
//   scalar extpx = 0.2, extpy = 0.2;

//   // NOTE about periodic fractional weight: assumes that there are primitives
//   // such that weights sum to 1, i.e each original vertex occurs exactly once
//   // and its primitive exists (e.g. x--x--o & o--o--x)

//   // extend by adding periodic vertex for each periodic edge

//   // proto.yarns.clear();
//   // proto.yarns.resize(yl.size());
//   // for (size_t i = 0; i < yl.size(); i++) {
//   //   proto.yarns[i].reserve(yl[i].size());
//   //   for (size_t j = 0; j < yl[i].size(); j++) {
//   //     int vix       = yl[i][j];
//   //     int pid       = P[vix];
//   //     bool periodic = vix != pid;

//   //     Proto::VertexData vd;
//   //     vd.x     = Q(vix, 0);
//   //     vd.y     = Q(vix, 1);
//   //     vd.z     = Q(vix, 2);
//   //     vd.theta = Q(vix, 3);
//   //     vd.id    = (pid + 1) * (periodic ? -1 : 1);  // shift bc 0 bad in
//   proto
//   //     // rl rk1 rk2 rfx rfy rfz rftwist rt are all kept at 0 for now
//   //     proto.yarns[i].push_back(vd);
//   //   }
//   // }
//   // return proto;
// }

// prune m_q and m_p by removing vertices where !keepfunc(i)
// this invalidates m_yl
void prune_vertices(VectorXs& q, VectorXi& p, MatrixXXi& E,
                    std::function<bool(int)> keepfunc) {
  int n = q.rows() / 4;
  std::vector<int> newix(n, -1);
  int k = 0;
  for (int i = 0; i < n; i++) {
    if (keepfunc(i))
      newix[i] = k++;
  }
  VectorXs qnew(k * 4);
  VectorXi pnew(k);
  threadutils::parallel_for(0, n, [&](int i) {
    if (newix[i] >= 0) {
      qnew.segment<4>(newix[i] * 4) = q.segment<4>(i * 4);
      pnew(newix[i])                = p(i);
    }
  });
  q = std::move(qnew);
  p = std::move(pnew);

  // update edges to new indices (sets deleted to -1)
  threadutils::parallel_for(0, (int)E.rows(), [&](int i) {
    E(i, 0) = newix[E(i, 0)];
    E(i, 1) = newix[E(i, 1)];
  });
}

void GYP::initialize(std::shared_ptr<PYP> pyp, const Material& material,
                     scalar extpx, scalar extpy) {
  // NOTE: only need material because of lwc stuff stored here
  s_pyp = pyp;

  int Nbase = pyp->Q.rows();

  if (extpx < 0 || extpy < 0) {
    // loading for YLC dynamics, no periodicity
    // copy vertices
    m_q.resize(pyp->Q.rows() * 4);
    m_p.resize(pyp->Q.rows());
    for (int i = 0; i < pyp->Q.rows(); i++) {
      m_q.segment<4>(4 * i) = pyp->Q.row(i);
      m_p(i)                = i;
    }
    // generate periodic vertex ids
    // P.resize(Q.rows(),1);
    // for (int i = 0; i < Q.rows(); i++) P[i] = i;
    MatrixXXi vet =
        computeVertexEdgeTable(pyp->Q.rows(), pyp->E);  // vertex-edge table
    // find starting vertices: prev edge is periodic
    auto starts = find_where(
        [&](int i) {
          int prevedge = vet(i, 0);
          assert(prevedge >= 0 && "nonperiodic PYP!");
          if (pyp->E.row(prevedge).tail<2>().cwiseAbs().sum() >
              0)  // dxdy nonzero
            return true;
          return false;
        },
        0, pyp->Q.rows());
    // follow until next edge is periodic
    m_yl.resize(starts.size());
    int n_reserve_estimate =
        pyp->Q.rows() / (int)starts.size();  // ok for similar
    threadutils::parallel_for(0, (int)starts.size(), [&](int i) {
      auto& yarn = m_yl[i];
      yarn.reserve(n_reserve_estimate);
      int vix = starts[i];
      int k   = 0;
      while (true) {
        if (k > pyp->Q.rows()) {  // catch loops
          assert(false &&
                 "potential loop in yarn list, yarn has more vertices than "
                 "pattern");
          Debug::error("potential loop in yarn list aborted!");
          break;
        }
        yarn.push_back(vix);
        int next_eix = vet(vix, 1);
        assert(next_eix >= 0 && "nonperiodic PYP!");
        bool periodic = pyp->E.row(next_eix).tail<2>().cwiseAbs().sum() >
                        0;  // dxdy nonzero
        if (periodic)
          break;
        vix = pyp->E(next_eix, 1);
      }
      if (yarn.size() <
          0.1 * yarn.capacity())  // heuristic shrinking if 10% used
        yarn.shrink_to_fit();
    });

    m_yl.erase(
        std::remove_if(m_yl.begin(), m_yl.end(),
                       [](auto& y) { return y.size() < 2; }),
        m_yl.end());  // bc there might have been a vertexedge with periodic
                      // edge which would create a single vertex here
  } else {
    // define inside outside bounds
    // mark vertices as deleted by e.g. setting their PID to -1
    // march (from non-deleted vertex that has a deleted previous vert or
    // prevedge periodic, until next vert is deleted or nextedge is periodic)

    // compute grid size and center cell for extending both sides with extp%
    int m, n, jmid, imid;
    m = 1 + 2 * (int)std::ceil(extpx);  // extend py extp % both sides
    n = 1 + 2 * (int)std::ceil(extpy);
    if (m % 2 == 0)
      m++;
    if (n % 2 == 0)
      n++;
    imid = (int)(n * 0.5);
    jmid = (int)(m * 0.5);

    // Q P yl
    int n_total = n * m * Nbase;
    m_q.resize(n_total * 4);
    m_p.resize(n_total);
    MatrixXXi E(n_total, 4);

    // want to have middle be the original, ie original indices P[ix]=ix
    //  simple swap:
    //  if ij=00 instead use cix = imid * m + jmid
    //  if ij=mid use cix = 0
    //  shifted swap:
    //  if ij=mid use cix = 0
    //  if ij->cix < ijmid->cix use instead 1+cix(ij)
    auto swapcix = [&](int cix) {
      int i = cix / m;
      int j = cix % m;
      if (i == imid && j == jmid)
        return 0;
      else if (cix == 0)
        return imid * m + jmid;
      else
        return cix;
    };

    for (int vix = 0; vix < n_total; vix++) {
      int oix     = vix % Nbase;
      int cix     = vix / Nbase;
      int i       = cix / m;
      int j       = cix % m;
      cix         = swapcix(cix);
      int swapvix = cix * Nbase + oix;
      // set Q from pypQ but shifted by (j-jmid) px, (i-imid) py
      Vector3s shift;
      shift << (j - jmid) * pyp->px, (i - imid) * pyp->py, 0;
      m_q.segment<4>(swapvix * 4) = pyp->Q.block<1, 4>(oix, 0);
      m_q.segment<3>(swapvix * 4) += shift.transpose();
      m_p(swapvix)  = oix;
      Vector4i edge = pyp->E.row(oix);
      int dx        = edge(2);
      int dy        = edge(3);
      // outgoing vert is in same cell
      edge(0) += cix * Nbase;
      // next vert depends on periodic jump
      if (std::abs(dx) + std::abs(dy) == 0)  // no periodic jump
        edge(1) += cix * Nbase;
      else {
        int nj = j + dx;
        int ni = i + dy;
        // jump between existing cells: resolve periodicity to neighbor cix
        if (nj >= 0 && nj <= m - 1)
          edge(2) = 0;  // dx -> 0
        if (ni >= 0 && ni <= n - 1)
          edge(3) = 0;  // dy -> 0
        // jump across new periodic boundary: wrap to correct cix
        int cixother = swapcix(negmod(ni, n) * m + negmod(nj, m));
        // Debug::logf("jump from (%d,%d)[%d] by (%d,%d) to
        // (%d,%d)[%d]\n",i,j,cix,dy,dx,negmod(ni,n), negmod(nj,m),cixother);
        edge(1) += cixother * Nbase;
      }
      E.row(swapvix) = edge;
    }

    // "cut"
    Vector2s c  = pyp->Q.leftCols<2>().colwise().sum() / Nbase;
    auto inside = [&](int i) {
      if (i < pyp->Q.rows())
        return true;  // ensure keep original
      auto pt = m_q.segment<2>(4 * i);
      return (std::abs(pt(0) - c(0)) <= pyp->px * (0.5 + extpx) &&
              std::abs(pt(1) - c(1)) <= pyp->py * (0.5 + extpy));
    };

    prune_vertices(m_q, m_p, E, inside);

    // trace yarns
    {
      MatrixXXi vet = computeVertexEdgeTable(n_total, E);  // vertex-edge table
      // find starting vertices: prev edge is periodic
      auto starts = find_where(
          [&](int vix) {
            int prevedge = vet(vix, 0);
            // prev edge doesn't exist or prev edge is periodic
            if (prevedge < 0)
              return true;
            if (E.row(prevedge).tail<2>().cwiseAbs().sum() >
                0)  // dxdy nonzero -> periodic edge
              return true;
            return false;
          },
          0, n_total);

      // follow until next edge is periodic or doesnt exit
      m_yl.resize(starts.size());
      int n_reserve_estimate = n_total / (int)starts.size();  // ok for similar
      threadutils::parallel_for(0, (int)starts.size(), [&](int i) {
        auto& yarn = m_yl[i];
        yarn.reserve(n_reserve_estimate);
        int vix = starts[i];
        int k   = 0;
        while (true) {
          if (k > n_total) {  // catch loops
            assert(false &&
                   "potential loop in yarn list, yarn has more vertices than "
                   "pattern");
            Debug::error("potential loop in yarn list aborted!");
            break;
          }
          yarn.push_back(vix);
          int next_eix = vet(vix, 1);
          if (next_eix < 0)
            break;  // no further edge
          bool periodic =
              E.row(next_eix).tail<2>().cwiseAbs().sum() > 0;  // dxdy nonzero
          if (periodic)
            break;
          vix = E(next_eix, 1);
        }
        if (yarn.size() <
            0.1 * yarn.capacity())  // heuristic shrinking if 10% used
          yarn.shrink_to_fit();
      });

      m_yl.erase(
          std::remove_if(m_yl.begin(), m_yl.end(),
                         [](auto& y) { return y.size() < 2; }),
          m_yl.end());  // bc there might have been a vertexedge with periodic
                        // edge which would create a single vertex here
    }

    // potentially prune: walk through yarns and mark vertices as not cut
    // reaccumulate indices, this should preserve original cell
  }

  // precompute and store vertex index to local yarn/segment index
  // for accessing lwc stuff

  m_ycoords = MatrixXXi::Constant(getNumVerticesTotal(), 2, -1);
  threadutils::parallel_for(0, (int)m_yl.size(), [&](int iy) {
    for (int is = 0; is < (int)m_yl[iy].size(); is++) {
      m_ycoords.row(m_yl[iy][is]) << iy, is;
    }
  });
  // assert([&](){
  //   std::vector<bool> vis(m_p.rows(),false);
  //   threadutils::parallel_for(0, (int)m_yl.size(), [&](int iy) {
  //     for (int is = 0; is < (int)m_yl[iy].size(); is++) {
  //       vis[m_yl[iy][is]] = true;
  //     }
  //   });
  //   for (int i = 0; i < m_p.rows(); i++)
  //     if(!vis[i])
  //       return false;
  //   return true;
  // }());

  // create LWC rod stuff TODO METHOD THIS

  // get or compute rest and reference frames
  const VectorXs& restLengths = s_pyp->getRestLengths();
  const MatrixXXs& refd1      = s_pyp->getRefD1s();

  // default kappas and twists to 0
  if (s_pyp->restKappas.rows() != s_pyp->Q.rows())
    s_pyp->restKappas.setZero(s_pyp->Q.rows(), 2);
  if (s_pyp->restTwists.rows() != s_pyp->Q.rows())
    s_pyp->restTwists.setZero(s_pyp->Q.rows());

  // TODO method this to add_lwcyarn and tbb the for loop // need to prealloc
  // with shared nullptr
  for (size_t i = 0; i < m_yl.size(); i++) {
    // rA rB rA rB rA rB ...
    VectorXs radii      = VectorXs::Constant(m_yl[i].size() * 2, pyp->r);
    double mult_stretch = 1, mult_coll = 1,
           mult_attach  = 1;  // LWC modify physically based energies
    double baseRotation = 0;  // LWC rotate bending matrix
    // unused viscosity stuff
    bool accumulateWithViscous                = false,
         accumulateViscousOnlyForBendingModes = false, postProjectFixed = true;
    double viscosity = 0;
    double dt        = 0;
    // more unused LWC stuff
    double straightHairs = 1;
    Vector3s color(0, 0, 0);
    double friction_alpha = 0, friction_beta = 0;
    double restVolFrac = 0;
    // LWC parameter struct
    // since LWC only offers a stretching multiplier
    // to scale bending we scale youngs modulus
    // and undo it for the stretching by diving by gamma
    auto m_lwcstrandParameters = std::make_shared<StrandParameters>(
        radii, material.yE * material.gamma, material.G * material.gamma,
        mult_stretch / material.gamma, mult_coll, mult_attach, material.density,
        viscosity, baseRotation, dt, friction_alpha, friction_beta, restVolFrac,
        accumulateWithViscous, accumulateViscousOnlyForBendingModes,
        postProjectFixed, straightHairs, color);

    // LWC object storing material frames and calculating DER forces
    auto lwc = std::make_shared<StrandForce>(
        std::static_pointer_cast<LwcInterface>(shared_from_this()), m_yl[i],
        m_lwcstrandParameters);
    m_lwcyarns.push_back(lwc);

    // update lwc init from pyp
    // NOTE assuming pyp in flat reference configuration
    // such that copying reference directors etc.
    // works out without any rotation
    for (int ie = 0; ie < lwc->getNumEdges(); ie++) {
      int vix  = lwc->m_verts[ie];
      int ovix = m_p[vix];  // pyp original vertex/edge to copy stuff from

      // set quantities or keep LWC initialization
      if (restLengths.rows() == s_pyp->Q.rows()) {
        lwc->m_restLengths[ie] = s_pyp->restLengths(ovix);
      }
      if (s_pyp->restTwists.rows() == s_pyp->Q.rows()) {
        lwc->m_restTwists[ie] = s_pyp->restTwists(ovix);
      }
      if (s_pyp->restKappas.rows() == s_pyp->Q.rows()) {
        lwc->m_restKappas[ie] = s_pyp->restKappas.row(ovix);
      }
      if (!Debug::get<bool>("DBG_FLAG2"))
        if (refd1.rows() == s_pyp->Q.rows()) {
          // NOTE reference frames refd1 deserialized from txt files may not be
          // perfectly orthogonal, so we reorthogonalize them here (also
          // otherwise lwc complains)
          Vector3s rd1 = refd1.row(ovix);
          orthoNormalize(rd1, lwc->m_strandState->m_tangents[ie]);
          lwc->m_strandState->m_referenceFrames1.set(ie, rd1);
        }
      if (s_pyp->refTwists.rows() == s_pyp->Q.rows()) {
        lwc->m_strandState->m_referenceTwists.set(ie, s_pyp->refTwists(ovix));
      }

      lwc->updateEverythingThatDependsOnRestLengths();
    }
  }
}

Vector3s GYP::computeExtents() {
  int n            = getNumVerticesTotal();
  Vector3s extents = Vector3s::Zero();
  for (int i = 0; i < 3; i++) {
    scalar min = std::numeric_limits<scalar>::max();
    scalar max = std::numeric_limits<scalar>::min();
    for (int j = 0; j < n; j++) {
      scalar v = m_q(4 * j + i);
      min      = std::min(min, v);
      max      = std::max(max, v);
    }
    extents(i) = max - min;
  }
  return extents;
}

void GYP::precomputeExpansionData(std::shared_ptr<Expansion> exp) {
  s_exp = exp;
  // precompute exp_x, R per vertex
  int n = getNumVerticesTotal();
  m_exp_x.resize(n * 3);
  m_dqdg.resize(n * 4, getNumDofPeriodic());
  std::vector<Triplet> triplets;
  triplets.reserve((3 * 3 + 1) * n);
  for (int i = 0; i < n; i++) {
    if (m_ycoords(i, 0) < 0)
      continue;  // ignore cut vertex

    Vector2s xi               = m_q.segment<2>(4 * i);
    scalar h                  = m_q(4 * i + 2);
    Matrix3s R                = s_exp->R(xi);
    Vector3s phi              = s_exp->phi(xi);
    m_exp_x.segment<3>(3 * i) = phi + h * R.col(2);

    int itrue = m_p[i];
    for (int k = 0; k < 3; ++k) {
      for (int l = 0; l < 3; ++l) {
        scalar val = R(k, l);
        if (std::abs(val) > 1e-15)
          triplets.emplace_back(4 * i + k, 4 * itrue + l, val);
      }
    }
    triplets.emplace_back(4 * i + 3, 4 * itrue + 3, 1.0);
  }
  m_dqdg.setFromTriplets(triplets.begin(), triplets.end());
}

scalar GYP::getRestLength(int i) const {
  int iy = m_ycoords(i, 0);
  int is = m_ycoords(i, 1);
  if (is >= m_lwcyarns[iy]->getNumEdges())
    return 0;
  return m_lwcyarns[iy]->m_restLengths[is];
}

scalar GYP::computeWeight(const std::vector<int>& ixs) const {
  assert(ixs.size() > 0);
  scalar w = 0;
  for (size_t i = 0; i < ixs.size(); i++) w += 1 - isPeriodic(ixs[i]);
  return w / ixs.size();
}

void GYP::setGhostsFromPeriodic(const VectorXs& g, const VectorXs& dgdt,
                                scalar dt) {
  // int nprd = s_pyp->Q.rows();
  if (g.rows() > 0) {
    assert(g.rows() == s_pyp->Q.rows() * 4);

    // x(XI,H) = phi(XI) + H n(XI) + utilde(XI, H)
    //         = phi(XI) + H n(XI) + R(XI) R^T.utilde(XI, H)
    //         = phi(XI) + H n(XI) + R(XI) g(XI, H)
    // g is periodic in XI
    // with ig = index of ghost vertex, ip = index of original vertex
    // x_ig    = phi(XI_ig) + H_ig n(XI_ig) + R(XI_ig) g_ip

    int n = getNumVerticesTotal();
    if (s_exp)
      for (int i = 0; i < n; i++) {
        int ip                = m_p[i];
        m_q.segment<3>(4 * i) = m_exp_x.segment<3>(3 * i) +
                                getR(i) * g.segment<3>(4 * ip);  // set xyz
        m_q(4 * i + 3) = g(4 * ip + 3);                          // theta
      }
    else
      for (int i = 0; i < n; i++) m_q.segment<4>(4 * i) = g.segment<4>(4 * i);
  }

  if (dgdt.rows() > 0) {
    assert(dgdt.rows() == s_pyp->Q.rows() * 4);
    assert(false);  // not implemented, not used
    // dxdt_ig = dexpdt_ig + dRdt_ig g_ip + R_ig dqdt_ip // in general
    // dxdt_ig = R_ig dqdt_ip // if exp doesnt move
    // dthetadt_ig = dthetadt_ip
  }

  VectorXs empty;
  threadutils::parallel_for_each(m_lwcyarns, [&](auto sf) {
    sf->updateStrandState();
    // required for correct hessians in lwc
    if (dt < 0)
      return;  // skip in case we never want hessians
    if (m_dqdt.rows() == 0)
      sf->updateMultipliers(empty, VectorXs::Zero(m_q.rows()), empty, dt);
    else
      sf->updateMultipliers(empty, m_dqdt, empty, dt);
  });

  // NOTE: due to periodic updates, this projection should only be necessary
  // once at the beginning of simulation. however, for some reason this
  // needs to be called at least once more at some early later point. most
  // likely caused by some nontrivial update of internal variables within LWC
  // which I failed to pinpoint.
  if (s_exp)
    projectPeriodicFrames();

  // NOTE: for dynamics: exp=null and q=dqdt=null allows to use this method as
  // updatestuff() while setting the state from the outside
  // alternative: just implement some basic forcemodel&non-544pyp mixed class
  // that
  // does everything we need it to super optimized
  // and that can easily set underlying PYP stuff or so for serializing
  // also in dynamics use dt = -1 to skip updatemultipliers
}

VectorXs GYP::reduceGradient(const VectorXs& dfdq) {
  // q_ghost = (exp_x_ghost + R_ghost g_original; theta_original)
  // d q_ghost/d (g_original;theta_original) = (R_ghost, 0; 0, 1 )
  // -> need to dot gradE with transpose R for vertices

  assert(dfdq.rows() == m_q.rows());

  // NOTE parallel possible if store ghostverts[truevert] -> {gv0 gv1 gv1 ...}
  // VectorXs dfdg = VectorXs::Zero(getNumDofPeriodic());
  // for (int i = 0; i < getNumVerticesTotal(); i++) {
  //   auto R     = m_R.block<3, 3>(3 * i, 0);
  //   int i_true = m_p[i];
  //   dfdg.segment<3>(4 * i_true).noalias() +=
  //       R.transpose() * grad.segment<3>(4 * i);
  //   dfdg(4 * i_true + 3) += grad(4 * i + 3);
  // }
  return m_dqdg.transpose() * dfdq;
}

SparseXXs GYP::reduceHessian(const SparseXXs& d2fdqdq) {
  return m_dqdg.transpose() * d2fdqdq * m_dqdg;
}

void GYP::projectPeriodicFrames() {
  // for any vertex on the edge cylinders

  // denoting +/- as periodic pair
  // along with theta+ = theta-
  // need R+^T ref_k+ = R-^T ref_k-
  // in lwc ref_2 is defined from tangent (which is assumed to be Rtu-periodic)
  // and ref_1, so we only need to project ref_1
  int n = getNumVerticesTotal();
  threadutils::parallel_for(0, n, [&](int i) {
    int i_orig = m_p(i);
    if (i_orig == i)
      return;  // no need to project onto self

    auto yc = m_ycoords.row(i);  // iyarn iseg for ghost vertex
    if (yc(0) < 0)
      return;  // vertex not part of any yarn

    bool isTip = yc(1) == (int)m_yl[yc(0)].size() - 1;
    // otherwise setting lwc edge values for nonexistent edges breaks lwc
    if (isTip)
      return;

    // R_ghost R^T_orig
    Matrix3s RRt = getR(i) * getR(i_orig).transpose();

    auto yc_orig = m_ycoords.row(i_orig);  // iyarn iseg for original vertex

    // rotate r1 from original to ghost
    Vector3s r1_orig =
        m_lwcyarns[yc_orig(0)]->m_strandState->m_referenceFrames1[yc_orig(1)];
    Vector3s r1 = RRt * r1_orig;

    // orthogonalize wrt tangent because numerics (periodicity of tangent is
    // discretized from two vertices), and lwc assumes orthogonality
    Vector3s t =
        m_lwcyarns[yc(0)]
            ->m_strandState->m_referenceFrames1.getPreviousTangents()[yc(1)];
    r1 -= t.dot(r1) * t;
    r1.normalize();

    m_lwcyarns[yc(0)]->m_strandState->m_referenceFrames1.set(yc(1), r1);
  });
}

template <typename F, typename T>
void setLWCFromTo(F& from, T& to) {
  to->m_kappas.set(from->m_kappas.get());
  to->m_materialFrames2.set(from->m_materialFrames2.get());
  to->m_materialFrames1.set(from->m_materialFrames1.get());
  to->m_trigThetas.set(from->m_trigThetas.get());
  to->m_curvatureBinormals.set(from->m_curvatureBinormals.get());
  to->m_twists.set(from->m_twists.get());
  to->m_referenceTwists.set(from->m_referenceTwists.get());
  to->m_referenceFrames2.set(from->m_referenceFrames2.get());
  to->m_referenceFrames1.set(from->m_referenceFrames1.get());
  to->m_referenceFrames1.getPreviousTangents() =
      from->m_referenceFrames1.getPreviousTangents();
  to->m_tangents.set(from->m_tangents.get());
  to->m_lengths.set(from->m_lengths.get());
  to->m_edges.set(from->m_edges.get());
  to->m_dofs.set(from->m_dofs.get());

  to->m_kappas.setClean();
  to->m_materialFrames2.setClean();
  to->m_materialFrames1.setClean();
  to->m_trigThetas.setClean();
  to->m_curvatureBinormals.setClean();
  to->m_twists.setClean();
  to->m_referenceTwists.setClean();
  to->m_referenceFrames2.setClean();
  to->m_referenceFrames1.setClean();
  to->m_tangents.setClean();
  to->m_lengths.setClean();
  to->m_edges.setClean();
  to->m_dofs.setClean();
}

void GYP::saveLWCState() {
  // NOTE abusing LWC's viscous threads implementation
  threadutils::parallel_for_each(m_lwcyarns, [&](auto sf) {
    setLWCFromTo(sf->m_strandState, sf->m_startState);
  });
}
void GYP::restoreLWCState() {
  // NOTE abusing LWC's viscous threads implementation
  threadutils::parallel_for_each(m_lwcyarns, [&](auto sf) {
    setLWCFromTo(sf->m_startState, sf->m_strandState);
  });
}

void GYP::freezeRestshapes() {
  // set current state as rest state
  threadutils::parallel_for_each(
      m_lwcyarns, [&](auto sf) { sf->freezeRestShape(0, sf->getNumEdges()); });
}

void GYP::scaleRestshapes(scalar rl, scalar rk, scalar rt) {
  // scale rest lengths, rest curvatures, and rest twists
  threadutils::parallel_for_each(m_lwcyarns, [&](auto sf) {
    for (int i = 0; i < sf->getNumEdges(); i++) {
      sf->m_restLengths[i] *= rl;
      sf->m_restKappas[i] *= rk;
      sf->m_restTwists[i] *= rt;
    }
    sf->updateEverythingThatDependsOnRestLengths();
  });
}

MatrixXs GYP::getQd() const {
  int N = s_pyp->Q.rows();
  // copy current state into correct shape
  MatrixXs Qd(N,4);

  int n = getNumVerticesTotal();
  for (int ig = 0; ig < n; ig++) {
    if (isPeriodic(ig))
      continue;  // only overwrite using data from noncopied dofs
    const auto yc = m_ycoords.row(ig);
    if (yc(0) < 0)
      continue;  // ignore cut vertex
    int i = m_p[ig];  // true vertex index (should be same as ig)
    Qd.row(i) = m_q.segment<4>(4 * ig);
  }
  return Qd;
}

MatrixXs GYP::computeQrd(scalar eps_r, int max_steps) const {
  // get deformed state m_q in the shape of pyp.Q ie only the true dofs, and mapped back into reference space

  scalar eps = s_pyp->r  * eps_r;
  int N = s_pyp->Q.rows();

  // copy current state into correct shape
  MatrixXs Qd = getQd();
  MatrixXs Qrd = s_pyp->Q; // copy initial reference state
  Qrd.col(3) = Qd.col(3); // copy twists, ignore transformation of reference directors

  AlignedVector<Vector3s> dx(N);

  // newton iteration to find Xrd such that xbar(Xrd) = xd
  // Xrd += Jbar(Xrd)^-1 (xd - xbar(Xrd))
  for (int i = 0; i < max_steps; ++i) {
    // std::cout << "      NWT " << i << "\n";
    // compute dx: (xd - xbar(Xrd))
    for (int vix = 0; vix < N; ++vix) {
      auto Xrd = Qrd.block<1,3>(vix,0);
      auto xd = Qd.block<1,3>(vix,0).transpose();
      dx[vix] = xd - s_exp->xbar(Xrd);
    }

    // check stopping threshold |dx|_inf < eps_r * r
    scalar max_dx = 0;
    for (int vix = 0; vix < N; ++vix) {
      max_dx = std::max(max_dx, dx[vix].cwiseAbs().maxCoeff());
      if (max_dx > eps)
            break;
    }
    if (max_dx < eps)
      break; // target error achieved

    // step
    for (int vix = 0; vix < N; ++vix) {
      auto Xrd = Qrd.block<1,3>(vix,0);
      Xrd += s_exp->Jbar(Xrd).inverse() * (dx[vix]);
    }
  }

  // NOTE alternatively could do the newton iterations individually for each vertex,
  // to avoid some computations of already converged vertices. but as it is now
  // a couple iterations for the small patterns are basically instantaneous
  // compared to the simulation anyway

  return Qrd;
}

void GYP::setPYPFromLWC() {
  // override Q, ref and rest data of pyp from simulated lwc

  int N = s_pyp->Q.rows();
  s_pyp->restLengths.resize(N);
  s_pyp->restKappas.resize(N, 2);
  s_pyp->restTwists.resize(N);
  s_pyp->refd1.resize(N, 3);
  s_pyp->refTwists.resize(N);

  // NOTE assuming E is still ok (no topological changes made)

  // NOTE lwc stores inner-vertex data in arrays of length nverts-1
  // but ignores the first entry, s.t. vertex index and data index match

  int n = getNumVerticesTotal();
  for (int ig = 0; ig < n; ig++) {
    if (isPeriodic(ig))
      continue;  // only overwrite using data from noncopied dofs
    const auto yc = m_ycoords.row(ig);
    if (yc(0) < 0)
      continue;  // ignore cut vertex

    int i = m_p[ig];  // true vertex index (should be same as ig)

    auto sf = m_lwcyarns[yc(0)];
    int ie  = yc(1);  // local edgevertindex within strand

    s_pyp->Q.row(i) = m_q.segment<4>(4 * ig);

    if (ie >= (int)sf->m_verts.size() - 1) {  // tip vertex
      assert(false);  // this shouldn't happen since there should always be
                      // ghost vertices on either side
      s_pyp->restLengths(i) = 0;
      s_pyp->restKappas.row(i).setZero();
      s_pyp->restTwists(i) = 0;
      s_pyp->refd1.row(i).setZero();
      s_pyp->refTwists(i) = 0;
    } else {
      s_pyp->restLengths(i)    = sf->m_restLengths[ie];
      s_pyp->restKappas.row(i) = sf->m_restKappas[ie];
      s_pyp->restTwists(i)     = sf->m_restTwists[ie];
      s_pyp->refd1.row(i)      = sf->m_strandState->m_referenceFrames1[ie];
      s_pyp->refTwists(i)      = sf->m_strandState->m_referenceTwists[ie];
    }
  }
}