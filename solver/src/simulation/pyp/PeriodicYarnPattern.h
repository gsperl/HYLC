#ifndef __PERIODICYARNPATTERN__H__
#define __PERIODICYARNPATTERN__H__

#include <vector>
#include "../EigenDefinitions.h"

// periodic yarn pattern in reference configuration: [xi1 xi2 h] & twists
struct PeriodicYarnPattern {
  // vertex-edge data (vertex + outgoing edge combined)
  MatrixXXs Q;  // [x y z theta]
  VectorXs restLengths; // edge rest lengths // stored per vertex' outgoing edge!
  MatrixXXs restKappas; // vertex rest bending kappa
  VectorXs restTwists; // edge rest twist // stored per vertex' outgoing edge!
  MatrixXXs refd1; // edge reference directors _d1 // stored per vertex' outgoing edge!
  VectorXs refTwists; // (accumulating) vertex reference twist
  // NOTE: the per-edge quantities above are stored per vertex, i.e. they should be accessed e.g. via RL[vix] and not RL[eix] or RL[E(vix,0)]

  // topology
  // edge data [v0, v1, dx, dy]
  MatrixXXi E;  // edge from v0 to v1 is jumping boundaries by dx dy
  MatrixXXi VE; // vertex edge table VE[vix] = [eprev,enext]

  scalar px, py;  // periodic lengths
  scalar r;       // radius

  // compute vix->[eix_prev,eix_next]
  const MatrixXXi & getVertexEdgeTable(bool force_recompute = false);
  void recomputeVertexEdgeTable();
  void rectangulize();
  bool verify();
  // recompute reference frames if not set and return reference
  MatrixXXs& getRefD1s(bool force_recompute = false);
  void recomputeReferenceDirectors();
  // recompute rest lengths if not set and return reference
  VectorXs& getRestLengths(bool force_recompute = false);
  VectorXs computeEdgeLengths();
  // compute periodically connected yarns as lists of vertices
  std::vector<std::vector<int>> computePeriodicallyConnectedYarns();
  // compute list of all edges crossing the boundary, i.e. dx,dy != 0,0
  std::vector<int> getBoundaryEdges();

  template <int N = 3>
  std::tuple<Eigen::Matrix<scalar, N, 1>, Eigen::Matrix<scalar, N, 1>>
  getBounds() {
    Eigen::Matrix<scalar, N, 1> min = Q.leftCols<N>().colwise().minCoeff();
    Eigen::Matrix<scalar, N, 1> max = Q.leftCols<N>().colwise().maxCoeff();
    return std::make_tuple(min, max);
  }

  // compute total volume of yarns from cylinder segments
  scalar computeYarnVolume();
};
using PYP = PeriodicYarnPattern;  // short alias of periodic yarn pattern

#endif  // __PERIODICYARNPATTERN__H__