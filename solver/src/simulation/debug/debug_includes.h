#ifndef _DEBUG_INCLUDES_H_
#define _DEBUG_INCLUDES_H_

#include "debug_singleton.h"
#include "debug_logging.h"
#include "debug_timer.h"

#endif /* end of include guard: _DEBUG_INCLUDES_H_ */
