#ifndef __ALGORITHMUTILS__H__
#define __ALGORITHMUTILS__H__

#include <vector>
#include <functional>

std::vector<int> find_where(std::function<bool(int)> func, int start, int stop,
                            int step = 1);

#endif // __ALGORITHMUTILS__H__