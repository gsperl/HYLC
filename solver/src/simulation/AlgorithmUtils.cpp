#include "AlgorithmUtils.h"

std::vector<int> find_where(std::function<bool(int)> func, int start, int stop,
                            int step) {
  std::vector<int> where;
  for (int i = start; i < stop; i += step) {
    if (func(i))
      where.push_back(i);
  }
  return where;
}