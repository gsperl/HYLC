#ifndef _SERIALIZATION_H_
#define _SERIALIZATION_H_

#include "../pyp/PeriodicYarnPattern.h"

#define VTX_PRECISION 8  // precision of floats during serialization

bool deserialize_pyp(const std::string &filename, PYP &pyp);

bool serialize_pyp(const std::string &filename, const PYP &pyp);

#endif /* end of include guard: _SERIALIZATION_H_ */
