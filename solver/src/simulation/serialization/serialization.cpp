#include "serialization.h"
#include <fstream>
#include <iostream>
#include "../debug/debug_includes.h"

void deserialize_pyp_pattern(const std::vector<std::string>& chunk, PYP& pyp) {
  for (size_t i = 1; i < chunk.size(); i++) {
    std::stringstream ss(chunk[i]);
    // px py radius nvertedges
    int n;
    ss >> pyp.px >> pyp.py >> pyp.r;
    ss >> n;
    pyp.Q.resize(n, 4);
    pyp.E.resize(n, 4);
  }
}

template <typename value_type, int ncols>
void deserialize_matrix_chunk(
    const std::vector<std::string>& chunk,
    Eigen::Matrix<value_type, Eigen::Dynamic, ncols>& M) {
  assert(M.cols() > 0);  // need to set size of M before calling this
  for (int i = 0; i < (int)chunk.size() - 1; i++) {
    std::stringstream ss(chunk[i + 1]);
    for (int j = 0; j < M.cols(); j++) {
      ss >> M(i, j);
    }
  }
}

std::string& trim(std::string& str, const std::string& lchars = "\t\n\v\f\r ",
                  const std::string& rchars = "\t\n\v\f\r #") {
  str.erase(0, str.find_first_not_of(lchars));
  str.erase(str.find_last_not_of(rchars) + 1);
  return str;
}

std::vector<std::vector<std::string>> preload_chunks(
    std::ifstream& ifs, const std::string& start = "*") {
  std::vector<std::vector<std::string>> chunks;

  std::string line;
  while (std::getline(ifs, line)) {
    trim(line);
    if (line.rfind(start, 0) == 0) {
      chunks.push_back({});
      chunks.back().push_back(line);
      while (std::getline(ifs, line)) {
        trim(line);
        if (line.empty())
          break;
        chunks.back().push_back(line);
      }
    }
  }
  return chunks;
}

int find_chunk_starting_with(
    const std::vector<std::vector<std::string>>& chunks,
    const std::string& start) {
  for (size_t i = 0; i < chunks.size(); i++) {
    if (chunks[i][0].rfind(start, 0) == 0)
      return (int)i;
  }
  return -1;
}

bool deserialize_pyp(const std::string& filename, PYP& pyp) {
  std::ifstream ifs(filename);
  if (!ifs) {
    return false;
  }

  auto chunks = preload_chunks(ifs, "*");

  // *pattern
  // px py radius num_vertedges
  int i;
  i = find_chunk_starting_with(chunks, "*pattern");
  if (i < 0)
    return false;
  deserialize_pyp_pattern(chunks[i], pyp);  // also sets size of Q, E

  // *Q
  // x y z t
  i = find_chunk_starting_with(chunks, "*Q");
  if (i < 0)
    return false;
  deserialize_matrix_chunk(chunks[i], pyp.Q);

  // *E
  // v0 v1 dx dy
  i = find_chunk_starting_with(chunks, "*E");
  if (i < 0)
    return false;
  deserialize_matrix_chunk(chunks[i], pyp.E);

  // optional blocks

  // *RL
  // l
  i = find_chunk_starting_with(chunks, "*RL");
  if (i >= 0) {
    pyp.restLengths.resize(pyp.Q.rows());
    deserialize_matrix_chunk(chunks[i], pyp.restLengths);
  }

  // *RK
  // k1 k2
  i = find_chunk_starting_with(chunks, "*RK");
  if (i >= 0) {
    pyp.restKappas.resize(pyp.Q.rows(),2);
    deserialize_matrix_chunk(chunks[i], pyp.restKappas);
  }

  // *RT
  // t
  i = find_chunk_starting_with(chunks, "*RT");
  if (i >= 0) {
    pyp.restTwists.resize(pyp.Q.rows());
    deserialize_matrix_chunk(chunks[i], pyp.restTwists);
  }

  // *RefD1
  // x y z
  i = find_chunk_starting_with(chunks, "*RefD1");
  if (i >= 0) {
    pyp.refd1.resize(pyp.Q.rows(),3);
    deserialize_matrix_chunk(chunks[i], pyp.refd1);
  }

  // *RefT
  // m
  i = find_chunk_starting_with(chunks, "*RefT");
  if (i >= 0) {
    pyp.refTwists.resize(pyp.Q.rows());
    deserialize_matrix_chunk(chunks[i], pyp.refTwists);
  }

  return true;
}

template <typename value_type, int ncols>
void write_matrix_block(
    std::ofstream& fstream,
    const Eigen::Matrix<value_type, Eigen::Dynamic, ncols>& M,
    const std::string& header = "") {
  if (!header.empty())
    fstream << header << "\n";
  Eigen::IOFormat matfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, " ",
                         "\n", "", "", "", "");
  fstream << M.format(matfmt) << "\n";
}

bool serialize_pyp(const std::string& filename, const PYP& pyp) {
  std::ofstream fstream;
  fstream.open(filename);
  if (!fstream) {
    return false;
  }
  fstream.precision(VTX_PRECISION);

  // required blocks

  // *pattern
  // px py radius num_vertedges
  fstream << "*pattern # px py radius num_vertedges\n";
  fstream << pyp.px << " " << pyp.py << " " << pyp.r << " " << pyp.Q.rows()
          << "\n";
  fstream << "\n";

  // *Q
  // x y z t
  write_matrix_block(fstream, pyp.Q, "*Q # dofs: x y z t");
  fstream << "\n";

  // *E
  // v0 v1 dx dy
  write_matrix_block(fstream, pyp.E, "*E # edges: v0 v1 dx dy");
  fstream << "\n";

  // optional blocks

  // *RL
  // l
  write_matrix_block(fstream, pyp.restLengths, "*RL # rest lengths");
  fstream << "\n";

  // *RK
  // k1 k2
  write_matrix_block(fstream, pyp.restKappas, "*RK # rest kappas");
  fstream << "\n";

  // *RT
  // t
  write_matrix_block(fstream, pyp.restTwists, "*RT # rest twists");
  fstream << "\n";

  // *RefD1
  // x y z
  write_matrix_block(fstream, pyp.refd1, "*RefD1 # reference directors");
  fstream << "\n";

  // *RefT
  // m
  write_matrix_block(fstream, pyp.refTwists, "*RefT # reference twists");
  fstream << "\n";

  fstream.close();
  return true;
}
