import HYLC.PatternSolver as ps
from HYLC.batching import batch
from HYLC.parallel import run_parallel
import HYLC.values
import numpy as np
import os
import time

max_time = 240  # seconds per sim
max_steps = 10000
output_every = 50  # log output after finishing # simulations
n_cpus = None  # cpus to use in parallel (default: max available)
overwrite_existing = False  # overwrite existing output files
output_folder = "results"
DEBUG_SKIP_2D = True

# how many samples for 1D and 2D (per dimension) and which 2D ranges
n_per_1D = 150
n_per_2D = 50
sx, sa, sy, IIxx, IIyy = 0, 1, 2, 3, 5
coords_2D = [
    [sx, sa],
    [sx, sy],
    [sx, IIxx],
    [sx, IIyy],

    [sa, sy],
    [sa, IIxx],
    [sa, IIyy],

    [sy, IIxx],
    [sy, IIyy],
]


def compute_step_factor(strain):
    """compute step factor from lowest eigenvalue of stretching tensor"""
    # lower eigenvalue shows strongest compression length
    a = strain[0] + 1
    b = strain[1] * (strain[2] + 1)
    c = np.sqrt(1 - strain[1]**2) * (strain[2] + 1)
    lam = (a+c)*0.5 - np.sqrt((a-c)**2 * 0.25 + b**2)
    lam = min(max(lam, 0), 1)  # clamp
    rstep = lam**2 * 0.15 + 0.05  # from 0.01 to 0.2 quadratically
    return rstep


def sample_range(rge, N, k):
    """generate N non-uniformly spaced samples for strains within range rge"""
    if k in [sa, IIxx, IIyy]:
        samples = np.linspace(-1 * np.abs(rge[0])**0.5, rge[1]**0.5, N)
        s = np.sign(samples)
        return np.abs(samples)**2 * s
    else:  # k in [sx, sy]
        return np.linspace((rge[0]+1)**0.1, (rge[1]+1)**0.1, N)**10 - 1


def reduce_range(rge, plow, phigh):
    sz = rge[1] - rge[0]
    return [rge[0] + plow*sz, rge[0] + phigh*sz]


def reduce_2d_range_from_1d_limits(k0, k1, strain_limits):
    """generate reduced range for the 2D ranges given by k0,k1 from 1D range limits"""
    assert(k0 < k1)
    rge0 = strain_limits[k0]
    rge1 = strain_limits[k1]
    if (k0, k1) == (0, 2):  # poisson mode: sx,sy
        rge0 = reduce_range(rge0, 0.1, 1.0)  # reduce bottom only
        rge1 = reduce_range(rge1, 0.1, 1.0)  # reduce bottom only
    else:
        # reduce both ranges
        rge0 = reduce_range(rge0, 0.1, 0.9)
        rge1 = reduce_range(rge1, 0.1, 0.9)
        # clamp compression
        if k0 in [sx, sy]:
            rge0[0] = max(rge0[0], -0.1)
        if k1 in [sx, sy]:
            rge1[0] = max(rge0[0], -0.1)
        # reduce bending when in the same direction as compression
        if (k0, k1) == (sx, IIxx):
            rge1[0] *= 0.9
            rge1[1] *= 0.9
        if (k0, k1) == (sy, IIyy):
            rge1[0] *= 0.9
            rge1[1] *= 0.9
    return [rge0, rge1]


def iterate_1D(k, rangek):
    samples = sample_range(rangek, n_per_1D, k)
    for a in samples:
        strain = np.zeros((6,), dtype=np.float)
        strain[k] = a
        yield strain


def iterate_2D(k0, k1, rangek0, rangek1):
    samples0 = sample_range(rangek0, n_per_2D, k0)
    samples1 = sample_range(rangek1, n_per_2D, k1)
    for a in samples0:
        for b in samples1:
            strain = np.zeros((6,), dtype=np.float)
            strain[k0] = a
            strain[k1] = b
            yield strain


def make_iterators(strain_limits):
    iterators = []
    for k in [sx,sa,sy,IIxx,IIyy]:
        iterators.append(("1D_%d" % k, iterate_1D(k, strain_limits[k])))

    if DEBUG_SKIP_2D:
        return iterators

    for i in range(len(coords_2D)):
        k0, k1 = coords_2D[i]
        rangek0, rangek1 = reduce_2d_range_from_1d_limits(
            k0, k1, strain_limits)
        iterators.append(
            ("2D_%d%d" % (k0, k1), iterate_2D(k0, k1, rangek0, rangek1)))

    return iterators


def write_info(settings_func, infofile):
    dummystrains = np.zeros((6,))
    settings = settings_func(dummystrains)
    sim = ps.Simulation(settings)
    A = sim.getArea()
    D = sim.getAreaDensity()
    del sim
    folder = os.path.dirname(infofile)
    if folder != "":
        os.makedirs(folder, exist_ok=True)
    with open(infofile, 'w') as thefile:
        thefile.write("%.10g # area (m^2)\n" % A)
        thefile.write("%.10g # area density (kg / m^2)\n" % D)


def write_timing(timingfile, t):
    folder = os.path.dirname(timingfile)
    if folder != "":
        os.makedirs(folder, exist_ok=True)
    with open(timingfile, 'w') as thefile:
        thefile.write("%.2f\n" % t)

# BASKET ######################################################################


pattern = "basket1"
strain_limits = [
    [-0.6, 1.0],   # sx
    [-0.75, 0.75],  # sa
    [-0.6, 1.0],   # sy
    [-170, 170],   # IIxx
    [0, 0],
    [-170, 170],   # IIyy
]
iterators = make_iterators(strain_limits)


def settings_func(strain):
    s = ps.SimulationSettings()
    s.strains = strain
    s.pypfile = "../../pyp/hylc2020/basket.pyp"
    HYLC.values.set_material_settings(s, pattern)
    HYLC.values.set_simulation_settings(s, pattern)
    s.solvableSettings.step_factor = compute_step_factor(strain)
    return s


print("Starting pattern:", pattern)
write_info(settings_func, os.path.join(output_folder, pattern, "info.txt"))
for name, iterator in iterators:
    outputfile = os.path.join(output_folder, pattern, "%s.txt" % name)
    if not os.path.isfile(outputfile) or overwrite_existing:
        T0 = time.time()
        batch(outputfile, iterator, settings_func,
              max_steps=max_steps, max_time=max_time, n_cpus=n_cpus, output_every=output_every)
        write_timing(os.path.join(output_folder, pattern,
                                  "time", name), time.time() - T0)
        print("Finished batch:", outputfile)
print("Finished pattern:", pattern)


# HONEY #######################################################################

pattern = "honey1"
strain_limits = [
    [-0.5, 0.8],   # sx
    [-0.75, 0.75],  # sa
    [-0.5, 0.8],   # sy
    [-120, 120],   # IIxx
    [0, 0],
    [-120, 120],   # IIyy
]
iterators = make_iterators(strain_limits)


def settings_func(strain):
    s = ps.SimulationSettings()
    s.strains = strain
    s.pypfile = "../../pyp/hylc2020/slip_stitch_honeycomb.pyp"
    HYLC.values.set_material_settings(s, pattern)
    HYLC.values.set_simulation_settings(s, pattern)
    s.solvableSettings.step_factor = compute_step_factor(strain)
    return s


print("Starting pattern:", pattern)
write_info(settings_func, os.path.join(output_folder, pattern, "info.txt"))
for name, iterator in iterators:
    outputfile = os.path.join(output_folder, pattern, "%s.txt" % name)
    if not os.path.isfile(outputfile) or overwrite_existing:
        T0 = time.time()
        batch(outputfile, iterator, settings_func,
              max_steps=max_steps, max_time=max_time, n_cpus=n_cpus, output_every=output_every)
        write_timing(os.path.join(output_folder, pattern,
                                  "time", name), time.time() - T0)
        print("Finished batch:", outputfile)
print("Finished pattern:", pattern)


# RIB #########################################################################

pattern = "rib1"
strain_limits = [
    [-0.6, 1.0],   # sx
    [-0.6, 0.6],  # sa
    [-0.6, 1.0],   # sy
    [-100, 100],   # IIxx
    [0, 0],
    [-80,  80],   # IIyy
]
iterators = make_iterators(strain_limits)


def settings_func(strain):
    s = ps.SimulationSettings()
    s.strains = strain
    s.pypfile = "../../pyp/hylc2020/cartridge_belt_rib.pyp"
    HYLC.values.set_material_settings(s, pattern)
    HYLC.values.set_simulation_settings(s, pattern)
    s.solvableSettings.step_factor = compute_step_factor(strain)
    return s


print("Starting pattern:", pattern)
write_info(settings_func, os.path.join(output_folder, pattern, "info.txt"))
for name, iterator in iterators:
    outputfile = os.path.join(output_folder, pattern, "%s.txt" % name)
    if not os.path.isfile(outputfile) or overwrite_existing:
        T0 = time.time()
        batch(outputfile, iterator, settings_func,
              max_steps=max_steps, max_time=max_time, n_cpus=n_cpus, output_every=output_every)
        write_timing(os.path.join(output_folder, pattern,
                                  "time", name), time.time() - T0)
        print("Finished batch:", outputfile)
print("Finished pattern:", pattern)

# SATIN #######################################################################

pattern = "satin1"
strain_limits = [
    [-0.6, 1.0],   # sx
    [-0.75, 0.75],  # sa
    [-0.6, 1.0],   # sy
    [-170, 170],   # IIxx
    [0, 0],
    [-170, 170],   # IIyy
]
iterators = make_iterators(strain_limits)


def settings_func(strain):
    s = ps.SimulationSettings()
    s.strains = strain
    s.pypfile = "../../pyp/hylc2020/satin.pyp"
    HYLC.values.set_material_settings(s, pattern)
    HYLC.values.set_simulation_settings(s, pattern)
    s.solvableSettings.step_factor = compute_step_factor(strain)
    return s


print("Starting pattern:", pattern)
write_info(settings_func, os.path.join(output_folder, pattern, "info.txt"))
for name, iterator in iterators:
    outputfile = os.path.join(output_folder, pattern, "%s.txt" % name)
    if not os.path.isfile(outputfile) or overwrite_existing:
        T0 = time.time()
        batch(outputfile, iterator, settings_func,
              max_steps=max_steps, max_time=max_time, n_cpus=n_cpus, output_every=output_every)
        write_timing(os.path.join(output_folder, pattern,
                                  "time", name), time.time() - T0)
        print("Finished batch:", outputfile)
print("Finished pattern:", pattern)


# STOCKINETTE #################################################################

pattern = "stock1"
strain_limits = [
    [-0.5, 0.8],   # sx
    [-0.75, 0.75],  # sa
    [-0.5, 0.8],   # sy
    [-200, 200],   # IIxx
    [0, 0],
    [-230, 230],   # IIyy
]
iterators = make_iterators(strain_limits)


def settings_func(strain):
    s = ps.SimulationSettings()
    s.strains = strain
    s.pypfile = "../../pyp/hylc2020/stockinette.pyp"
    HYLC.values.set_material_settings(s, pattern)
    HYLC.values.set_simulation_settings(s, pattern)
    s.solvableSettings.step_factor = compute_step_factor(strain)
    return s


print("Starting pattern:", pattern)
write_info(settings_func, os.path.join(output_folder, pattern, "info.txt"))
for name, iterator in iterators:
    outputfile = os.path.join(output_folder, pattern, "%s.txt" % name)
    if not os.path.isfile(outputfile) or overwrite_existing:
        T0 = time.time()
        batch(outputfile, iterator, settings_func,
              max_steps=max_steps, max_time=max_time, n_cpus=n_cpus, output_every=output_every)
        write_timing(os.path.join(output_folder, pattern,
                                  "time", name), time.time() - T0)
        print("Finished batch:", outputfile)
print("Finished pattern:", pattern)


# SATIN SMALL #################################################################

pattern = "satin_small"
strain_limits = [
    [-0.6, 1],   # sx
    [-0.75, 0.75],  # sa
    [-0.6, 1],   # sy
    [-1700, 1700],   # IIxx
    [0, 0],
    [-1700, 1700],   # IIyy
]
iterators = make_iterators(strain_limits)


def settings_func(strain):
    s = ps.SimulationSettings()
    s.strains = strain
    s.pypfile = "../../pyp/hylc2020/satin_small.pyp"
    HYLC.values.set_material_settings(s, pattern)
    HYLC.values.set_simulation_settings(s, pattern)
    s.solvableSettings.step_factor = compute_step_factor(strain)
    return s


print("Starting pattern:", pattern)
write_info(settings_func, os.path.join(output_folder, pattern, "info.txt"))
for name, iterator in iterators:
    outputfile = os.path.join(output_folder, pattern, "%s.txt" % name)
    if not os.path.isfile(outputfile) or overwrite_existing:
        T0 = time.time()
        batch(outputfile, iterator, settings_func,
              max_steps=max_steps, max_time=max_time, n_cpus=n_cpus, output_every=output_every)
        write_timing(os.path.join(output_folder, pattern,
                                  "time", name), time.time() - T0)
        print("Finished batch:", outputfile)
print("Finished pattern:", pattern)

# STOCKINETTE SMALL ###########################################################

pattern = "stock_small"
strain_limits = [
    [-0.5, 0.8],   # sx
    [-0.75, 0.75],  # sa
    [-0.5, 0.8],   # sy
    [-2000, 2000],   # IIxx
    [0, 0],
    [-2300, 2300],   # IIyy
]
iterators = make_iterators(strain_limits)


def settings_func(strain):
    s = ps.SimulationSettings()
    s.strains = strain
    s.pypfile = "../../pyp/hylc2020/stockinette_small.pyp"
    HYLC.values.set_material_settings(s, pattern)
    HYLC.values.set_simulation_settings(s, pattern)
    s.solvableSettings.step_factor = compute_step_factor(strain)
    return s


print("Starting pattern:", pattern)
write_info(settings_func, os.path.join(output_folder, pattern, "info.txt"))
for name, iterator in iterators:
    outputfile = os.path.join(output_folder, pattern, "%s.txt" % name)
    if not os.path.isfile(outputfile) or overwrite_existing:
        T0 = time.time()
        batch(outputfile, iterator, settings_func,
              max_steps=max_steps, max_time=max_time, n_cpus=n_cpus, output_every=output_every)
        write_timing(os.path.join(output_folder, pattern,
                                  "time", name), time.time() - T0)
        print("Finished batch:", outputfile)
print("Finished pattern:", pattern)
